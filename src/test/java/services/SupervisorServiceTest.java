package services;

import domain.Folder;
import domain.FolderType;
import domain.Officer;
import domain.Supervisor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class SupervisorServiceTest extends AbstractTest {




   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private SupervisorService supervisorService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private OfficerService officerService;



   @Test(expected = NullPointerException.class)
   public void registerAsSupervisorPositive() {
      Supervisor u = supervisorService.create();
      Assert.notNull(u);
      u.getUserAccount().setUsername("kkkkkk");
      u.setName("kkkkkk");
      u.setSurname("sdfsdf");
      u.setEmail("perri@gjail.com");
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword("kkkkkk", null);
      u.getUserAccount().setPassword(hash);
      org.junit.Assert.assertNotNull(u);
   }

   @Test(expected = NullPointerException.class)
   public void registerAsSupervisorNegative() {
      Supervisor u = supervisorService.create();
      u.setName("perrito");
      u.getUserAccount().setUsername("perrito");
      u.getUserAccount().setPassword("perrito");
      Assert.notNull(u);
      u.getUserAccount().setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      u.getUserAccount().setPassword(hash);
      UserAccount userAccount = userAccountService.save(u.getUserAccount());
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Supervisor resu = supervisorService.save(u);
      Folder folder = folderService.create();
      folder.setFolderType(FolderType.PUBLIC);
      folder.setName("public");
      folder.setOwner(resu);
      folderService.save(folder);
      Folder folder1 = folderService.create();
      folder1.setFolderType(FolderType.INBOX);
      folder1.setOwner(resu);
      folder1.setName("inbox");
      folderService.save(folder1);
      org.junit.Assert.assertNotNull(resu);
   }
   

   @Test
   public void officersPerMonthBeforRevisions() {
      List<Set<Officer>> res;
      res = supervisorService.officersPerMonthBeforRevisions();
      org.junit.Assert.assertNotNull(res);
   }

   @Test
   public void myOfficers() {
      authenticate("supervisor1");
      Collection<Officer> officers = supervisorService.myOfficers();
      org.junit.Assert.assertNotNull(officers);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void myOfficersNegative() {
      authenticate("");
      Collection<Officer> officers = supervisorService.myOfficers();
      org.junit.Assert.assertNotNull(officers);
      unauthenticate();
   }

   @Test
   public void superviseOfficer() {
      authenticate("supervisor1");
      supervisorService.superviseOfficer(new ArrayList<>(officerService.findAll()).get(0).getId());
      unauthenticate();
   }
   @Test(expected = IndexOutOfBoundsException.class)
   public void superviseOfficerNegative() {
      authenticate("supervisor1");
      supervisorService.superviseOfficer(new ArrayList<>(officerService.findAll()).get(230).getId());
      unauthenticate();
   }
}