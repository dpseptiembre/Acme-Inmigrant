package services;

import domain.Application;
import domain.Category;
import domain.Inmigrant;
import domain.Visa;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class CategoryServiceTest extends AbstractTest {

   @Autowired
   private CategoryService categoryService;
   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private CountryService countryService;
   @Autowired
   private VisaService visaService;


   @Test
   public void driver(){
      List<Application> categorieList = new ArrayList<>();

      Object testingData[][]={
              //01 - EMPTY LIST
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
               * Return: TRUE
               * Postcondition: The application list is shown empty.
              */
              {"administrator1", categorieList.isEmpty(), null},
              //02 - REGULAR CUSTOMER WITH A OFFER
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
               * Precondition: The user is a inmigrant with an application list with one item.
               * Return: TRUE
               * Postcondition: The application list is shown with one single item.
              */
              {"administrator2", categorieList.size()==1, IllegalArgumentException.class},
              //03 - ALL OFFERS
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
              Precondition: The user is a inmigrant with an application list with more than one item.
               * Return: TRUE
               * Postcondition: The application list is shown with multiple items.
              */
              {"administrator5",categorieList.size()>=2, IllegalArgumentException.class}
      };
      for (int i=0; i<testingData.length;i++){
         template((String) testingData[i][0], (Boolean) testingData[i][1], (Class<?>) testingData[i][2]);
      }
   }

   protected void template(String user, Boolean ass, Class<?> exc){
      Class<?> caught;
      caught=null;
      try {
         authenticate(user);
         List<Category> categories = new ArrayList<>(categoryService.findAll());
         Inmigrant inmigrant = new ArrayList<>(inmigrantService.findAll()).get(0);
         Category category = categories.get(0);
         Assert.notNull(category.getName());
         Assert.isTrue(ass);
         authenticate(null);
         categoryService.flush();
      } catch (Throwable oops){
         caught = oops.getClass();
      }
      checkExceptions(exc, caught);
   }

   @Test
   public void categoryCreate(){
      authenticate("administrator1");
      Category category = categoryService.create();
      category.setFather(new ArrayList<>(categoryService.findAll()).get(0));
      category.setDescription("sadfsdfsd");
      category.setName("sadfsdfasdf");
      category.setHidden(false);
      category.setVisas(new ArrayList<Visa>(visaService.findAll()));
      category.setSons(new ArrayList<Category>());
      Category res = categoryService.save(category);
      org.junit.Assert.assertNotNull(res);
      unauthenticate();
   }

   @Test(expected = AssertionError.class)
   public void categoryCreateNegative(){
      authenticate("administrator1");
      Category category = categoryService.create();
      category.setDescription("sadfsdfsd");
      category.setName("sadfsdfasdf");
      category.setHidden(false);
      category.setVisas(new ArrayList<Visa>(visaService.findAll()));
      category.setSons(new ArrayList<Category>());
      Category res = categoryService.save(category);
      org.junit.Assert.assertNotNull(res.getFather());
      unauthenticate();
   }

   @Test
   public void notHiddenCategorys() {
      List<Category> categories = new ArrayList<>(categoryService.findAll());
      Collection <Category> notHiddenCats = categoryService.notHiddenCategorys();
      Integer init = notHiddenCats.size();
      categories.get(0).setHidden(true);
      categoryService.save(categories.get(0));
      Integer fin = notHiddenCats.size()-1;
      org.junit.Assert.assertNotEquals(init,fin);
   }

   @Test
   public void removeCat(){
      List<Category> categories = new ArrayList<>(categoryService.findAll());
      Integer initialSize = categories.size();
      categories.remove(0);
      Integer finalSize = categories.size();
      org.junit.Assert.assertNotEquals(initialSize,finalSize);
   }
}