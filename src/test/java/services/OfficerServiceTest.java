package services;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;

import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
public class OfficerServiceTest extends AbstractTest {
   @Autowired
   private OfficerService officerService;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private ReportService reportService;
   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;

   @Test
   public void registerAsOfficer() {
      Officer u = officerService.create();
      Assert.notNull(u);
      u.getUserAccount().setUsername("kkkkkk");
      u.setName("kkkkkk");
      u.setSurname("sdfsdf");
      u.setEmail("perri@gjail.com");
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword("kkkkkk", null);
      u.getUserAccount().setPassword(hash);
      org.junit.Assert.assertNotNull(u);
   }

   @Test(expected = DataIntegrityViolationException.class)
   public void registerAsOfficerNegative() {
      Officer u = officerService.create();
      u.setName("perrito");
      u.getUserAccount().setUsername("perrito");
      u.getUserAccount().setPassword("perrito");
      Assert.notNull(u);
      u.getUserAccount().setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      u.getUserAccount().setPassword(hash);
      UserAccount userAccount = userAccountService.save(u.getUserAccount());
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Officer resu = officerService.save(u);
      Folder folder = folderService.create();
      folder.setFolderType(FolderType.PUBLIC);
      folder.setName("public");
      folder.setOwner(resu);
      folderService.save(folder);
      Folder folder1 = folderService.create();
      folder1.setFolderType(FolderType.INBOX);
      folder1.setOwner(resu);
      folder1.setName("inbox");
      folderService.save(folder1);
      org.junit.Assert.assertNotNull(resu);
   }

   @Test
   public void acceptedApplicationsInWichImOfficer() {
      authenticate("officer1");
      Collection<Application> applications = officerService.acceptedApplicationsInWichImOfficer(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void acceptedApplicationsInWichImOfficerNegative() {
      authenticate("");
      Collection<Application> applications = officerService.acceptedApplicationsInWichImOfficer(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }

   @Test
   public void rejectedApplicationsInWichImOfficer() {
      authenticate("officer1");
      Collection<Application> applications = officerService.rejectedApplicationsInWichImOfficer(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void rejectedApplicationsInWichImOfficerNegative() {
      authenticate("");
      Collection<Application> applications = officerService.rejectedApplicationsInWichImOfficer(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }

   @Test
   public void pendingApplicationsInWichImOfficer() {
      authenticate("officer1");
      Collection<Application> applications = officerService.pendingApplicationsInWichImOfficer(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void pendingApplicationsInWichImOfficerNegative() {
      authenticate("");
      Collection<Application> applications = officerService.pendingApplicationsInWichImOfficer(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test
   public void reportOfApplicationThatIOffice() {
      authenticate("officer1");
      Collection<Report> reports = officerService.reportOfApplicationThatIOffice(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(reports);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void reportOfApplicationThatIOfficeNegative() {
      authenticate("");
      Collection<Report> reports = officerService.reportOfApplicationThatIOffice(officerService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(reports);
      unauthenticate();
   }
}