package services;

import domain.Application;
import domain.Country;
import domain.Inmigrant;
import domain.Visa;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.StrictMath.abs;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class ApplicationServiceTest extends AbstractTest {


   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private VisaService visaService;
   @Autowired
   private OfficerService officerService;

   @Test
   public void driver(){
      List<Application> applicationList = new ArrayList<>();

      Object testingData[][]={
              //01 - EMPTY LIST
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
               * Return: TRUE
               * Postcondition: The application list is shown empty.
              */
              {"inmigrant1", applicationList.isEmpty(), null},
              //02 - REGULAR CUSTOMER WITH A OFFER
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
               * Precondition: The user is a inmigrant with an application list with one item.
               * Return: TRUE
               * Postcondition: The application list is shown with one single item.
              */
              {"inmigrant2", applicationList.size()==1, IllegalArgumentException.class},
              //03 - ALL OFFERS
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
                Precondition: The user is a inmigrant with an application list with more than one item.
               * Return: TRUE
               * Postcondition: The application list is shown with multiple items.
              */
              {"inmigrant4",applicationList.size()>=2, IllegalArgumentException.class}
      };
      for (int i=0; i<testingData.length;i++){
         template((String) testingData[i][0], (Boolean) testingData[i][1], (Class<?>) testingData[i][2]);
      }
   }

   protected void template(String user, Boolean ass, Class<?> exc){
      Class<?> caught;
      caught=null;
      try {
         authenticate(user);
         List<Application> applicationList = new ArrayList<>();
         Inmigrant inmigrant = inmigrantService.findByPrincipal();
         applicationList.addAll(inmigrant.getApplications());
         Assert.isTrue(ass);
         authenticate(null);
         applicationService.flush();
      } catch (Throwable oops){
         caught = oops.getClass();
      }
      checkExceptions(exc, caught);
   }

   @Test
   public void applicationCreate(){
      List<Application> applicationList = new ArrayList<>();



      Object testingData[][]={
              //01 - EMPTY LIST
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
               * Return: TRUE
               * Postcondition: The application list is shown empty.
              */
                {"inmigrant1", applicationList.isEmpty(), IndexOutOfBoundsException.class},
              //02 - REGULAR CUSTOMER WITH A OFFER
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
               * Precondition: The user is a inmigrant with an application list with one item.
               * Return: TRUE
               * Postcondition: The application list is shown with one single item.
              */
                {"inmigrant2", applicationList.size()==1, IndexOutOfBoundsException.class},
              //03 - ALL OFFERS
              /**
               * Description: An actor who is authenticated as a inmigrant must be able to: Post an application in which he or she applies that he?s go
              Precondition: The user is a inmigrant with an application list with more than one item.
               * Return: TRUE
               * Postcondition: The application list is shown with multiple items.
              */
                {"inmigrant3",applicationList.size()>=2, IndexOutOfBoundsException.class}
      };
      for (int i=0; i<testingData.length;i++){
         template2((String) testingData[i][0], (Boolean) testingData[i][1], (Class<?>) testingData[i][2]);
      }
   }

   protected void template2(String user, Boolean ass, Class<?> exc){
      Class<?> caught;
      caught=null;
      try {
         authenticate(user);
         List<Application> applicationList = new ArrayList<>();
         List<Visa> visas = new ArrayList<>(visaService.findAll());
         Visa visa = visas.get(0);
         Inmigrant inmigrant = inmigrantService.findByPrincipal();
         visa.getApplications().add(applicationList.get(0));
         Assert.isTrue(ass);
         authenticate(null);
         applicationService.flush();
      } catch (Throwable oops){
         caught = oops.getClass();
      }
      checkExceptions(exc, caught);
   }
   @Test()
   public void linkTwoApplications() {
      String ticker = new ArrayList<>(applicationService.findAll()).get(0).getTicker();
      String id = String.valueOf(new ArrayList<>(applicationService.findAll()).get(1).getId());
      Integer appId = new Integer(id);
      Application applicationToLink = applicationService.getApplicationByTicker(ticker);
      Application applicationLinked = applicationService.findOne(appId);
   }


   @Test(expected = NumberFormatException.class)
   public void linkTwoApplicationsNegative() {
      String ticker ="";
      String id = "";
      Integer appId = new Integer(id);
      Application applicationToLink = applicationService.getApplicationByTicker(ticker);
      Application applicationLinked = applicationService.findOne(appId);
      applicationLinked.getLinkedApplications().add(applicationToLink);
      applicationToLink.getLinkedApplications().add(applicationLinked);
      applicationService.save(applicationLinked);
      applicationService.save(applicationToLink);
   }


   @Test
   public void linkApplicationsfFromSameCountry() {
      Application application = new ArrayList<>(applicationService.findAll()).get(0);
      Country country = application.getVisa().getCountry();
      Set<Application> applicationsFromCountry = new HashSet<>(applicationService.applicationsFromCountry(country.getId()));
      for (Application a : applicationsFromCountry){
         if(!application.getLinkedApplications().contains(a)){
            application.getLinkedApplications().add(a);
            applicationService.save(a);
            a.getLinkedApplications().add(application);
            applicationService.save(application);
         }
      }

   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void linkApplicationsfFromSameCountryNegative() {
      Application application = new ArrayList<>(applicationService.findAll()).get(110);
      Country country = application.getVisa().getCountry();
      Set<Application> applicationsFromCountry = new HashSet<>(applicationService.applicationsFromCountry(country.getId()));
      for (Application a : applicationsFromCountry){
         if(!application.getLinkedApplications().contains(a)){
            application.getLinkedApplications().add(a);
            applicationService.save(a);
            a.getLinkedApplications().add(application);
            applicationService.save(application);
         }
      }

   }
   @Test
   public void estimatedElapsedTime() {
      Application a = new ArrayList<>(applicationService.findAll()).get(0);
      SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
      Long res;
      try {
         if (a.getCloseDate() == null || a.getStatusChange() == null) {
            res =  0L;
         }
         String inputString1 = myFormat.format(a.getCloseDate());
         String inputString2 = myFormat.format(a.getStatusChange());
         Date date1 = myFormat.parse(inputString1);
         Date date2 = myFormat.parse(inputString2);
         long diff = date2.getTime() - date1.getTime();
         Long days = abs(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
         res =  days;
         Assert.notNull(res);
      } catch (ParseException e) {
         e.printStackTrace();
      }

   }



}