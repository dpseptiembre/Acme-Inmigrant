package services;

import domain.Visa;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utilities.AbstractTest;

import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
public class FinderServiceTest extends AbstractTest {

   @Autowired
   private FinderService finderService;
   @Autowired
   private ActorService actorService;


   @Test
   public void finder(){
      String key = "visa1";
      Collection<Visa> res = finderService.finder(key);
      org.springframework.util.Assert.notEmpty(res);
   }
   @Test
   public void finderRandom(){
      String key = "perri";
      Collection<Visa> res = finderService.finder(key);
      org.springframework.util.Assert.notEmpty(res);
   }
   @Test
   public void finderRandomNumbers(){
      String key = "perri";
      Collection<Visa> res = finderService.finder(key);
      org.springframework.util.Assert.notEmpty(res);
   }
   @Test
   public void finderRandom2(){
      String key = "dhhfd zdhg";
      Collection<Visa> res = finderService.finder(key);
      org.springframework.util.Assert.notEmpty(res);
   }
   @Test
   public void finderEmpty(){
      String key = "";
      Collection<Visa> res = finderService.finder(key);
      org.springframework.util.Assert.notEmpty(res);
   }
}