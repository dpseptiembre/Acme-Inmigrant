package services;

import domain.Category;
import domain.Mezzage;
import domain.Uttils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

   /*    IMPORTS OF NECESSARY SERVICES    */
   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private CategoryService categoryService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private UttilsService uttilsService;




   /*    TESTS    */
   @Test
   public void getUttils() {

      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      if (uttils.isEmpty()){
         throw new NoSuchElementException("The util list is empty");
      }else {
         Uttils first = uttils.get(0);
         Assert.assertNotNull(first);
      }
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void getUttilsNegative() {

      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils first = uttils.get(8);
      Assert.assertNotNull(first);

   }

   @Test
   public void addSpamWord() {
     String word = "nigga";
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils first = uttils.get(0);
      if (first.getSpamWords().contains(word)){
         throw new UnsupportedOperationException("The list contains selected word");
      }else {
         first.getSpamWords().add(word);
         uttilsService.save(first);
      }
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void addSpamWordNegative() {
      String word = "nigga";
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils first = uttils.get(9);
      if (first.getSpamWords().contains(word)){
         throw new UnsupportedOperationException("The list contains selected word");
      }else {
         first.getSpamWords().add(word);
         uttilsService.save(first);
      }
   }

   @Test
   public void deleteSpamWordNe() {
      String word = "ff";
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils first = uttils.get(0);
      if (first.getSpamWords().contains(word)){
         throw new UnsupportedOperationException("The list contains selected word");
      }else {
         first.getSpamWords().remove(word);
         uttilsService.save(first);
      }


   }
   @Test(expected = UnsupportedOperationException.class)
   public void deleteSpamWordNegative() {
      String word = "nigga";
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils first = uttils.get(0);
      if (!first.getSpamWords().contains(word)){
         throw new UnsupportedOperationException("The list contains selected word");
      }else {
         first.getSpamWords().remove(word);
         uttilsService.save(first);
      }
   }

   @Test
   public void changeSearchNumber() {
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils utils = uttils.get(0);
      Integer nume = 3;
      utils.setNumberOfFinderResults(nume);
      uttilsService.save(utils);
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void changeSearchNumberNegative() {
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils utils = uttils.get(78);
      Integer nume = 3;
      utils.setNumberOfFinderResults(nume);
      uttilsService.save(utils);
   }

   @Test
   public void setFinder() {
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils utils = uttils.get(0);
      Integer nume = 3;
      utils.setFinderCarche(nume);
      uttilsService.save(utils);
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void setFinderNegative() {
      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils utils = uttils.get(50);
      Integer nume = 3;
      utils.setFinderCarche(nume);
      uttilsService.save(utils);
   }

   @Test
   public void customize() {

      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils utils = uttils.get(0);
      String banner = "http://perri.png";
      String systemName = "perri";
      String welcome = "Hello perri";
      String welcomeES = "Hola perri";
      utils.setBanner(banner);
      utils.setSystemName(systemName);
      utils.setWelcomeMezzage(welcome);
      utils.setWelcomeMezzageES(welcomeES);
      uttilsService.save(utils);
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void customizeNegative() {

      List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
      Uttils utils = uttils.get(78);
      String banner = "http://perri.png";
      String systemName = "perri";
      String welcome = "Hello perri";
      String welcomeES = "Hola perri";
      utils.setBanner(banner);
      utils.setSystemName(systemName);
      utils.setWelcomeMezzage(welcome);
      utils.setWelcomeMezzageES(welcomeES);
      uttilsService.save(utils);
   }


   @Test
   public void createCategory() {
      authenticate("administrator1");
      Category category =categoryService.create();
      Assert.assertNotNull(category);
   }

   @Test
   public void editCategory() {
      authenticate("administrator1");
      Category category =categoryService.create();
      Assert.assertNotNull(category);
      Category category2 = categoryService.save(category);
      Assert.assertNotNull(category2);
   }

   @Test
   public void deleteCategory() {
      authenticate("administrator1");
      Category category =categoryService.create();
      category.setName("nsda");
      category.setDescription("fgdfgdfgd");
      category.setFather(new ArrayList<>(categoryService.findAll()).get(0));
      category.setHidden(true);
      categoryService.save(category);

   }

   @Test
   public void storeBroadCastMessage() {
      authenticate("administrator1");
      Mezzage m = mezzageService.create();
      mezzageService.createActionMessage("Perro","prueba",administratorService.findByPrincipal().getId());
      Assert.assertNotNull(administratorService.findByPrincipal().getId());
   }


   @Test
   public void createChart(){
      String res = administratorService.createChart();
      Assert.assertNotNull(res);
   }

}