package services;

import domain.Country;
import domain.Visa;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class VisaServiceTest extends AbstractTest {
   @Autowired
   private VisaService visaService;
   @Autowired
   private CountryService countryService;
   @Autowired
   private InmigrantService inmigrantService;

   @Test
   public void countriesFromVisa() {
      Visa visa = new ArrayList<>(visaService.findAll()).get(0);
      Collection<Country> countries = visaService.countriesFromVisa(visa);
      Assert.notEmpty(countries);
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void countriesFromVisaNegative() {
      Visa visa = new ArrayList<>(visaService.findAll()).get(100);
      Collection<Country> countries = visaService.countriesFromVisa(visa);
      Assert.notEmpty(countries);
   }

   @Test
   public void visasFromCountry() {
      Country country = new ArrayList<>(countryService.findAll()).get(0);
      Collection<Visa> visas = visaService.visasFromCountry(country.getId());
      org.junit.Assert.assertNotNull(visas);
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void visasFromCountryNegative() {
      Country country = new ArrayList<>(countryService.findAll()).get(100);
      Collection<Visa> visas = visaService.visasFromCountry(country.getId());
      org.junit.Assert.assertNotNull(visas);
   }

   @Test
   public void myVisas() {
      authenticate("inmigrant1");
      Collection<Visa> myVisas = visaService.myVisas(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(myVisas);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void myVisasNegative() {
      authenticate("");
      Collection<Visa> myVisas = visaService.myVisas(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(myVisas);
      unauthenticate();
   }
}