package services;

import domain.Requirement;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class RequirementServiceTest extends AbstractTest {

   @Autowired
   private RequirementService requirementService;
   @Test
   public void notHiddenRequirements() {
      Collection<Requirement> requirements = requirementService.notHiddenRequirements();
      Assert.assertNotNull(requirements);
   }
}