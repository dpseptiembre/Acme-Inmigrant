package services;

import domain.Actor;
import domain.Administrator;
import domain.Inmigrant;
import domain.Mezzage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import java.util.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class MezzageServiceTest extends AbstractTest {

    // System under test ------------------------------------------------------

    @Autowired
    private MezzageService messageService;
    @Autowired
    private InmigrantService customerService;
    @Autowired
    private AdministratorService administratorService;
    @Autowired
    private ActorService actorService;
    
    // Tests ------------------------------------------------------------------


  
    @Test
    public void saveFromInmigrantToAdminOk() {
       authenticate("inmigrant1");
       List<Administrator> admins = new ArrayList<>(administratorService.findAll());
       Actor receiver = admins.get(0);
       Actor saveer = customerService.findByPrincipal();
       Mezzage m = messageService.create();
       m.setSender(saveer);
       m.setReceiver(receiver);
       m.setBody("BODY");
       m.setSubject("SUBJECT");
       m.setSendDate(new Date(System.currentTimeMillis() - 1000));
       Collection<String> att = new HashSet<>();
       att.add("dsf");
       m.setAttachments(att);
       messageService.save(m);
       authenticate(null);
    }
    
    @Test
    public void saveFromAdminToInmigratOk(){
        authenticate("administrator1");
        List<Inmigrant> customers = new ArrayList<>(customerService.findAll());
        Actor receiver = customers.get(3);
        Actor saveer = administratorService.findByPrincipal();
        Mezzage m = messageService.create();
        m.setSender(saveer);
        m.setReceiver(receiver);
        m.setBody("BODIES");
        m.setSubject("SUBJECTS");
        m.setSendDate(new Date(System.currentTimeMillis() - 1000));
        Collection<String> att = new HashSet<>();
        att.add("dsfs");
        m.setAttachments(att);
        messageService.save(m);
        authenticate(null);
        
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void saveFromAdminToAdminOk(){
        authenticate("administrator1");
        List<Administrator> admins = new ArrayList<>(administratorService.findAll());
        Actor receiver = admins.get(3);
        Actor saveer = administratorService.findByPrincipal();
        Mezzage m = messageService.create();
        m.setSender(saveer);
        m.setReceiver(receiver);
        m.setBody("BODYY");
        m.setSubject("SUBJECTT");
        m.setSendDate(new Date(System.currentTimeMillis() - 1000));
        Collection<String> att = new HashSet<>();
        att.add("dsfff");
        m.setAttachments(att);
        messageService.save(m);
        authenticate(null);
        
    }
    @Test(expected = IllegalArgumentException.class)
    public void saveFromInmigrantToInmigrantNoOk(){
        authenticate("customer1");
        List<Inmigrant> customers = new ArrayList<>(customerService.findAll());
        Actor receiver = customers.get(3);
        Actor saveer = customerService.findByPrincipal();
        Mezzage m = messageService.create();
        messageService.save(m);
        authenticate(null);

    }
    @Test(expected = IllegalArgumentException.class)
    public void saveFromInmigrantToAdminNoOk(){
        authenticate("customer1");
        List<Administrator> admins = new ArrayList<>(administratorService.findAll());
        Actor receiver = admins.get(3);
        Actor saveer = customerService.findByPrincipal();
        Mezzage m = messageService.create();
        Collection<String> att = new HashSet<>();
        att.add("dsf");
        m.setAttachments(att);
        messageService.save(m);
        authenticate(null);

    }
    @Test
    public void saveFromAdminToInmigrantOk(){
        authenticate("administrator1");
        List<Inmigrant> customers = new ArrayList<>(customerService.findAll());
        Actor receiver = customers.get(0);
        Actor saveer = administratorService.findByPrincipal();
        Mezzage m = messageService.create();
        messageService.save(m);
        authenticate(null);

    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void saveFromAdminToAdminNoOk(){
        authenticate("administrator1");
        List<Administrator> admins = new ArrayList<>(administratorService.findAll());
        Actor receiver = admins.get(3);
        Actor saveer = administratorService.findByPrincipal();
        Mezzage m = messageService.create();
        messageService.save(m);
        authenticate(null);

    }

    // The following are fictitious test cases that are intended to check that
    // JUnit works well in this project.  Just righ-click this class and run
    // it using JUnit.

    @Test
    public void samplePositiveTest() {
        Assert.isTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sampleNegativeTest() {
        Assert.isTrue(false);
    }

    // Ancillary methods ------------------------------------------------------

}