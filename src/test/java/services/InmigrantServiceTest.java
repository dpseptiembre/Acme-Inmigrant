package services;

import domain.Application;
import domain.Inmigrant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import security.UserAccountService;
import utilities.AbstractTest;

import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
public class InmigrantServiceTest extends AbstractTest {

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private FolderService folderService;



   @Test
   public void registerAsInmigrantPositive() {
      Inmigrant u = inmigrantService.create();
      Assert.notNull(u);
      u.getUserAccount().setUsername("kkkkkk");
      u.setName("kkkkkk");
      u.setSurname("sdfsdf");
      u.setEmail("perri@gjail.com");
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword("kkkkkk", null);
      u.getUserAccount().setPassword(hash);
      org.junit.Assert.assertNotNull(u);
   }


   @Test
   public void opennedAppsByImmigrantId() {
      authenticate("inmigrant1");
      Collection<Application> applications =inmigrantService.opennedAppsByImmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void opennedAppsByImmigrantINegative() {
      authenticate("");
      Collection<Application> applications =inmigrantService.opennedAppsByImmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test
   public void closedAndAcceptedAppsByInmigrantId() {
      authenticate("inmigrant1");
      Collection<Application> applications =inmigrantService.closedAndAcceptedAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void closedAndAcceptedAppsByInmigrantIdNegative() {
      authenticate("");
      Collection<Application> applications =inmigrantService.closedAndAcceptedAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test
   public void closedAndDeniedAppsByInmigrantId() {
      authenticate("inmigrant1");
      Collection<Application> applications =inmigrantService.closedAndDeniedAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void closedAndDeniedAppsByInmigrantIdNegative() {
      authenticate("");
      Collection<Application> applications =inmigrantService.closedAndDeniedAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test
   public void closedAndAwaitingAppsByInmigrantId() {
      authenticate("inmigrant1");
      Collection<Application> applications =inmigrantService.closedAndAwaitingAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void closedAndAwaitingAppsByInmigrantIdNegative() {
      authenticate("");
      Collection<Application> applications =inmigrantService.closedAndAwaitingAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      org.junit.Assert.assertNotNull(applications);
      unauthenticate();
   }
}