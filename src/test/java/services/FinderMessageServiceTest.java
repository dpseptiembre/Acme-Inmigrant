package services;

import domain.FinderMessage;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import java.util.Collection;
import java.util.Date;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
public class FinderMessageServiceTest extends AbstractTest {

   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private FinderMessageService finderMessageService;
   @Autowired
   private ActorService actorService;

   @Test
   public void finderMessagesPerActorInmigrant() {
      authenticate("inmigrant1");
      int actorId = actorService.findByPrincipal().getId();
      int numbeOfCacheHours = administratorService.getUttils().getFinderCarche();
      Date cacheprev = new DateTime().minusHours(numbeOfCacheHours).toDate();
      Assert.notNull(actorId, "Actor id is null");
      Collection<FinderMessage> mezzages = finderMessageService.finderMessagesPerActor();
      org.junit.Assert.assertNotNull(mezzages);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void finderMessagesPerActorInmigrantNegative() {
      authenticate("");
      int actorId = actorService.findByPrincipal().getId();
      int numbeOfCacheHours = administratorService.getUttils().getFinderCarche();
      Date cacheprev = new DateTime().minusHours(numbeOfCacheHours).toDate();
      Assert.notNull(actorId, "Actor id is null");
      Collection<FinderMessage> mezzages = finderMessageService.finderMessagesPerActor();
      org.junit.Assert.assertNotNull(mezzages);
      unauthenticate();
   }

   @Test
   public void finderMessagesPerActorOfficer() {
      authenticate("officer1");
      int actorId = actorService.findByPrincipal().getId();
      int numbeOfCacheHours = administratorService.getUttils().getFinderCarche();
      Date cacheprev = new DateTime().minusHours(numbeOfCacheHours).toDate();
      Assert.notNull(actorId, "Actor id is null");
      Collection<FinderMessage> mezzages = finderMessageService.finderMessagesPerActor();
      org.junit.Assert.assertNotNull(mezzages);
      unauthenticate();
   }

   @Test
   public void finderMessagesPerActorAdmin() {
      authenticate("administrator1");
      int actorId = actorService.findByPrincipal().getId();
      int numbeOfCacheHours = administratorService.getUttils().getFinderCarche();
      Date cacheprev = new DateTime().minusHours(numbeOfCacheHours).toDate();
      Assert.notNull(actorId, "Actor id is null");
      Collection<FinderMessage> mezzages = finderMessageService.finderMessagesPerActor();
      org.junit.Assert.assertNotNull(mezzages);
      unauthenticate();
   }

}