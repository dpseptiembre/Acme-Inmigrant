package services;

import domain.Folder;
import domain.FolderType;
import domain.Inmigrant;
import domain.Investigator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;

import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
public class InvestigatorServiceTest extends AbstractTest {
   @Autowired
   InvestigatorService investigatorService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private UserAccountService userAccountService;

   @Test
   public void registerAsInvestigator() {
      Investigator u = investigatorService.create();
      Assert.notNull(u);
      u.getUserAccount().setUsername("kkkkkk");
      u.setName("kkkkkk");
      u.setSurname("sdfsdf");
      u.setEmail("perri@gjail.com");
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword("kkkkkk", null);
      u.getUserAccount().setPassword(hash);
      org.junit.Assert.assertNotNull(u);
   }

   @Test(expected = DataIntegrityViolationException.class)
   public void registerAsInvestigatorNegative() {
      Investigator u = investigatorService.create();
      u.setName("perrito");
      u.getUserAccount().setUsername("perrito");
      u.getUserAccount().setPassword("perrito");
      Assert.notNull(u);
      u.getUserAccount().setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      u.getUserAccount().setPassword(hash);
      UserAccount userAccount = userAccountService.save(u.getUserAccount());
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Investigator resu = investigatorService.save(u);
      Folder folder = folderService.create();
      folder.setFolderType(FolderType.PUBLIC);
      folder.setName("public");
      folder.setOwner(resu);
      folderService.save(folder);
      Folder folder1 = folderService.create();
      folder1.setFolderType(FolderType.INBOX);
      folder1.setOwner(resu);
      folder1.setName("inbox");
      folderService.save(folder1);
      org.junit.Assert.assertNotNull(resu);
   }
   @Test
   public void underInvestigation() {
      authenticate("investigator1");
      Collection<Inmigrant> inmigrants = investigatorService.underInvestigation();
      org.junit.Assert.assertNotNull(inmigrants);
      unauthenticate();
   }
   @Test(expected = IllegalArgumentException.class)
   public void underInvestigationNegative() {
      Collection<Inmigrant> inmigrants = investigatorService.underInvestigation();
      org.junit.Assert.assertNotNull(inmigrants);
   }
}