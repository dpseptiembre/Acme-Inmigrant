package services;

import domain.CreditCard;
import domain.CreditCardType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class CreditCardServiceTest extends AbstractTest {

//   @Before
//   public void setUp() throws Exception {
//   }
//
//   @After
//   public void tearDown() throws Exception {
//   }
   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private InmigrantService inmigrantService;

   @Test
   public void checkCreditCardOk() {
      authenticate("inmigrant1");
      CreditCard creditCard = creditCardService.create();
      creditCard.setCVV("345");
      creditCard.setHolderName("err");
      creditCard.setMonth(1);
      creditCard.setYear(2020);
      creditCard.setNumber("5489018194198017");
      creditCard.setOwner(inmigrantService.findByPrincipal());
      creditCard.setType(CreditCardType.DINNERS);
      CreditCard cc = creditCardService.save(creditCard);
      boolean res  = creditCardService.checkCreditCard2(cc);
      Assert.isTrue(res);
   }

   @Test
   public void checkCreditCardOk2() {
      authenticate("inmigrant2");
      CreditCard creditCard = creditCardService.create();
      creditCard.setCVV("345");
      creditCard.setHolderName("err");
      creditCard.setMonth(3);
      creditCard.setYear(2020);
      creditCard.setNumber("5489018194198017");
      creditCard.setOwner(inmigrantService.findByPrincipal());
      creditCard.setType(CreditCardType.DINNERS);
      CreditCard cc = creditCardService.save(creditCard);
      boolean res  = creditCardService.checkCreditCard2(cc);
      Assert.isTrue(res);
   }


   @Test(expected = IllegalArgumentException.class)
   public void checkCreditCard3MParaCaducar() {
      authenticate("inmigrant2");
      CreditCard creditCard = creditCardService.create();
      creditCard.setCVV("345");
      creditCard.setHolderName("err");
      creditCard.setMonth(10);
      creditCard.setYear(2018);
      creditCard.setNumber("5489018194198017");
      creditCard.setOwner(inmigrantService.findByPrincipal());
      creditCard.setType(CreditCardType.DINNERS);
      CreditCard cc = creditCardService.save(creditCard);
      boolean res  = creditCardService.checkCreditCard2(cc);
      Assert.isTrue(res);
   }

   @Test(expected = IllegalArgumentException.class)
   public void checkCreditCardCaducada() {
      authenticate("inmigrant2");
      CreditCard creditCard = creditCardService.create();
      creditCard.setCVV("345");
      creditCard.setHolderName("err");
      creditCard.setMonth(10);
      creditCard.setYear(2017);
      creditCard.setNumber("5489018194198017");
      creditCard.setOwner(inmigrantService.findByPrincipal());
      creditCard.setType(CreditCardType.DINNERS);
      CreditCard cc = creditCardService.save(creditCard);
      boolean res  = creditCardService.checkCreditCard2(cc);
      Assert.isTrue(res);
   }
}


