package services;

import domain.Application;
import domain.Question;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
public class QuestionServiceTest extends AbstractTest {
   @Autowired
   private QuestionService questionService;
   @Autowired
   private ApplicationService applicationService;


   @Test
   public void questionsOfApplication() {
      Application application = new ArrayList<>(applicationService.findAll()).get(0);
      Collection<Question> questions = questionService.questionsOfApplication(application.getId());
      Assert.assertNotNull(questions);
   }
   @Test(expected = IndexOutOfBoundsException.class)
   public void questionsOfApplicationNegative() {
      Application application = new ArrayList<>(applicationService.findAll()).get(100);
      Collection<Question> questions = questionService.questionsOfApplication(application.getId());
      Assert.assertNotNull(questions);
   }
}