<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 19/12/16
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<br>

<div class="container">
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q1" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q1}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q2" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q2}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q3" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q3}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q4" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q4}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q5" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q5}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q6" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q6}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q7" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q7}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q8" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q8}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q10" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q10}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q11" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q11}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q12" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q12}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q13" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${13}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q14" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q14}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q15" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q15}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q16" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q16}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q17" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q17}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>

<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q18" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q18}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q20" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q20}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q21" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q21}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q22" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q22}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>


<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q23" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q23}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q24" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q24}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q25" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q25}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q26" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q26}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>

<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q27" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q27}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q28" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q28}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q29" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q29}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q30" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q30}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>



<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q31" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q31}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q32" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q32}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q33" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q33}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q34" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q34}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

</div>

<hr>
<div class="card-deck">
    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q35" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q35}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q36" var="q2b"/>
                <jstl:out value="${q2b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q36}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q37" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q37}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q38" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q38}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <spring:message code="dashboard.q39" var="q1b"/>
                <jstl:out value="${q1b}"/>:
            </h5>
            <h1 class="card-text"><jstl:out value="${q39}"/></h1>
            <p class="card-text"><small class="text-muted">Last updated just now</small></p>
        </div>
    </div>
    <!--/.Panel-->


</div>



<hr>

<h5 class="card-title">
    <spring:message code="dashboard.q40" var="q1b"/>
    <jstl:out value="${q1b}"/>:
</h5>
<p class="card-text"><small class="text-muted">
    <spring:message code="dashboard.q41" var="q1b"/>
    <jstl:out value="${q1b}"/>:
</small></p>


    <script type="text/javascript">
        window.onload = function () {


            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                exportEnabled: true,
                    axisY: {
                        stripLines: [
                            {
                                value:${min00},
                                label: "min"
                            },
                            {
                                value:${max00},
                                label: "max"
                            },
                            {
                                value:${avg00},
                                label: "avg"
                            },
                            {
                                value:${std00},
                                label: "std dev"
                            }
                        ],
                        valueFormatString: "####"
                    },
                title: {
                    text: "Elapsed time"
                },
                data: [{
                    type: "line", //change type to bar, line, area, pie, etc
                    //indexLabel: "{y}", //Shows y value on all Data Points
                    indexLabelFontColor: "#5A5757",
                    indexLabelPlacement: "outside",
                    dataPoints: ${dataP},

                }]

                }
            );
            chart.render();

        }


    </script>


</div>
<div class=?container?>
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</div>