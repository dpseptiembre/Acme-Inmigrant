<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
<security:authorize access="hasRole('ADMINISTRATOR')">
    <a class="btn btn-primary" href="category/create.do"> <spring:message code="general.create" /></a>
</security:authorize>
<!-- Listing grid -->
<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="categories" requestURI="${requestURI}" id="row">


    <display:column>
        <a class="button2" class="btn btn-primary" href="category/view.do?categoryId=${row.id}"> <spring:message
                code="general.view"/></a>
    </display:column>

    <spring:message code="cat.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="cat.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a class="button2" href="category/edit.do?categoryId=${row.id}"> <spring:message code="general.edit" />
            </a>
        </display:column>
        <display:column>
            <a class="button2" href="category/delete.do?categoryId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>')"> <spring:message code="visa.delete"/></a>
        </display:column>

    </security:authorize>

</display:table>

</div>