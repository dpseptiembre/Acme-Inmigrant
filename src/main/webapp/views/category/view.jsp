<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">


<%--<security:authorize access="hasRole('ADMINISTRATOR')">--%>
    <%--<a class="btn btn-primary" href="category/create.do"> <spring:message code="general.create" /></a>--%>
<%--</security:authorize>--%>
<!-- Listing grid -->

   <h4><jstl:out value="${cat.name}"/></h4>
    <%--<h2><jstl:out value="${cat.father}"/></h2>--%>


    <%--<spring:message code="cat.description" var="description1"/>--%>
    <%--<jstl:out value="${description1}"/></p>--%>
    <p><jstl:out value="${cat.description}"/></p>


    <spring:message code="cat.father" var="father1"/>
    <h3><jstl:out value="${father1}"/></h3>
    <jstl:out value="${cat.father}"/>

    <spring:message code="cat.sons" var="sons1"/>
    <h3><jstl:out value="${sons1}"/></h3>
    <display:table pagesize="10" class="displaytag" keepStatus="true"
                   name="sonnz" requestURI="${requestURIsonnz}" id="row">

        <display:column>
            <a class="button2" class="btn btn-primary" href="category/view.do?categoryId=${row.id}"> <spring:message
                    code="general.view"/></a>
        </display:column>

        <spring:message code="cat.name" var="name"/>
        <display:column property="name" title="${name}" sortable="true"/>
        <spring:message code="cat.description" var="description"/>
        <display:column property="description" title="${description}" sortable="true"/>


    </display:table>

    <spring:message code="cat.visas" var="visas1"/>
    <h4><jstl:out value="${visas1}"/></h4>

<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="cat.visas" requestURI="${requestURI}" id="row">
    <security:authorize access="isAnonymous()">
        <display:column>
            <a class="btn btn-primary" href="visa/viewAnon.do?visaId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="isAuthenticated()">
        <display:column>
            <a class="btn btn-primary" href="visa/viewAuthenticated.do?visaId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="visa.clazz" var="clazz"/>
    <display:column property="clazz" title="${clazz}" sortable="true"/>

    <spring:message code="visa.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="visa.price" var="price"/>
    <display:column property="price" title="${price}" sortable="true"/>

    <spring:message code="visa.category" var="category"/>
    <display:column property="category" title="${category}" sortable="true"/>

    <spring:message code="visa.application" var="application"/>
    <display:column property="applications" title="${application}" sortable="true"/>

    <br>

</display:table>

</div>