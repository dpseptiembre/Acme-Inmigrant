<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
<form:form action="category/edit.do" modelAttribute="category">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="hidden"/>

    <acme:textbox path="name" code="cat.name"/>
    <br/>
    <acme:textbox path="description" code="cat.description"/>
    <br/>
    <jstl:if test="${nm != 'category 0'}">
        <acme:select path="father" code="cat.father" items="${categories}" itemLabel="name"/>
    </jstl:if>
    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save" class="btn btn-primary"
           value="<spring:message code="visa.save"/>"
           onclick="return confirm('<spring:message code="visa.confirm.save"/>')"/>&nbsp;

    <input type="button" name="cancel" class="button2"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('category/list.do');"/>
    <br/>


</form:form>
</div>