 <%--
 * login.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<div class="container">
<form:form action="j_spring_security_check" modelAttribute="credentials">

	<form:label cssClass="form-group" path="username">
		<spring:message code="security.username" />
	</form:label>
	<form:input cssClass="form-control" path="username" />
	<form:errors class="error" cssClass="error" path="username" />
	<br />

	<form:label cssClass="form-group" path="password">
		<spring:message code="security.password" />
	</form:label>
	<form:password cssClass="form-control" path="password" />
	<form:errors class="error" cssClass="error" path="password" />
	<br />
	
	<jstl:if test="${showError == true}">
		<div class="error" cssClass="error">
			<spring:message code="security.login.failed" />
		</div>
	</jstl:if>
	
	<input type="submit" class="btn btn-info" value="<spring:message code="security.login" />" />
	
</form:form>
</div>