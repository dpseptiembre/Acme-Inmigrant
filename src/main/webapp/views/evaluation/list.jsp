<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">

<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="evaluations" requestURI="${requestURI}" id="row">
<c:set var="localeCode" value="${pageContext.response.locale}" />


    <spring:message code="evaluation.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="evaluation.postDate" var="postDate"/>
    		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="postDate" title="${postDate}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="postDate" title="${postDate}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

    <spring:message code="evaluation.mark" var="mark"/>
    <display:column property="mark" title="${mark}" sortable="true"/>

    <spring:message code="evaluation.owner" var="owner"/>
    <display:column property="owner" title="${owner}" sortable="true"/>


</display:table>

</div>