<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="evaluation/edit.do" modelAttribute="evaluation">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="owner"/>
    <form:hidden path="receiver"/>
    <form:hidden path="postDate"/>



    <acme:textbox path="description" code="evaluation.description"/>
    <br/>

    <form:label path="mark">
        <spring:message code="evaluation.mark"/>:
    </form:label>
    <br/>

    <form:select path="mark" code="mezzage.priority">
        <form:options/>
    </form:select>
    <br/><br/>
    <br/>
    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save"
           value="<spring:message code="visa.save"/>"
           onclick="return confirm('<spring:message code="visa.confirm.save"/>')"/>&nbsp;

    <input type="button" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>


</form:form>
