<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="officer/edit.do" modelAttribute="officer">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="userAccount"/>

    <acme:textbox path="name" code="inmigrant.name"/>
    <acme:textbox path="surname" code="inmigrant.surname"/>
    <acme:textbox path="email" code="inmigrant.email"/>
    <acme:textbox path="phone" code="inmigrant.phone"/>
    <acme:textbox path="address" code="inmigrant.address"/>



    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>

    <input type="button" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('/');"/>
    <br/>

</form:form>