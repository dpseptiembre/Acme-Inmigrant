<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="officers" requestURI="${requestURI}" id="row">
    <spring:message code="inmigrant.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="inmigrant.surname" var="surname"/>
    <display:column property="surname" title="${surname}" sortable="true"/>
    <spring:message code="inmigrant.email" var="email"/>
    <display:column property="email" title="${email}" sortable="true"/>
    <spring:message code="inmigrant.phone" var="phoneNumber"/>
    <display:column property="phone" title="${phoneNumber}" sortable="true"/>
    <spring:message code="officer.supervision" var="underSupervision1"/>
    <display:column property="underSupervision" title="${underSupervision}" sortable="true"/>

    <jstl:if test="${isMy}">
        <display:column>
            <a href="evaluation/create.do?officerId=${row.id}" class="btn btn-primary">
                <spring:message code="officer.evaluate"/>
            </a>
        </display:column>
    </jstl:if>
</display:table>