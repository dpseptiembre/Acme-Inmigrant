<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="findermessages" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />


	<!-- Attributes -->


	<display:column>
		<a href="finderMessage/view.do?finderMessageId=${row.id}"> <spring:message
				code="general.view" />
		</a>
	</display:column>


	<spring:message code="finder.keyword" var="keyword" />
	<display:column property="keyword" title="${keyword}" sortable="true" />

	<spring:message code="finder.startDate" var="startDate" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="startDate" title="${startDate}"
				sortable="true" format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="startDate" title="${startDate}"
				sortable="true" format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>


	<spring:message code="finder.endDate" var="endDate" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="endDate" title="${endDate}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="endDate" title="${endDate}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>


</display:table>