<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->
<div class="container">


	<form:form action="mezzage/finder.do" method="get">
		<input placeholder="Keyword" name="keyword" class="form-group" type="text"/>
		<input placeholder="Start Date" name="startDate" class="form-group" type="date"/>
		<input placeholder="Start Date" name="endDate" class="form-group" type="date"/>
		<input type="submit" value="<spring:message code="finder.seach"/>" class="btn btn-primary"/>
	</form:form>

	<a href="finderMessage/list.do" class="btn btn-primary"><spring:message code="my.finders"/></a>

</div>