<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
<form:form action="mezzage/edit.do" modelAttribute="mezzage">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="senderEmail"/>
    <form:hidden path="sender"/>
    <%--<form:hidden path="receiver"/>--%>
    <form:hidden path="sendDate"/>
    <form:hidden path="attachments"/>
    <%--<form:hidden path="folder"/>--%>
    <form:hidden path="spam"/>



    <acme:textbox path="receiverEmail" code="mezzage.receiverEmail"/>
    <acme:textbox path="subject" code="mezzage.subject"/>
    <acme:textarea path="body" code="mezzage.body"/>



    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save"
           value="<spring:message code="visa.save"/>')"/>&nbsp;

    <%--<acme:submit name="save" code="law.save"/>--%>

    <%--<jstl:if test="\$\{law.id != 0}">--%>
    <%--<input type="submit" name="delete"--%>
    <%--value="<spring:message code="law.delete" />"--%>
    <%--onclick="return confirm('<spring:message code="law.confirm.delete" />')" />&nbsp;--%>
    <%--</jstl:if>--%>
    <input type="button" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>


</form:form>
</div>