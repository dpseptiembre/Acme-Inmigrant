<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->



<div class="row">
	<div class="col-md-8 "><jstl:out value="${m.body}"/></div>
	<div class="col-md-4">

		<spring:message code="mezzage.subject" var="subjecta"/>
		<h2><jstl:out value="${subjecta}"/></h2>
		<jstl:out value="${m.subject}"/>
		<br>
		<spring:message code="mezzage.sender" var="senderEmaila"/>
		<h5><jstl:out value="${senderEmaila}"/></h5>
		<jstl:out value="${m.senderEmail}"/>
		<br>
		<spring:message code="mezzage.recipient" var="receiverEmaila"/>
		<h5><jstl:out value="${receiverEmaila}"/></h5>
		<jstl:out value="${m.receiverEmail}"/>
		<br>
		<spring:message code="mezzage.sendMomment" var="sendDatea"/>
		<h5><jstl:out value="${sendDatea}"/></h5>
		<jstl:out value="${m.sendDate}"/>
		<hr>

		<security:authorize access="hasRole('ADMINISTRATOR')">
            <a href="mezzage/delete.do?mezzageId=${m.id}" class="btn btn-primary"
               onclick="return confirm('<spring:message code="delete.confirm"/>')"><spring:message
                    code="general.delete"/></a>
		</security:authorize>


	</div>
</div>