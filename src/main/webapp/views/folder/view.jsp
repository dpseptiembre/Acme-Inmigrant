<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->



<div class="container">


	<h2><jstl:out value="${name}"/></h2>

	<display:table pagesize="5" class="displaytag" keepStatus="true"
				   name="mezzages" requestURI="mezzage/list.do" id="row">

		<!-- Attributes -->

		<spring:message code="mezzage.subject" var="subject" />
		<display:column property="subject" title="${subject}" sortable="false" />
		<spring:message code="mezzage.sender" var="senderEmail" />
		<display:column property="senderEmail" title="${senderEmail}" sortable="false" />
		<spring:message code="mezzage.recipient" var="receiverEmail" />
		<display:column property="receiverEmail" title="${receiverEmail}" sortable="false" />
		<spring:message code="mezzage.sendMomment" var="sendDate" />
		<display:column property="sendDate" title="${sendDate}" sortable="false" />


		<display:column>
			<a href="mezzage/view.do?mezzageId=${row.id}" class="btn btn-primary">
				<spring:message code="general.view"/>
			</a>
		</display:column>

	</display:table>

</div>