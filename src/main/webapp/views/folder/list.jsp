<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
<!-- Listing grid -->
    <a href="folder/create.do" class="btn btn-primary">
        <spring:message code="general.create"/>
    </a>
    <a href="mezzage/create.do" class="btn btn-primary">
        <spring:message code="mezzage.new"/>
    </a>

<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="folders" requestURI="${requestURI}" id="row">


    <spring:message code="folder.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>

    <spring:message code="folder.folderType" var="folderType"/>
    <display:column property="folderType" title="${folderType}" sortable="true"/>

    <display:column>
        <a href="folder/view.do?folderId=${row.id}" class="btn btn-primary">
            <spring:message code="general.view"/>
        </a>
    </display:column>


</display:table>
</div>