<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
<form:form action="folder/edit.do" modelAttribute="folder">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="owner"/>
    <form:hidden path="mezzages"/>

    <acme:textbox path="name" code="folder.name"/>
    <br/>
    <form:label path="folderType">
        <spring:message code="folder.folderType"/>:
    </form:label>
    <form:select path="folderType" code="folder.folderType">
        <form:options/>
    </form:select>


    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save"
           value="<spring:message code="visa.save"/>"
           onclick="return confirm('<spring:message code="visa.confirm.save"/>')"/>&nbsp;

    <%--<acme:submit name="save" code="law.save"/>--%>

    <%--<jstl:if test="\$\{law.id != 0}">--%>
    <%--<input type="submit" name="delete"--%>
    <%--value="<spring:message code="law.delete" />"--%>
    <%--onclick="return confirm('<spring:message code="law.confirm.delete" />')" />&nbsp;--%>
    <%--</jstl:if>--%>
    <input type="button" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>


</form:form>
</div>