<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
<security:authorize access="hasRole('ADMINISTRATOR')">
    <a class="btn btn-primary" href="country/create.do"> <spring:message code="country.create" /></a>
</security:authorize>
<!-- Listing grid -->
<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="countries" requestURI="${requestURI}" id="row">


    <security:authorize access="permitAll()">
        <display:column>
            <a class="btn btn-primary" href="visa/listCountriesFromVIsa.do?countryId=${row.id}"> <spring:message
                    code="visa.view"/>
            </a>
        </display:column>

        <display:column>
            <a class="button2" href="country/view.do?countryId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>

    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a class="btn btn-primary" href="country/edit.do?countryId=${row.id}"> <spring:message code="visa.edit"/>
            </a>
        </display:column>
        <display:column>
            <a class="btn btn-danger" href="country/delete.do?countryId=${row.id}"
               onclick="return confirm('<spring:message code="general.confirm"/>')"> <spring:message
                    code="visa.delete"/></a>
        </display:column>
    </security:authorize>


    <spring:message code="country.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>

    <spring:message code="country.isoCode" var="isoCode"/>
    <display:column property="isoCode" title="${isoCode}" sortable="true"/>

    <display:column title="${flag}">
        <img src="${row.flag}" width="130" height="100">
    </display:column>

    <%--<spring:message code="country.wikiLink" var="wikiLink"/>--%>
    <%--<display:column property="wikiLink" title="${wikiLink}" sortable="true"/>--%>
    <%--<a href="${wikiLink}"><spring:message code="country.wikiLink"/></a>--%>

    <%--<display:column title="${wikiLink}">--%>
    <%--<a href="${row.wikiLink}">--%>
    <%--<spring:message code="country.wikiLink" var="wikiLink"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>

    <%--<spring:message code="country.legislation" var="legislation"/>--%>
    <%--<display:column property="legislation" title="${legislation}" sortable="true"/>--%>

    <%--<spring:message code="country.offeredVisas" var="offeredVisas"/>--%>
    <%--<display:column property="offeredVisas" title="${offeredVisas}" sortable="true"/>--%>


</display:table>
</div>