<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jstl:if test="${localeCode == 'en'}">
    <c:set value="en" var="loc"></c:set>
</jstl:if>
<jstl:if test="${localeCode == 'es'}">
    <c:set value="es" var="loc"></c:set>
</jstl:if>


<div class="container">

<div class="row">
    <div class="col">
        <h5 style="color: #737476"><spring:message code="country.name"/></h5>
        <h1 class="card-text"><jstl:out value="${c.name}"/></h1>
    </div>
    <div class="col">
        <h5 style="color: #737476"><spring:message code="country.isoCode"/></h5>
        <h1 class="card-text"><jstl:out value="${c.isoCode}"/></h1>
    </div>
    <div class="col">
        <h5 style="color: #737476"><spring:message code="country.flag"/></h5>
        <img src="${c.flag}" width="130" height="100">
    </div>
</div>
    <hr>

    <script type="text/javascript">
        $(document).ready(function(){

            $.ajax({
                type: "GET",
                url: "https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text&section=0&page=${page}&callback=?",
                contentType: "application/json; charset=utf-8",
                async: false,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {

                    var markup = data.parse.text["*"];
                    var blurb = $('<div></div>').html(markup);
                    $('#article').html($(blurb).find('p'));

                },
                error: function (errorMessage) {
                }
            });
        });
    </script>

    <h3><spring:message code="country.description"/></h3>

    <div id="article"></div>
</div>
