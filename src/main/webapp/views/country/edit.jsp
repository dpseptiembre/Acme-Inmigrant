<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>

<div class="container">
<form:form action="country/edit.do" modelAttribute="country">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="legislation"/>
    <form:hidden path="offeredVisas"/>



    <acme:textbox path="name" code="country.name"/>
    <br/>
    <acme:textbox path="isoCode" code="country.isoCode"/>
    <br/>
    <acme:textbox path="flag" code="country.flag"/>
    <br/>
    <acme:textbox path="wikiLink" code="country.wikiLink"/>
    <br/>


    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save" class="btn btn-primary"
           value="<spring:message code="country.save"/>"
           onclick="return confirm('<spring:message code="country.confirm.save"/>')"/>&nbsp;

    <input type="button" name="cancel" class="btn btn-warning"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('country/list.do');"/>
    <br/>


</form:form>
</div>