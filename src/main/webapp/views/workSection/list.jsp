<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
<c:set var="localeCode" value="${pageContext.response.locale}" />
    <security:authorize access="hasRole('INMIGRANT')">
        <a class="btn btn-primary" href="workSection/create.do"> <spring:message code="application.create"/></a>
    </security:authorize>
    <!-- Listing grid -->
    <display:table pagesize="10" class="table table-condensed" keepStatus="true"
                   name="workSections" requestURI="${requestURI}" id="row">


        <security:authorize access="hasRole('INMIGRANT')">

            <display:column>
                <a href="workSection/edit.do?workSectionId=${row.id}"> <spring:message
                        code="application.workSection.edit"/>
                </a>
            </display:column>
        </security:authorize>

        <spring:message code="workSection.company" var="company1"/>
        <display:column property="company" title="${company1}" sortable="true"/>

        <spring:message code="workSection.position" var="position1"/>
        <display:column property="position" title="${position1}" sortable="true"/>

        <spring:message code="workSection.startDate" var="startDate1"/>
        <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="startDate" title="${startDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="startDate" title="${startDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

        <spring:message code="workSection.endDate" var="endDate1"/>
        <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="endDate" title="${endDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="endDate" title="${endDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

    </display:table>
</div>