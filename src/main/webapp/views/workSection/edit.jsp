<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
    <form:form action="workSection/edit.do" modelAttribute="workSection">

        <form:hidden path="id"/>
        <form:hidden path="version"/>
        <form:hidden path="application"/>


        <acme:textbox path="company" code="workSection.company"/>
        <br/>
        <acme:textbox path="position" code="workSection.position"/>
        <br/>
        <acme:textbox path="startDate" code="workSection.startDate"/>
        <br/>
        <acme:textbox path="endDate" code="workSection.endDate"/>
        <br/>


        <!---------------------------- BOTONES -------------------------->
        <input type="submit" class="btn btn-primary" name="save"
               value="<spring:message code="general.save"/>"
        />&nbsp;


        <input type="button" name="cancel" class="btn btn-warning"
               value="<spring:message code="general.cancel" />"
               onclick="relativeRedir('application/view.do?applicationId=${workSection.application.id}');"/>
    </form:form>

</div>