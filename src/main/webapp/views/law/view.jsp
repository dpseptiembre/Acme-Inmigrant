<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->



<div class="container">

		<h2><jstl:out value="${l.title}"/></h2>

		<p><jstl:out value="${l.text}"/></p>

	<h1><spring:message code="law.requirments"/></h1>
	<display:table pagesize="10" class="displaytag" keepStatus="true"
				   name="requirements" requestURI="${requestURI}" id="row">

		<%--</security:authorize>--%>

		<spring:message code="law.title" var="title"/>
		<display:column property="title" title="${title}" sortable="true"/>

		<spring:message code="requirement.description" var="description"/>
		<display:column property="description" title="${description}" sortable="true"/>

		<%--<spring:message code="requirement.law" var="law"/>--%>
		<%--<display:column property="law" title="${law}" sortable="true"/>--%>


	</display:table>

	<h1><spring:message code="law.laws"/></h1>

	<display:table pagesize="10" class="displaytag" keepStatus="true"
				   name="laws" requestURI="${requestURI}" id="row">


		<spring:message code="law.title" var="title"/>
		<display:column property="title" title="${title}" sortable="true"/>

		<spring:message code="law.text" var="text"/>
		<display:column property="text" title="${text}" sortable="true"/>

		<spring:message code="law.enatctment" var="enatctment"/>
		<display:column property="enatctment" title="${enatctment}" sortable="true"/>

		<spring:message code="law.country" var="country"/>
		<display:column property="country" title="${country}" sortable="true"/>


	</display:table>

</div>