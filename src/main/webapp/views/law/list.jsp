<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	<security:authorize access="hasRole('ADMINISTRATOR')">
		<a class="btn btn-primary" href="law/create.do"> <spring:message
				code="law.create" /></a>
	</security:authorize>
	<!-- Listing grid -->
	<display:table pagesize="10" class="displaytag" keepStatus="true"
		name="laws" requestURI="${requestURI}" id="row">


		<security:authorize access="hasRole('ADMINISTRATOR')">
			<display:column>
				<a class="button2" href="law/edit.do?lawId=${row.id}"> <spring:message
						code="visa.edit" />
				</a>
			</display:column>
			<display:column>
				<a class="button2" href="law/delete.do?lawId=${row.id}"
					onclick="return confirm('<spring:message code="general.confirm"/>')">
					<spring:message code="visa.delete" />
				</a>
			</display:column>
			<display:column>
				<a class="button2" href="law/view.do?lawId=${row.id}"> <spring:message
						code="general.view" /></a>
			</display:column>
		</security:authorize>


		<spring:message code="law.title" var="title" />
		<display:column property="title" title="${title}" sortable="true" />

		<spring:message code="law.text" var="text" />
		<display:column property="text" title="${text}" sortable="true" />

		<spring:message code="law.enatctment" var="enatctment" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="enatctment" title="${enatctment}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="enatctment" title="${enatctment}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>


		<spring:message code="law.country" var="country" />
		<display:column property="country" title="${country}" sortable="true" />


	</display:table>
	<a class="btn btn-primary" href="/visa/list.do"> <spring:message
			code="backtovisa.list" /></a> <a class="btn btn-primary"
		href="/requirement/list.do"> <spring:message
			code="requirement.list" /></a>
</div>