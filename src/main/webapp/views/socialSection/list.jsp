<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
    <security:authorize access="hasRole('INIGRANT')">
        <a class="btn btn-primary" href="socialSection/create.do"> <spring:message code="application.create"/></a>
    </security:authorize>
    <!-- Listing grid -->
    <display:table pagesize="10" class="table table-condensed" keepStatus="true"
                   name="socialSections" requestURI="${requestURI}" id="row">

        <spring:message code="socialSection.nickname" var="nickname1"/>
        <display:column property="nickname" title="${nickname1}" sortable="true"/>
        <spring:message code="socialSection.socialNetwork" var="socialNetwork1"/>
        <display:column property="socialNetwork" title="${socialNetwork1}" sortable="true"/>
        <spring:message code="socialSection.link" var="link1"/>
        <display:column property="link" title="${link1}" sortable="true"/>

        <security:authorize access="hasRole('INMIGRANT')">
            <display:column>
                <a href="socialSection/edit.do?socialSectionId=${row.id}"> <spring:message
                        code="application.socialSection.edit"/>
                </a>
            </display:column>
        </security:authorize>
    </display:table>
</div>