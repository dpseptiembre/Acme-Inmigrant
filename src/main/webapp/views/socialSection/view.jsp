<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>


<spring:message code="socialSection.nickname" var="nickname1"/>
<h3><jstl:out value="${nickname1}"/></h3>
<jstl:out value="${nickname}"/>

<spring:message code="socialSection.socialNetwork" var="socialNetwork1"/>
<h3><jstl:out value="${socialNetwork1}"/></h3>
<jstl:out value="${socialNetwork}"/>

<spring:message code="socialSection.link" var="link1"/>
<h3><jstl:out value="${link1}"/></h3>
<jstl:out value="${link}"/>


<security:authorize access="hasRole('INMIGRANT')">
    <a class="button2" href="socialSection/edit.do?socialSectionId=${id}"> <spring:message
            code="application.socialSection.edit"/>
    </a>
</security:authorize>

<a class="button" href="/socialSection/list.do"><spring:message code="general.cancel"/></a>
