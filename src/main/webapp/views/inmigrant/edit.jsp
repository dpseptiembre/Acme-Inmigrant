<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="inmigrant/edit.do" modelAttribute="inmigrant">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="userAccount"/>

    <acme:textbox path="name" code="inmigrant.name"/>
    <acme:textbox path="surname" code="inmigrant.surname"/>
    <acme:textbox path="email" code="inmigrant.email"/>
    <acme:textbox path="phone" code="inmigrant.phone"/>
    <acme:textbox path="address" code="inmigrant.address"/>

    <h1><spring:message code="credit.card"/>:</h1>
    <br/>
    <acme:textbox path="creditCard.holderName" code="credit-card.holderName"/>
    <acme:textbox path="creditCard.number" code="credit-card.number"/>
    <acme:textbox path="creditCard.year" code="credit-card.year"/>
    <acme:textbox path="creditCard.month" code="credit-card.month"/>
    <acme:textbox path="creditCard.CVV" code="credit-card.CVV"/>
    <form:label path="creditCard.type">
        <spring:message code="credit-card.type"/>:
    </form:label>
    <form:select path="creditCard.type" code="creditCard.credit-card.type">
        <form:options/>
    </form:select>

    <hr>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <input type="button" class="btn btn-danger" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>

</form:form>

</div>