<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="inmigrant/register.do" modelAttribute="inmigrant">

    <form:hidden path="id"/>
    <form:hidden path="version"/>


    <acme:textbox path="name" code="inmigrant.name"/>
    <acme:textbox path="surname" code="inmigrant.surname"/>
    <acme:textbox path="email" code="inmigrant.email"/>
    <acme:textbox path="phone" code="inmigrant.phone"/>
    <acme:textbox path="address" code="inmigrant.address"/>

    <br/>
    <h1>User Account</h1>
    <acme:textbox path="userAccount.username" code="inmigrant.username"/>
    <acme:textbox path="userAccount.password" code="inmigrant.password"/>




    <%----%>
    <%--<br>--%>
    <%--<form:label path="userAccount.username">--%>
        <%--<spring:message code="inmigrant.username"/>:--%>
    <%--</form:label>--%>
    <%--<form:input path="userAccount.username"/>--%>
    <%--<form:errors cssClass="error" path="UserAccount.username"/>--%>
    <%--<br/>--%>
    <%--<br>--%>
    <%--<form:label path="userAccount.password">--%>
        <%--<spring:message code="inmigrant.password"/>:--%>
    <%--</form:label>--%>
    <%--<form:password path="userAccount.password"/>--%>
    <%--<form:errors cssClass="error" path="UserAccount.password"/>--%>
    <%--<br/>--%>
    <%--<br/>--%>

    <!---------------------------- BOTONES -------------------------->
    <input class="btn btn-info" type="submit" name="save"
           value="<spring:message code="inmigrant.save" />" onclick="return confirm('<spring:message code="confirm.phone.number"/>')"/>
    <a class="btn btn-danger" href="/welcome/index.do"><spring:message code="general.cancel"/></a>

</form:form>
</div>