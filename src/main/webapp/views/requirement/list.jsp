<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
<security:authorize access="hasRole('ADMINISTRATOR')">
    <a class="btn btn-primary" href="requirement/create.do"> <spring:message code="requirement.create" /></a>
</security:authorize>
<!-- Listing grid -->
<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="requirements" requestURI="${requestURI}" id="row">


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a class="btn btn-primary" href="requirement/edit.do?requirementId=${row.id}"> <spring:message code="visa.edit" />
            </a>
        </display:column>
        <display:column>
            <a class="btn btn-primary" href="requirement/delete.do?requirementId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>')"> <spring:message code="visa.delete"/></a>
        </display:column>
    </security:authorize>




    <spring:message code="law.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="requirement.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="requirement.law" var="law"/>
    <display:column property="law" title="${law}" sortable="true"/>


</display:table>
    <a class="btn btn-primary" href="/visa/list.do"> <spring:message code="backtovisa.list"/></a>
</div>
