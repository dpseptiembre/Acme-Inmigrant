<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="container">
<form:form action="requirement/edit.do" modelAttribute="requirement">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="abrogated"/>
    <form:hidden path="hidden"/>


    <acme:textbox path="title" code="law.title"/>
    <br/>

    <acme:textbox path="description" code="requirement.description"/>
    <br/>

    <acme:select path="law" code="requirement.law" items="${laws}" itemLabel="title"/>
    <br/>


    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save" class="btn btn-primary"
           value="<spring:message code="visa.save"/>"
           onclick="return confirm('<spring:message code="visa.confirm.save"/>')"/>&nbsp;

    <input type="button" name="cancel" class="btn btn-warning"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('/');"/>
    <br/>


</form:form>
</div>