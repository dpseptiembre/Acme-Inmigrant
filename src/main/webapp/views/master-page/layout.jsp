<%--
 * layout.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<base
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="shortcut icon" href="favicon.ico"/> 


	<%--<script type="text/javascript" src="scripts/jquery.js"></script>--%>
	<%--<script type="text/javascript" src="scripts/jquery-ui.js"></script>--%>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="scripts/jmenu.js"></script>
    <script type="text/javascript" src="scripts/cookies.js"></script>

<link rel="stylesheet" href="styles/common.css" type="text/css">
<link rel="stylesheet" href="styles/jmenu.css" media="screen" type="text/css" />
<link rel="stylesheet" href="styles/displaytag.css" type="text/css">
	<link rel="stylesheet" href="styles/cookies.css" type="text/css">

	<div class="container">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
    </div>


<%--<script type="text/javascript">--%>
	<%--$(document).ready(function() {--%>
		<%--$("#jMenu").menu();--%>
	<%--});--%>

	<%--function askSubmission(msg, form) {--%>
		<%--if (confirm(msg))--%>
			<%--form.submit();--%>
	<%--}--%>
	<%----%>
	<%--function relativeRedir(loc) {	--%>
		<%--var b = document.getElementsByTagName('base');--%>
		<%--if (b && b[0] && b[0].href) {--%>
  			<%--if (b[0].href.substr(b[0].href.length - 1) == '/' && loc.charAt(0) == '/')--%>
    		<%--loc = loc.substr(1);--%>
  			<%--loc = b[0].href + loc;--%>
		<%--}--%>
		<%--window.location.replace(loc);--%>
	<%--}--%>
<%--</script>--%>
	<%--Loading bootsrap--%>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>

<body>
<div class="container">
	<div>
		<tiles:insertAttribute name="header" />
	</div>
	<div>
		<h1 class="text-center">
			<tiles:insertAttribute name="title" />

			<hr>
		</h1>
		<tiles:insertAttribute name="body" />	
		<jstl:if test="${message != null}">
			<br />
			<span class="message"><spring:message code="${message}" /></span>
		</jstl:if>	
	</div>
	<div>
		<tiles:insertAttribute name="footer" />
	</div>
</div>
	<%--Loading bootsrap--%>
	<%--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--%>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>