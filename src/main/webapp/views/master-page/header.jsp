<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="container">
    <a href="welcome/index.do"> <img class="img-responsive" src="images/logo.png" alt="Acme Inmigrant, Inc."/>
    </a>
    <br>
</div>

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<%--<div class="navbar-header">--%>

			<%--<a class="navbar-brand" href="#">Brand</a>--%>

		<%--</div>--%>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">

				<security:authorize access="isAnonymous()">
					<li><a href="inmigrant/create.do"><spring:message code="master.page.inmigrant.register"/></a></li>
					<li><a href="finder/create.do"><spring:message code="master.page.finder.create"/></a></li>
					<li><a href="category/list.do"><spring:message code="master.page.visa.list"/></a></li>
					<li><a href="country/list.do"><spring:message code="master.page.country.list"/></a></li>
					<li><a href="security/login.do"><spring:message code="master.page.login" /></a></li>
				</security:authorize>



				<security:authorize access="hasRole('ADMINISTRATOR')">


					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="master.page.folders"/><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="mezzage/listPublic.do"><spring:message code="master.page.administrator.mezzage"/></a></li>
							<li><a href="mezzage/listPublicWithSpamWord.do"><spring:message code="master.page.administrator.publicWithSpamWords"/></a></li>
						</ul>
					</li>


					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="master.page.actors"/><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="officer/create.do"><spring:message code="master.page.administrator.createOfficer"/></a></li>
							<li><a href="investigator/create.do"><spring:message code="master.page.administrator.createInvestigator"/></a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="master.page.administrator.visas"/><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="visa/list.do"><spring:message code="master.page.administrator.visas"/></a></li>
							<li><a href="country/list.do"><spring:message code="master.page.administrator.countries"/></a></li>
							<li><a href="law/list.do"><spring:message code="master.page.administrator.laws"/></a></li>
							<li><a href="requirement/list.do"><spring:message code="master.page.administrator.requirement"/></a></li>
							<li><a href="category/list.do"><spring:message code="master.page.administrator.categories"/></a></li>
						</ul>
					</li>

					<li><a href="administrator/dashboard.do"><spring:message code="master.page.administrator.dashboard"/></a></li>
					<li><a href="administrator/utilsView.do"><spring:message code="master.page.administrator.utils"/></a></li>



				</security:authorize>


				<security:authorize access="hasRole('INMIGRANT')">
					<%--<li><a><spring:message code="master.page.inmigrant"/></a></li>--%>
					<li><a href="inmigrant/edit.do"><spring:message code="master.page.inmigrant.editProfile"/></a></li>
					<li><a href="application/listMy.do"><spring:message code="master.page.inmigrant.myapplications"/></a></li>
					<li><a href="application/statusView.do"><spring:message code="master.page.inmigrant.statusView"/></a></li>
					<li><a href="visa/list.do"><spring:message code="master.page.visa.list"/></a></li>
					<li><a href="country/list.do"><spring:message code="master.page.country.list"/></a></li>
				</security:authorize>

				<security:authorize access="hasRole('SUPERVISOR')">
					<li><a href="supervisor/listOfficer.do"><spring:message code="master.page.supervisor.officer"/></a></li>
					<li><a href="supervisor/listMyOfficers.do"><spring:message code="master.page.supervisor.officer.my"/></a></li>
				</security:authorize>

				<security:authorize access="hasRole('OFFICER')">
					<%--<li><a class="fNiv" href="officer/edit.do"><spring:message code="master.page.inmigrant.editProfile"/></a></li>--%>

					<li><a href="application/listAll.do"><spring:message code="master.page.application.list"/></a></li>
					<li><a href="officer/viewEvaluations.do"><spring:message code="master.page.evaluations"/></a></li>
					<li><a href="application/statusViewOfficer.do"><spring:message code="master.page.application.orderedByStatus"/></a></li>
					<li><a href="report/doneReports.do"><spring:message code="master.page.reports.done"/></a></li>
				</security:authorize>


				<security:authorize access="hasRole('INVESTIGATOR')">
					<%--<li><a href="investigator/edit.do"><spring:message code="master.page.inmigrant.editProfile"/></a></li>--%>
					<li><a href="report/myReports.do"><spring:message code="master.page.investigator.reports"/></a></li>
					<li><a href="inmigrant/listUnderInvestigation.do"><spring:message code="master.page.investigator.inmigrantUnderInvest"/></a></li>
				</security:authorize>


				<%--<li class="dropdown">--%>
					<%--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>--%>
					<%--<ul class="dropdown-menu">--%>
						<%--<li><a href="#">Action</a></li>--%>
						<%--<li><a href="#">Another action</a></li>--%>
						<%--<li><a href="#">Something else here</a></li>--%>
						<%--<li role="separator" class="divider"></li>--%>
						<%--<li><a href="#">Separated link</a></li>--%>
						<%--<li role="separator" class="divider"></li>--%>
						<%--<li><a href="#">One more separated link</a></li>--%>
					<%--</ul>--%>
				<%--</li>--%>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<security:authorize access="isAuthenticated()">
					<li><a href="finder/create.do"><spring:message code="master.page.finder.create"/></a></li>


					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="master.page.folders"/><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="mezzage/find.do"><spring:message code="master.page.find.mezzages" /></a></li>
							<li><a href="folder/listMyFolders.do"><spring:message code="master.page.folders"/></a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="master.page.profile" />(<security:authentication property="principal.username" />) <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="j_spring_security_logout"><spring:message code="master.page.profile.logout"/></a></li>
						</ul>
					</li>

				</security:authorize>

			</ul>

			<%--<ul class="nav navbar-nav navbar-right">--%>
				<%--<li><a href="#">Link</a></li>--%>
				<%--<li class="dropdown">--%>
					<%--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>--%>
					<%--<ul class="dropdown-menu">--%>
						<%--<li><a href="#">Action</a></li>--%>
						<%--<li><a href="#">Another action</a></li>--%>
						<%--<li><a href="#">Something else here</a></li>--%>
						<%--<li role="separator" class="divider"></li>--%>
						<%--<li><a href="#">Separated link</a></li>--%>
					<%--</ul>--%>
				<%--</li>--%>
			<%--</ul>--%>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>

<div class="container">
    <a href="?language=en">EN</a> | <a href="?language=es">ES</a>
</div>

