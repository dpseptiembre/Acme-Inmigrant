<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
<form:form action="visa/edit.do" modelAttribute="visa">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="applications"/>
    <form:hidden path="requirements"/>
    <form:hidden path="abrogated"/>

    <acme:textbox path="clazz" code="visa.clazz"/>
    <br/>
    <acme:textbox path="description" code="visa.description"/>
    <br/>
    <acme:textbox path="price" code="visa.price"/>
    <br/>
    <acme:textbox path="currency" code="visa.currency"/>
    <br/>
    <acme:select path="country" code="visa.country" items="${countries}" itemLabel="name"/>
    <br/>
    <acme:select path="category" code="visa.category" items="${categories}" itemLabel="name"/>




    <%--<form:label path="category">--%>
        <%--<spring:message code="visa.category"/>:--%>
    <%--</form:label>--%>
    <%--<form:select id="category" path="category">--%>
        <%--<form:options items="${categories}" itemValue="id"--%>
                      <%--itemLabel="title"/>--%>
    <%--</form:select>--%>

    <hr>

    <!---------------------------- BOTONES -------------------------->
    <input type="submit"  class="btn btn-primary" name="save"
           value="<spring:message code="visa.save"/>"
           onclick="return confirm('<spring:message code="visa.confirm.save"/>')"/>&nbsp;




    <input type="button" name="cancel" class="btn btn-warning"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('visa/list.do');"/>
    <%--<jstl:if test="${visa.abrogated != true}">--%>
        <%--<input type="button" name="cancel" class="btn btn-danger"--%>
               <%--value="<spring:message code="visa.abrogated" />"--%>
               <%--onclick="relativeRedir('visa/abrogate.do?visaIs=${id}');"/>--%>
    <%--</jstl:if>--%>


</form:form>

</div>