<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<security:authorize access="hasRole('INMIGRANT')">
    <jstl:if test="${noYetAppliedFor == true}">
        <a class="button2" href="application/create.do?visaId=${id}"> <spring:message code="visa.apply" />
        </a>
    </jstl:if>
</security:authorize>


<spring:message code="visa.clazz" var="clazz1"/>
<h3><jstl:out value="${clazz1}"/></h3>
<jstl:out value="${clazz}"/>

<spring:message code="visa.description" var="description1"/>
<h3><jstl:out value="${description1}"/></h3>
<jstl:out value="${description}"/>

<spring:message code="visa.price" var="price1"/>
<h3><jstl:out value="${price1}"/></h3>
<jstl:out value="${price}"/>
<jstl:out value="${currency}"/>
<spring:message code="visa.category" var="category1"/>
<h3><jstl:out value="${category1}"/></h3>
<jstl:out value="${category}"/>

<spring:message code="visa.country" var="country1"/>
<h3><jstl:out value="${country1}"/></h3>
<jstl:out value="${country}"/>

<%--<spring:message code="visa.application" var="application1"/>--%>
<%--<h3><jstl:out value="${application1}"/></h3>--%>
<%--<jstl:out value="${application}"/>--%>


<%--<display:table pagesize="10" class="table table-condensed" keepStatus="true"--%>
               <%--name="applications" requestURI="${requestURI}" id="row">--%>


    <%--<security:authorize access="hasRole('INMIGRANT')">--%>
        <%--<display:column>--%>
            <%--<jstl:if test="${row.closed == true}">--%>
                <%--<a class="btn btn-primary" href="application/open.do?applicationId=${row.id}"> <spring:message--%>
                        <%--code="application.open"/>--%>
                <%--</a>--%>
            <%--</jstl:if>--%>
        <%--</display:column>--%>

        <%--<display:column>--%>
            <%--<a class="btn btn-primary" href="application/link.do?applicationId=${row.id}"> <spring:message--%>
                    <%--code="application.link"/>--%>
            <%--</a>--%>
        <%--</display:column>--%>
        <%--<display:column>--%>
            <%--<jstl:if test="${row.closed != true}">--%>
                <%--<a class="btn btn-primary" href="application/close.do?applicationId=${row.id}"--%>
                   <%--onclick="return confirm('<spring:message code="close.confirm"/>')"> <spring:message--%>
                        <%--code="application.close"/>--%>
                <%--</a>--%>
            <%--</jstl:if>--%>
        <%--</display:column>--%>
        <%--<display:column>--%>
            <%--<a class="btn btn-primary" href="application/view.do?applicationId=${row.id}"> <spring:message--%>
                    <%--code="application.view"/>--%>
            <%--</a>--%>
        <%--</display:column>--%>
    <%--</security:authorize>--%>
    <%--<security:authorize access="hasRole('OFFICER')">--%>
        <%--<display:column>--%>
            <%--<a class="btn btn-primary" href="application/view.do?applicationId=${row.id}"> <spring:message--%>
                    <%--code="application.view"/>--%>
            <%--</a>--%>
        <%--</display:column>--%>
    <%--</security:authorize>--%>


    <%--<spring:message code="application.ticker" var="ticker1"/>--%>
    <%--<display:column property="ticker" title="${ticker1}" sortable="true"/>--%>

    <%--<spring:message code="application.officer" var="officer1"/>--%>
    <%--<display:column property="officer" title="${officer1}" sortable="true"/>--%>

    <%--<spring:message code="application.status" var="status1"/>--%>
    <%--<display:column property="status" title="${status1}" sortable="true"/>--%>

    <%--<spring:message code="application.applicant" var="applicant1"/>--%>
    <%--<display:column property="applicant" title="${applicant1}" sortable="true"/>--%>

    <%--<spring:message code="application.openDate" var="openDate1"/>--%>
    <%--<display:column property="openDate" title="${openDate1}" sortable="true"/>--%>

    <%--<spring:message code="application.closeDate" var="closeDate1"/>--%>
    <%--<display:column property="closeDate" title="${closeDate1}" sortable="true"/>--%>

    <%--<spring:message code="application.visa" var="visa1"/>--%>
    <%--<display:column property="visa" title="${visa1}" sortable="true"/>--%>

    <%--<spring:message code="application.closed" var="closed1"/>--%>
    <%--<display:column property="closed" title="${closed1}" sortable="true"/>--%>


    <%--&lt;%&ndash;<security:authorize access="hasRole('OFFICER')">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<jstl:if test="${row.hasntOfficer == false}">&ndash;%&gt;--%>
            <%--&lt;%&ndash;<display:column>&ndash;%&gt;--%>
                <%--&lt;%&ndash;<a href="application/selfassign.do?applicationId=${row.id}"> <spring:message&ndash;%&gt;--%>
                        <%--&lt;%&ndash;code="application.selfassign"/>&ndash;%&gt;--%>
                <%--&lt;%&ndash;</a>&ndash;%&gt;--%>
            <%--&lt;%&ndash;</display:column>&ndash;%&gt;--%>

        <%--&lt;%&ndash;</jstl:if>&ndash;%&gt;--%>

    <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>




<%--</display:table>--%>




<spring:message code="general.requirements" var="register11"/>
<h3><jstl:out value="${register11}"/></h3>
<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="requirements" requestURI="${requestURI}" id="row">


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a class="btn btn-primary" href="requirement/edit.do?requirementId=${row.id}"> <spring:message code="visa.edit"/>
            </a>
        </display:column>
        <display:column>
            <a class="btn btn-primary" href="requirement/delete.do?requirementId=${row.id}"
               onclick="return confirm('<spring:message code="general.confirm"/>')"> <spring:message
                    code="visa.delete"/></a>
        </display:column>
    </security:authorize>

    <security:authorize access="permitAll()">
        <display:column>
            <a class="btn btn-primary" href="law/list.do?requirementId=${row.id}"> <spring:message
                    code="law.relatedLaws"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="law.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="requirement.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <%--<spring:message code="requirement.law" var="law"/>--%>
    <%--<display:column property="law" title="${law}" sortable="true"/>--%>


</display:table>



<a class="button" href="/visa/list.do"><spring:message code="general.cancel"/></a>

