<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
<security:authorize access="hasRole('ADMINISTRATOR')">
        <a class="btn btn-primary" href="visa/create.do"> <spring:message code="visa.create" /></a>
</security:authorize>


    <security:authorize access="permitAll()">
            <a class="btn btn-primary" href="requirement/list.do?visaId=${row.id}"> <spring:message
                    code="visa.viewRequirements"/>
            </a>
    </security:authorize>
<!-- Listing grid -->
<display:table pagesize="10" class="table table-condensed" keepStatus="true"
               name="visas" requestURI="${requestURI}" id="row">


    <!-- Attributes -->

    <security:authorize access="hasRole('ADMINISTRATOR')">
    <display:column>
    <a class="button2" href="visa/edit.do?visaId=${row.id}"> <spring:message code="visa.edit" />
    </a>
    </display:column>
        <display:column>
            <jstl:if test="${row.abrogated == false}">
                <a  class="btn btn-danger" href="visa/abrogate.do?visaId=${row.id}" onclick="return confirm('<spring:message code="general.confirm" />')"> <spring:message code="visa.abrogated" />
                </a>
            </jstl:if>

        </display:column>
        <display:column>
            <a class="btn btn-primary" href="visa/delete.do?visaId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>

    <security:authorize access="isAnonymous()">
        <display:column>
            <a class="btn btn-primary" href="visa/viewAnon.do?visaId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>



    <security:authorize access="isAuthenticated()">
        <display:column>
            <a class="btn btn-primary" href="visa/viewAuthenticated.do?visaId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>



    <spring:message code="visa.clazz" var="clazz"/>
    <display:column property="clazz" title="${clazz}" sortable="true"/>

    <spring:message code="visa.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="visa.price" var="price"/>
    <display:column property="price" title="${price}" sortable="true"/>

    <spring:message code="visa.category" var="category"/>
    <display:column property="category" title="${category}" sortable="true"/>

    <spring:message code="visa.application" var="application"/>
    <display:column property="applications" title="${application}" sortable="true"/>

    <%--<spring:message code="visa.requirements" var="requirements"/>--%>
    <%--<display:column property="requirements" title="${requirements}" sortable="true"/>--%>

    <spring:message code="visa.country" var="country"/>
    <display:column property="country" title="${country}" sortable="true"/>
    <%-- TODO: el boolean abrogated peta--%>
    <%--<spring:message code="visa.abrogated" var="abrogated"/>--%>
    <%--<display:column property="abrogated" title="${abrogated}" sortable="true"/>--%>


</display:table>
</div>