<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>


<spring:message code="contactSection.email" var="email1"/>
<h3><jstl:out value="${email1}"/></h3>
<jstl:out value="${email}"/>

<spring:message code="contactSection.phoneNumber" var="phoneNumber1"/>
<h3><jstl:out value="${phoneNumber1}"/></h3>
<jstl:out value="${phoneNumber}"/>

<spring:message code="contactSection.pageNumber" var="pageNumber1"/>
<h3><jstl:out value="${pageNumber1}"/></h3>
<jstl:out value="${pageNumber}"/>


<security:authorize access="hasRole('INMIGRANT')">
    <a class="button2" href="contactSection/edit.do?contactSectionId=${id}"> <spring:message
            code="application.contactSection.edit"/>
    </a>
</security:authorize>


<a class="button" href="/application/list.do"><spring:message code="general.cancel"/></a>
