<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">

    <display:table pagesize="5" class="displaytag" keepStatus="true"
                   name="contactSection" requestURI="${requestURI}" id="row">


        <!-- Attributes -->

        <security:authorize access="hasRole('INMIGRANT')">
            <display:column>
                <a href="contactSection/edit.do?contactSectionId=${row.id}"> <spring:message
                        code="application.contactSection.edit"/>
                </a>
            </display:column>
        </security:authorize>

        <spring:message code="contactSection.email" var="email1"/>
        <display:column property="email" title="${email1}" sortable="true"/>
        <spring:message code="contactSection.phoneNumber" var="phoneNumber1"/>
        <display:column property="phoneNumber" title="${phoneNumber1}" sortable="true"/>
        <spring:message code="contactSection.pageNumber" var="pageNumber1"/>
        <display:column property="pageNumber" title="${pageNumber1}" sortable="true"/>


    </display:table>

</div>