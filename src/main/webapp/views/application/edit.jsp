<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
    <form:form action="application/edit.do" modelAttribute="application">

        <form:hidden path="id"/>
        <form:hidden path="version"/>
        <form:hidden path="officer"/>
        <form:hidden path="applicant"/>
        <form:hidden path="openDate"/>
        <form:hidden path="closeDate"/>
        <form:hidden path="statusChange"/>
        <form:hidden path="statusComment"/>
        <form:hidden path="visa"/>
        <form:hidden path="ticker"/>
        <form:hidden path="closed"/>
        <security:authorize access="hasRole('INMIGRANT')">

            <a href="personalSection/edit.do?applicationId=${id}"> <spring:message
                    code="application.personalSection"/></a>
            <br/>
            <a href="socialSection/edit.do?applicationId=${id}"><spring:message code="application.socialSection"/></a>
            <br/>
            <a href="contactSection/edit.do?applicationId=${id}"><spring:message code="application.contactSection"/></a>
            <br/>
            <a href="educationSection/edit.do?applicationId=${id}"> <spring:message
                    code="application.educationSection"/></a>
            <br/>
            <a href="workSection/edit.do?applicationId=${id}"><spring:message code="application.workSection"/></a>

        </security:authorize>


        <!---------------------------- BOTONES -------------------------->
        <input type="submit" class="btn btn-primary" name="save"
               value="<spring:message code="application.save"/>"
        />&nbsp;


        <input type="button" name="cancel" class="btn btn-warning"
               value="<spring:message code="general.cancel" />"
               onclick="relativeRedir('welcome/index.do');"/>
        <%--<jstl:if test="${application.abrogated != true}">--%>
        <%--<input type="button" name="cancel" class="btn btn-danger"--%>
        <%--value="<spring:message code="application.abrogated" />"--%>
        <%--onclick="relativeRedir('application/abrogate.do?applicationIs=${id}');"/>--%>
        <%--</jstl:if>--%>


    </form:form>

</div>