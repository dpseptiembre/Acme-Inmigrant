<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	<!-- Listing grid -->
	<spring:message code="application.oppended" var="oppended1" />
	<h3>
		<jstl:out value="${oppended1}" />
	</h3>
	<display:table pagesize="10" class="table table-condensed"
		keepStatus="true" name="oppended" requestURI="${requestURIoppended}"
		id="row">
		<security:authorize access="hasRole('INMIGRANT')">
			<display:column>
				<jstl:if test="${row.closed == true}">
					<a class="button2"
						href="application/open.do?applicationId=${row.id}"> <spring:message
							code="application.open" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/link.do?applicationId=${row.id}"> <spring:message
						code="application.link" />
				</a>
			</display:column>
			<display:column>
				<jstl:if test="${row.closed != true}">
					<a class="button2"
						href="application/close.do?applicationId=${row.id}"
						onclick="return confirm('<spring:message code="close.confirm"/>')">
						<spring:message code="application.close" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<security:authorize access="hasRole('OFFICER')">
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<spring:message code="application.ticker" var="ticker1" />
		<display:column property="ticker" title="${ticker1}" sortable="true" />

		<spring:message code="application.officer" var="officer1" />
		<display:column property="officer" title="${officer1}" sortable="true" />

		<spring:message code="application.status" var="status1" />
		<display:column property="status" title="${status1}" sortable="true" />

		<spring:message code="application.applicant" var="applicant1" />
		<display:column property="applicant" title="${applicant1}"
			sortable="true" />

		<spring:message code="application.openDate" var="openDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.closeDate" var="closeDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.visa" var="visa1" />
		<display:column property="visa" title="${visa1}" sortable="true" />
		<spring:message code="application.closed" var="closed1" />
		<display:column property="closed" title="${closed1}" sortable="true" />

		<security:authorize access="hasRole('INMIGRANT')">
			<a href="personalSection/view.do?applicationId=${id}"> <spring:message
					code="application.personalSection" /></a>
			<br />
			<a href="socialSection/view.do?applicationId=${id}"><spring:message
					code="application.socialSection" /></a>
			<br />
			<a href="contactSection/view.do?applicationId=${id}"><spring:message
					code="application.contactSection" /></a>
			<br />
			<a href="educationSection/view.do?applicationId=${id}"> <spring:message
					code="application.educationSection" /></a>
			<br />
			<a href="workSection/view.do?applicationId=${id}"><spring:message
					code="application.workSection" /></a>
		</security:authorize>
	</display:table>


	<spring:message code="application.closedaccepted" var="closedaccepted1" />
	<h3>
		<jstl:out value="${closedaccepted1}" />
	</h3>


	<display:table pagesize="10" class="table table-condensed"
		keepStatus="true" name="closedaccepted"
		requestURI="${requestURIclosedaccepted}" id="row">
		<security:authorize access="hasRole('INMIGRANT')">
			<display:column>
				<jstl:if test="${row.closed == true}">
					<a class="button2"
						href="application/open.do?applicationId=${row.id}"> <spring:message
							code="application.open" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/link.do?applicationId=${row.id}"> <spring:message
						code="application.link" />
				</a>
			</display:column>
			<display:column>
				<jstl:if test="${row.closed != true}">
					<a class="button2"
						href="application/close.do?applicationId=${row.id}"
						onclick="return confirm('<spring:message code="close.confirm"/>')">
						<spring:message code="application.close" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<security:authorize access="hasRole('OFFICER')">
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<spring:message code="application.ticker" var="ticker1" />
		<display:column property="ticker" title="${ticker1}" sortable="true" />

		<spring:message code="application.officer" var="officer1" />
		<display:column property="officer" title="${officer1}" sortable="true" />

		<spring:message code="application.status" var="status1" />
		<display:column property="status" title="${status1}" sortable="true" />

		<spring:message code="application.applicant" var="applicant1" />
		<display:column property="applicant" title="${applicant1}"
			sortable="true" />

		<spring:message code="application.openDate" var="openDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.closeDate" var="closeDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.visa" var="visa1" />
		<display:column property="visa" title="${visa1}" sortable="true" />
		<spring:message code="application.closed" var="closed1" />
		<display:column property="closed" title="${closed1}" sortable="true" />
		<security:authorize access="hasRole('INMIGRANT')">
			<a href="personalSection/view.do?applicationId=${id}"> <spring:message
					code="application.personalSection" /></a>
			<br />
			<a href="socialSection/view.do?applicationId=${id}"><spring:message
					code="application.socialSection" /></a>
			<br />
			<a href="contactSection/view.do?applicationId=${id}"><spring:message
					code="application.contactSection" /></a>
			<br />
			<a href="educationSection/view.do?applicationId=${id}"> <spring:message
					code="application.educationSection" /></a>
			<br />
			<a href="workSection/view.do?applicationId=${id}"><spring:message
					code="application.workSection" /></a>
		</security:authorize>
	</display:table>


	<spring:message code="application.closeddenied" var="closeddenied1" />
	<h3>
		<jstl:out value="${closeddenied1}" />
	</h3>
	<display:table pagesize="10" class="table table-condensed"
		keepStatus="true" name="closeddenied"
		requestURI="${requestURIcloseddenied}" id="row">
		<security:authorize access="hasRole('INMIGRANT')">
			<display:column>
				<jstl:if test="${row.closed == true}">
					<a class="button2"
						href="application/open.do?applicationId=${row.id}"> <spring:message
							code="application.open" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/link.do?applicationId=${row.id}"> <spring:message
						code="application.link" />
				</a>
			</display:column>
			<display:column>
				<jstl:if test="${row.closed != true}">
					<a class="button2"
						href="application/close.do?applicationId=${row.id}"
						onclick="return confirm('<spring:message code="close.confirm"/>')">
						<spring:message code="application.close" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<security:authorize access="hasRole('OFFICER')">
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<spring:message code="application.ticker" var="ticker1" />
		<display:column property="ticker" title="${ticker1}" sortable="true" />

		<spring:message code="application.officer" var="officer1" />
		<display:column property="officer" title="${officer1}" sortable="true" />

		<spring:message code="application.status" var="status1" />
		<display:column property="status" title="${status1}" sortable="true" />

		<spring:message code="application.applicant" var="applicant1" />
		<display:column property="applicant" title="${applicant1}"
			sortable="true" />

		<spring:message code="application.openDate" var="openDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.closeDate" var="closeDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.visa" var="visa1" />
		<display:column property="visa" title="${visa1}" sortable="true" />
		<spring:message code="application.closed" var="closed1" />
		<display:column property="closed" title="${closed1}" sortable="true" />
		<security:authorize access="hasRole('INMIGRANT')">
			<a href="personalSection/view.do?applicationId=${id}"> <spring:message
					code="application.personalSection" /></a>
			<br />
			<a href="socialSection/view.do?applicationId=${id}"><spring:message
					code="application.socialSection" /></a>
			<br />
			<a href="contactSection/view.do?applicationId=${id}"><spring:message
					code="application.contactSection" /></a>
			<br />
			<a href="educationSection/view.do?applicationId=${id}"> <spring:message
					code="application.educationSection" /></a>
			<br />
			<a href="workSection/view.do?applicationId=${id}"><spring:message
					code="application.workSection" /></a>
		</security:authorize>
	</display:table>


	<spring:message code="application.closedawaiting" var="closedawaiting1" />
	<h3>
		<jstl:out value="${closedawaiting1}" />
	</h3>
	<display:table pagesize="10" class="table table-condensed"
		keepStatus="true" name="closedawaiting"
		requestURI="${requestURIclosedawaiting}" id="row">
		<security:authorize access="hasRole('INMIGRANT')">
			<display:column>
				<jstl:if test="${row.closed == true}">
					<a class="button2" class="btn btn-primary"
						href="application/open.do?applicationId=${row.id}"> <spring:message
							code="application.open" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/link.do?applicationId=${row.id}"> <spring:message
						code="application.link" />
				</a>
			</display:column>
			<display:column>
				<jstl:if test="${row.closed != true}">
					<a class="button2"
						href="application/close.do?applicationId=${row.id}"
						onclick="return confirm('<spring:message code="close.confirm"/>')">
						<spring:message code="application.close" />
					</a>
				</jstl:if>
			</display:column>
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<security:authorize access="hasRole('OFFICER')">
			<display:column>
				<a class="btn btn-primary"
					href="application/view.do?applicationId=${row.id}"> <spring:message
						code="application.view" />
				</a>
			</display:column>
		</security:authorize>
		<spring:message code="application.ticker" var="ticker1" />
		<display:column property="ticker" title="${ticker1}" sortable="true" />

		<spring:message code="application.officer" var="officer1" />
		<display:column property="officer" title="${officer1}" sortable="true" />

		<spring:message code="application.status" var="status1" />
		<display:column property="status" title="${status1}" sortable="true" />

		<spring:message code="application.applicant" var="applicant1" />
		<display:column property="applicant" title="${applicant1}"
			sortable="true" />

		<spring:message code="application.openDate" var="openDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.closeDate" var="closeDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.visa" var="visa1" />
		<display:column property="visa" title="${visa1}" sortable="true" />
		<spring:message code="application.closed" var="closed1" />
		<display:column property="closed" title="${closed1}" sortable="true" />
		<br>

		<security:authorize access="hasRole('INMIGRANT')">
			<a href="personalSection/view.do?applicationId=${id}"> <spring:message
					code="application.personalSection" /></a>
			<br />
			<a href="socialSection/view.do?applicationId=${id}"><spring:message
					code="application.socialSection" /></a>
			<br />
			<a href="contactSection/view.do?applicationId=${id}"><spring:message
					code="application.contactSection" /></a>
			<br />
			<a href="educationSection/view.do?applicationId=${id}"> <spring:message
					code="application.educationSection" /></a>
			<br />
			<a href="workSection/view.do?applicationId=${id}"><spring:message
					code="application.workSection" /></a>
		</security:authorize>
	</display:table>
	<br> <br> <br>
</div>