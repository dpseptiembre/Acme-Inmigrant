<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container">


    <form:form action="application/investigate.do" method="get">


        <select name="investigatorId">
            <c:forEach var = "listValue" items = "${investigators}">
                <option value="${listValue.id}">${listValue.name}</option>
            </c:forEach>
        </select>


        <%--<input type="checkbox" name="investigator" value="${listValue.id}">${listValue.name}<br>--%>
        <%----%>

        <input type="hidden" name="appId" value="${ap.id}">


        <input type="submit" value="<spring:message code="application.save"/>" class="btn btn-primary"/>
    </form:form>


    <br>
    <br>
    <br>
</div>

