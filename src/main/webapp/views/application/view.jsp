<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="container">
<c:set var="localeCode" value="${pageContext.response.locale}" />

    <security:authorize access="hasRole('OFFICER')">

        <jstl:if test="${assigneddd==false && loggedOfficer==officer}">

            <a class="button2" href="application/toInvestigate.do?applicationId=${appID}"> <spring:message
                    code="application.investigate"/>
            </a>
        </jstl:if>
    </security:authorize>



    <spring:message code="application.ticker" var="ticker1"/>
<h3><jstl:out value="${ticker1}"/></h3>
<jstl:out value="${ticker}"/>

<spring:message code="application.officer" var="officer1"/>
<h3><jstl:out value="${officer1}"/></h3>
<jstl:out value="${officer}"/>

<spring:message code="application.applicant" var="applicant1"/>
<h3><jstl:out value="${applicant1}"/></h3>
<jstl:out value="${applicant}"/>

<spring:message code="application.openDate" var="openDate1"/>
<h3><jstl:out value="${openDate1}"/></h3>
<jstl:out value="${openDate}"/>

<spring:message code="application.closed" var="isClosed1"/>
<h3><jstl:out value="${isClosed1}"/></h3>
<jstl:out value="${isClosed}"/>

<spring:message code="application.closeDate" var="closeDate1"/>
<h3><jstl:out value="${closeDate1}"/></h3>
<jstl:out value="${closeDate}"/>

<spring:message code="application.statusChange" var="statusChange1"/>
<h3><jstl:out value="${statusChange1}"/></h3>
<jstl:out value="${statusChange}"/>

<spring:message code="application.statusComment" var="statusComment1"/>
<h3><jstl:out value="${statusComment1}"/></h3>
<jstl:out value="${statusComment}"/>

<spring:message code="application.visa" var="visa1"/>
<h3><jstl:out value="${visa1}"/></h3>
<jstl:out value="${visa}"/>
    <security:authorize access="isAnonymous()">
        <a class="btn btn-primary" href="visa/viewAnon.do?visaId=${visa.id}"> <spring:message
                code="application.visa.view"/>
        </a>
    </security:authorize>
    <security:authorize access="isAuthenticated()">
        <a class="btn btn-primary" href="visa/viewAuthenticated.do?visaId=${visa.id}"> <spring:message
                code="application.visa.view"/>
        </a>
    </security:authorize>
    <br>
<spring:message code="application.personalSection" var="personalSection1"/>
<h3><jstl:out value="${personalSection1}"/></h3>
<display:table pagesize="100" class="displaytag" keepStatus="true"
               name="personalSection" requestURI="${requestURLPersonalSection}" id="row">
    <security:authorize access="hasRole('INMIGRANT')">
        <jstl:if test="${isClosed != true}">
            <display:column>
                <a class="button" href="personalSection/addName.do?personalSectionId=${row.id}"> <spring:message
                        code="application.personalSection.addname"/>
                </a>
            </display:column>
        </jstl:if>
    </security:authorize>

    <spring:message code="personalSection.names" var="names1"/>
    <display:column property="names" title="${names1}" sortable="true"/>
    <spring:message code="personalSection.birthPlace" var="birthPlace1"/>
    <display:column property="birthPlace" title="${birthPlace1}" sortable="true"/>

    <spring:message code="personalSection.picture" var="picture"/>
    <display:column title="${picture}">
        <img src="${row.picture}" width="130" height="100">
    </display:column>


    <spring:message code="personalSection.birthDate" var="birthDate1"/>
    <display:column property="birthDate" title="${birthDate1}" sortable="true"/>


    <security:authorize access="hasRole('INMIGRANT')">
        <jstl:if test="${isClosed != true}">
            <display:column>
                <a class="button" href="personalSection/edit.do?personalSectionId=${row.id}"> <spring:message
                        code="application.personalSection.edit"/>
                </a>
            </display:column>

        </jstl:if>
    </security:authorize>

</display:table>
    <br>

    <spring:message code="application.contactSection" var="contactSection1"/>
<h3><jstl:out value="${contactSection1}"/></h3>


<jstl:if test="${isClosed != true}">
<security:authorize access="hasRole('INMIGRANT')">
    <a class="button2" href="contactSection/create.do?applicationId=${appID}"> <spring:message
            code="application.contactSection.create"/>
    </a>
</security:authorize>
</jstl:if>
<display:table pagesize="100" class="displaytag" keepStatus="true"
               name="contactSection" requestURI="${requestURLContactSection}" id="row">
    <jstl:if test="${isClosed != true}">
    <security:authorize access="hasRole('INMIGRANT')">
        <display:column>
            <a class="btn btn-danger"
               href="contactSection/delete.do?contactSectionId=${row.id}&appId=${row.application.id}"
               onclick="return confirm('<spring:message code="delete.confirm"/>')"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>
    </jstl:if>

    <spring:message code="contactSection.email" var="email1"/>
    <display:column property="email" title="${email1}" sortable="true"/>
    <spring:message code="contactSection.phoneNumber" var="phoneNumber1"/>
    <display:column property="phoneNumber" title="${phoneNumber1}" sortable="true"/>
    <spring:message code="contactSection.pageNumber" var="pageNumber1"/>
    <display:column property="pageNumber" title="${pageNumber1}" sortable="true"/>

    <jstl:if test="${isClosed != true}">
    <security:authorize access="hasRole('INMIGRANT')">
        <display:column>
            <a class="button" href="contactSection/edit.do?contactSectionId=${row.id}"> <spring:message
                    code="application.contactSection.edit"/>
            </a>
        </display:column>
    </security:authorize>
    </jstl:if>


</display:table>
    <br>

<spring:message code="application.socialSection" var="socialSection1"/>
<h3><jstl:out value="${socialSection1}"/></h3>
<jstl:if test="${isClosed != true}">
<security:authorize access="hasRole('INMIGRANT')">
    <a class="button2" href="socialSection/create.do?applicationId=${appID}"> <spring:message
            code="application.socialSection.create"/>
    </a>
</security:authorize>
</jstl:if>
<display:table pagesize="100" class="displaytag" keepStatus="true"
               name="socialSection" requestURI="${requestURLSocialSection}" id="row">
    <jstl:if test="${isClosed != true}">
    <security:authorize access="hasRole('INMIGRANT')">
        <display:column>
            <a class="btn btn-danger"
               href="socialSection/delete.do?socialSectionId=${row.id}&appId=${row.application.id}"
               onclick="return confirm('<spring:message code="delete.confirm"/>')"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>
    </jstl:if>


    <spring:message code="socialSection.nickname" var="nickname1"/>
    <display:column property="nickname" title="${nickname1}" sortable="true"/>
    <spring:message code="socialSection.socialNetwork" var="socialNetwork1"/>
    <display:column property="socialNetwork" title="${socialNetwork1}" sortable="true"/>
    <spring:message code="socialSection.link" var="link1"/>
    <display:column property="link" title="${link1}" sortable="true"/>
    <jstl:if test="${isClosed != true}">
    <security:authorize access="hasRole('INMIGRANT')">
        <display:column>
            <a class="button" href="socialSection/edit.do?socialSectionId=${row.id}"> <spring:message
                    code="application.socialSection.edit"/>
            </a>
        </display:column>

    </security:authorize>
    </jstl:if>

</display:table>
    <br>

<spring:message code="application.educationSection" var="educationSection1"/>
<h3><jstl:out value="${educationSection1}"/></h3>
<jstl:if test="${isClosed != true}">
<security:authorize access="hasRole('INMIGRANT')">
    <a class="button2" href="educationSection/create.do?applicationId=${appID}"> <spring:message
            code="application.educationSection.create"/>
    </a>
</security:authorize>
</jstl:if>
<display:table pagesize="100" class="displaytag" keepStatus="true"
               name="educationSection" requestURI="${requestURLEducationSection}" id="row">
    <jstl:if test="${isClosed != true}">
    <security:authorize access="hasRole('INMIGRANT')">
        <display:column>
            <a class="btn btn-danger"
               href="educationSection/delete.do?educationSectionId=${row.id}&appId=${row.application.id}"
               onclick="return confirm('<spring:message code="delete.confirm"/>')"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>
    </jstl:if>
    <spring:message code="educationSection.degree" var="degree1"/>
    <display:column property="degree" title="${degree1}" sortable="true"/>
    <spring:message code="educationSection.institution" var="institution1"/>
    <display:column property="institution" title="${institution1}" sortable="true"/>
    <c:choose>
			<c:when test="${localeCode == 'en'}">
				<spring:message code="educationSection.awardedDate" var="awardedDate1" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<spring:message code="educationSection.awardedDate" var="awardedDate1" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>
    <display:column property="awardedDate" title="${awardedDate1}" sortable="true"/>
    <spring:message code="educationSection.level" var="level1"/>
    <display:column property="level" title="${level1}" sortable="true"/>
    <jstl:if test="${isClosed != true}">
        <security:authorize access="hasRole('INMIGRANT')">
            <display:column>
                <a class="button" href="educationSection/edit.do?educationSectionId=${row.id}"> <spring:message
                        code="application.educationSection.edit"/>
                </a>
            </display:column>

        </security:authorize>
    </jstl:if>
</display:table>
    <br>

<spring:message code="application.workSection" var="workSection1"/>
<h3><jstl:out value="${workSection1}"/></h3>
<jstl:if test="${isClosed != true}">
<security:authorize access="hasRole('INMIGRANT')">
    <a class="button2" href="workSection/create.do?applicationId=${appID}"> <spring:message
            code="application.workSection.create"/>
    </a>
</security:authorize>
</jstl:if>
<display:table pagesize="100" class="displaytag" keepStatus="true"
               name="workSection" requestURI="${requestURLWorkSection}" id="row">
    <jstl:if test="${isClosed != true}">
    <security:authorize access="hasRole('INMIGRANT')">
        <display:column>
            <a class="btn btn-danger" href="workSection/delete.do?workSectionId=${row.id}&appId=${row.application.id}"
               onclick="return confirm('<spring:message code="delete.confirm"/>')"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>
    </jstl:if>

    <spring:message code="workSection.company" var="company1"/>
    <display:column property="company" title="${company1}" sortable="true"/>
    <spring:message code="workSection.position" var="position1"/>
    <display:column property="position" title="${position1}" sortable="true"/>
    <spring:message code="workSection.startDate" var="startDate1"/>
    <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="startDate" title="${startDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="startDate" title="${startDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>
    <spring:message code="workSection.endDate" var="endDate1"/>
    <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="endDate" title="${endDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="endDate" title="${endDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>
    <jstl:if test="${isClosed != true}">
        <security:authorize access="hasRole('INMIGRANT')">
            <display:column>
                <a class="button" href="workSection/edit.do?workSectionId=${row.id}"> <spring:message
                        code="application.workSection.edit"/>
                </a>
            </display:column>
        </security:authorize>
    </jstl:if>
</display:table>

<br>

    <spring:message code="application.questions" var="questions1"/>
    <h3><jstl:out value="${questions1}"/></h3>
    <security:authorize access="hasRole('OFFICER')">

        <jstl:if test="${officer ==loggedOfficer}">
        <a class="button2" href="question/create.do?applicationId=${appID}"> <spring:message
                code="application.question.create"/>
        </a>
        </jstl:if>
    </security:authorize>

    <display:table pagesize="10" class="displaytag" keepStatus="true"
                   name="relatedQuestions" requestURI="${requestURIQuestions}" id="row">

        <jstl:if test="${isClosed != true}">
            <security:authorize access="hasRole('INMIGRANT')">
                <display:column>
                    <a class="btn btn-danger" href="question/reply.do?questionId=${row.id}"> <spring:message
                            code="application.question.reply"/>
                    </a>
                </display:column>
            </security:authorize>
        </jstl:if>

        <security:authorize access="hasRole('OFFICER')">

            <jstl:if test="${officer ==loggedOfficer}">
                <display:column>
                    <a class="button" href="question/view.do?questionId=${row.id}"> <spring:message
                            code="application.question.view"/>
                    </a>
                </display:column>
                <display:column>
                    <a class="button2" href="question/edit.do?questionId=${row.id}"> <spring:message
                            code="application.question.edit"/>
                    </a>
                </display:column>
            </jstl:if>
        </security:authorize>

        <spring:message code="question.statement" var="statement1"/>
        <display:column property="statement" title="${statement1}" sortable="true"/>
        <spring:message code="question.answer" var="answer1"/>
        <display:column property="answer" title="${answer1}" sortable="true"/>
        <spring:message code="question.date" var="writeDate1"/>
        <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="writeDate" title="${writeDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="writeDate" title="${writeDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>
        <%--<spring:message code="question.inmigrant" var="applicant1"/>--%>
        <%--<display:column property="applicant" title="${applicant1}" sortable="true"/>--%>
        <spring:message code="question.maker" var="maker1"/>
        <display:column property="maker" title="${maker1}" sortable="true"/>
        <%--<spring:message code="question.relatedApplication" var="relatedApplication1"/>--%>
        <%--<display:column property="relatedApplication" title="${relatedApplication1}" sortable="true"/>--%>

    </display:table>


    <br>
<spring:message code="application.linkedApplications" var="linkedApplications1"/>
<h3><jstl:out value="${linkedApplications1}"/></h3>
<display:table pagesize="10" class="table table-condensed" keepStatus="true"
               name="linkedApplications" requestURI="${requestURI}" id="row">


    <spring:message code="application.ticker" var="ticker1"/>
    <display:column property="ticker" title="${ticker1}" sortable="true"/>

    <spring:message code="application.officer" var="officer1"/>
    <display:column property="officer" title="${officer1}" sortable="true"/>

    <spring:message code="application.status" var="status1"/>
    <display:column property="status" title="${status1}" sortable="true"/>

    <spring:message code="application.applicant" var="applicant1"/>
    <display:column property="applicant" title="${applicant1}" sortable="true"/>

<spring:message code="application.openDate" var="openDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="openDate" title="${openDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

		<spring:message code="application.closeDate" var="closeDate1" />
		<c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="closeDate" title="${closeDate1}"
					sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

    <spring:message code="application.visa" var="visa1"/>
    <display:column property="visa" title="${visa1}" sortable="true"/>
    <spring:message code="application.closed" var="closed1"/>
    <display:column property="closed" title="${closed1}" sortable="true"/>


</display:table>
    <br>

<security:authorize access="hasRole('INMIGRANT')">

    <jstl:if test="${isClosed == true}">
        <a class="button2" href="application/open.do?applicationId=${appID}"> <spring:message
                code="application.open"/>
        </a>

    </jstl:if>
    <jstl:if test="${isClosed != true}">
        <a class="button2" href="application/link.do?applicationId=${appID}"> <spring:message
                code="application.link"/>
        </a> <a class="button2" href="application/close.do?applicationId=${appID}"
           onclick="return confirm('<spring:message code="close.confirm"/>')"> <spring:message
                code="application.close"/>
        </a>
    </jstl:if>

    <a class="button" href="/application/listMy.do"><spring:message code="general.cancel"/></a>

</security:authorize>

<br><br><br>

</div>