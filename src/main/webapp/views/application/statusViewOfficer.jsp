<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="localeCode" value="${pageContext.response.locale}" />
<div class="container">
	<div class="container">
		<!-- Listing grid -->
		<spring:message code="application.accepted" var="accepted1" />
		<h3>
			<jstl:out value="${accepted1}" />
		</h3>


		<display:table pagesize="10" class="table table-condensed"
			keepStatus="true" name="accepted" requestURI="${requestURIaccepted}"
			id="row">
			<security:authorize access="hasRole('INMIGRANT')">
				<display:column>
					<jstl:if test="${row.closed == true}">
						<a class="btn btn-primary"
							href="application/open.do?applicationId=${row.id}"> <spring:message
								code="application.open" />
						</a>
					</jstl:if>
				</display:column>
				<display:column>
					<a href="application/link.do?applicationId=${row.id}"> <spring:message
							code="application.link" />
					</a>
				</display:column>
				<display:column>
					<jstl:if test="${row.closed != true}">
						<a class="btn btn-primary"
							href="application/close.do?applicationId=${row.id}"
							onclick="return confirm('<spring:message code="close.confirm"/>')">
							<spring:message code="application.close" />
						</a>
					</jstl:if>
				</display:column>
				<display:column>
					<a class="btn btn-primary"
						href="application/view.do?applicationId=${row.id}"> <spring:message
							code="application.view" />
					</a>
				</display:column>
			</security:authorize>
			<security:authorize access="hasRole('OFFICER')">
				<display:column>
					<a class="btn btn-primary"
						href="application/view.do?applicationId=${row.id}"> <spring:message
							code="application.view" />
					</a>
				</display:column>
			</security:authorize>
			<spring:message code="application.ticker" var="ticker1" />
			<display:column property="ticker" title="${ticker1}" sortable="true" />

			<spring:message code="application.officer" var="officer1" />
			<display:column property="officer" title="${officer1}"
				sortable="true" />

			<spring:message code="application.status" var="status1" />
			<display:column property="status" title="${status1}" sortable="true" />

			<spring:message code="application.applicant" var="applicant1" />
			<display:column property="applicant" title="${applicant1}"
				sortable="true" />

			<spring:message code="application.openDate" var="openDate1" />
			<c:choose>
				<c:when test="${localeCode == 'en'}">
					<display:column property="openDate" title="${openDate1}"
						sortable="true" format="{0,date,yyyy/MM/dd}" />
				</c:when>
				<c:when test="${localeCode == 'es'}">
					<display:column property="openDate" title="${openDate1}"
						sortable="true" format="{0,date,dd-MM-yyyy}" />
				</c:when>
			</c:choose>

			<spring:message code="application.closeDate" var="closeDate1" />
			<c:choose>
				<c:when test="${localeCode == 'en'}">
					<display:column property="closeDate" title="${closeDate1}"
						sortable="true" format="{0,date,yyyy/MM/dd}" />
				</c:when>
				<c:when test="${localeCode == 'es'}">
					<display:column property="closeDate" title="${closeDate1}"
						sortable="true" format="{0,date,dd-MM-yyyy}" />
				</c:when>
			</c:choose>

			<spring:message code="application.visa" var="visa1" />
			<display:column property="visa" title="${visa1}" sortable="true" />
			<spring:message code="application.closed" var="closed1" />
			<display:column property="closed" title="${closed1}" sortable="true" />

			<security:authorize access="hasRole('OFFICER')">
				<jstl:if test="${row.officer == loggedOfficer}">


					<jstl:if test="${row.status == 'PENDING'}">
						<display:column>
							<a class="button2"
								href="application/accept.do?applicationId=${row.id}"> <spring:message
									code="application.accept" />
							</a>
						</display:column>
						<display:column>
							<a class="btn btn-danger"
								href="application/deny.do?applicationId=${row.id}"> <spring:message
									code="application.deny" />
							</a>
						</display:column>
					</jstl:if>
					<jstl:if test="${row.status == 'ACCEPTED'}">
						<display:column>
							<a class="btn btn-danger"
								href="application/deny.do?applicationId=${row.id}"> <spring:message
									code="application.deny" />
							</a>
						</display:column>
					</jstl:if>
					<jstl:if test="${row.status == 'REJECTED'}">
						<display:column>
							<a class="button2"
								href="application/accept.do?applicationId=${row.id}"> <spring:message
									code="application.accept" />
							</a>
						</display:column>
					</jstl:if>
				</jstl:if>
			</security:authorize>

			<security:authorize access="hasRole('INMIGRANT')">
				<a href="personalSection/view.do?applicationId=${id}"> <spring:message
						code="application.personalSection" /></a>
				<br />
				<a href="socialSection/view.do?applicationId=${id}"><spring:message
						code="application.socialSection" /></a>
				<br />
				<a href="contactSection/view.do?applicationId=${id}"><spring:message
						code="application.contactSection" /></a>
				<br />
				<a href="educationSection/view.do?applicationId=${id}"> <spring:message
						code="application.educationSection" /></a>
				<br />
				<a href="workSection/view.do?applicationId=${id}"><spring:message
						code="application.workSection" /></a>
			</security:authorize>
		</display:table>
	</div>
	<div class="container">
		<spring:message code="application.rejected" var="rejected1" />
		<h3>
			<jstl:out value="${rejected1}" />
		</h3>


		<display:table pagesize="10" class="table table-condensed"
			keepStatus="true" name="rejected" requestURI="${requestURIrejected}"
			id="row">
			<security:authorize access="hasRole('INMIGRANT')">
				<display:column>
					<jstl:if test="${row.closed == true}">
						<a class="btn btn-primary"
							href="application/open.do?applicationId=${row.id}"> <spring:message
								code="application.open" />
						</a>
					</jstl:if>
				</display:column>
				<display:column>
					<a class="btn btn-primary"
						href="application/link.do?applicationId=${row.id}"> <spring:message
							code="application.link" />
					</a>
				</display:column>
				<display:column>
					<jstl:if test="${row.closed != true}">
						<a class="btn btn-primary"
							href="application/close.do?applicationId=${row.id}"
							onclick="return confirm('<spring:message code="close.confirm"/>')">
							<spring:message code="application.close" />
						</a>
					</jstl:if>
				</display:column>
				<display:column>
					<a class="btn btn-primary"
						href="application/view.do?applicationId=${row.id}"> <spring:message
							code="application.view" />
					</a>
				</display:column>
			</security:authorize>
			<security:authorize access="hasRole('OFFICER')">
				<display:column>
					<a class="btn btn-primary"
						href="application/view.do?applicationId=${row.id}"> <spring:message
							code="application.view" />
					</a>
				</display:column>
			</security:authorize>
			<spring:message code="application.ticker" var="ticker1" />
			<display:column property="ticker" title="${ticker1}" sortable="true" />

			<spring:message code="application.officer" var="officer1" />
			<display:column property="officer" title="${officer1}"
				sortable="true" />

			<spring:message code="application.status" var="status1" />
			<display:column property="status" title="${status1}" sortable="true" />

			<spring:message code="application.applicant" var="applicant1" />
			<display:column property="applicant" title="${applicant1}"
				sortable="true" />

			<spring:message code="application.openDate" var="openDate1" />
			<c:choose>
				<c:when test="${localeCode == 'en'}">
					<display:column property="openDate" title="${openDate1}"
						sortable="true" format="{0,date,yyyy/MM/dd}" />
				</c:when>
				<c:when test="${localeCode == 'es'}">
					<display:column property="openDate" title="${openDate1}"
						sortable="true" format="{0,date,dd-MM-yyyy}" />
				</c:when>
			</c:choose>

			<spring:message code="application.closeDate" var="closeDate1" />
			<c:choose>
				<c:when test="${localeCode == 'en'}">
					<display:column property="closeDate" title="${closeDate1}"
						sortable="true" format="{0,date,yyyy/MM/dd}" />
				</c:when>
				<c:when test="${localeCode == 'es'}">
					<display:column property="closeDate" title="${closeDate1}"
						sortable="true" format="{0,date,dd-MM-yyyy}" />
				</c:when>
			</c:choose>

			<spring:message code="application.visa" var="visa1" />
			<display:column property="visa" title="${visa1}" sortable="true" />
			<spring:message code="application.closed" var="closed1" />
			<display:column property="closed" title="${closed1}" sortable="true" />

			<jstl:if test="${row.officer == loggedOfficer}">
				<jstl:if test="${row.status == 'PENDING'}">
					<display:column>
						<a class="button2"
							href="application/accept.do?applicationId=${row.id}"> <spring:message
								code="application.accept" />
						</a>
					</display:column>
					<display:column>
						<a class="btn btn-danger"
							href="application/deny.do?applicationId=${row.id}"> <spring:message
								code="application.deny" />
						</a>
					</display:column>
				</jstl:if>
				<jstl:if test="${row.status == 'ACCEPTED'}">
					<display:column>
						<a class="btn btn-danger"
							href="application/deny.do?applicationId=${row.id}"> <spring:message
								code="application.deny" />
						</a>
					</display:column>
				</jstl:if>
				<jstl:if test="${row.status == 'REJECTED'}">
					<display:column>
						<a class="button2"
							href="application/accept.do?applicationId=${row.id}"> <spring:message
								code="application.accept" />
						</a>
					</display:column>
				</jstl:if>
			</jstl:if>


			<security:authorize access="hasRole('INMIGRANT')">
				<a href="personalSection/view.do?applicationId=${id}"> <spring:message
						code="application.personalSection" /></a>
				<br />
				<a href="socialSection/view.do?applicationId=${id}"><spring:message
						code="application.socialSection" /></a>
				<br />
				<a href="contactSection/view.do?applicationId=${id}"><spring:message
						code="application.contactSection" /></a>
				<br />
				<a href="educationSection/view.do?applicationId=${id}"> <spring:message
						code="application.educationSection" /></a>
				<br />
				<a href="workSection/view.do?applicationId=${id}"><spring:message
						code="application.workSection" /></a>
			</security:authorize>
		</display:table>
	</div>
	<div class="container">

		<spring:message code="application.pending" var="pending1" />
		<h3>
			<jstl:out value="${pending1}" />
		</h3>
		<display:table pagesize="10" class="table table-condensed"
			keepStatus="true" name="pending" requestURI="${requestURIpending}"
			id="row">
			<security:authorize access="hasRole('INMIGRANT')">
				<display:column>
					<jstl:if test="${row.closed == true}">
						<a class="btn btn-primary"
							href="application/open.do?applicationId=${row.id}"> <spring:message
								code="application.open" />
						</a>
					</jstl:if>
				</display:column>
				<display:column>
					<a class="btn btn-primary"
						href="application/link.do?applicationId=${row.id}"> <spring:message
							code="application.link" />
					</a>
				</display:column>
				<display:column>
					<jstl:if test="${row.closed != true}">
						<a class="btn btn-primary"
							href="application/close.do?applicationId=${row.id}"
							onclick="return confirm('<spring:message code="close.confirm"/>')">
							<spring:message code="application.close" />
						</a>
					</jstl:if>
				</display:column>
				<display:column>
					<a class="btn btn-primary"
						href="application/view.do?applicationId=${row.id}"> <spring:message
							code="application.view" />
					</a>
				</display:column>
			</security:authorize>
			<security:authorize access="hasRole('OFFICER')">
				<display:column>
					<a class="btn btn-primary"
						href="application/view.do?applicationId=${row.id}"> <spring:message
							code="application.view" />
					</a>
				</display:column>
			</security:authorize>

			<spring:message code="application.ticker" var="ticker1" />
			<display:column property="ticker" title="${ticker1}" sortable="true" />

			<spring:message code="application.officer" var="officer1" />
			<display:column property="officer" title="${officer1}"
				sortable="true" />

			<spring:message code="application.status" var="status1" />
			<display:column property="status" title="${status1}" sortable="true" />

			<spring:message code="application.applicant" var="applicant1" />
			<display:column property="applicant" title="${applicant1}"
				sortable="true" />

			<spring:message code="application.openDate" var="openDate1" />
			<c:choose>
				<c:when test="${localeCode == 'en'}">
					<display:column property="openDate" title="${openDate1}"
						sortable="true" format="{0,date,yyyy/MM/dd}" />
				</c:when>
				<c:when test="${localeCode == 'es'}">
					<display:column property="openDate" title="${openDate1}"
						sortable="true" format="{0,date,dd-MM-yyyy}" />
				</c:when>
			</c:choose>

			<spring:message code="application.closeDate" var="closeDate1" />
			<c:choose>
				<c:when test="${localeCode == 'en'}">
					<display:column property="closeDate" title="${closeDate1}"
						sortable="true" format="{0,date,yyyy/MM/dd}" />
				</c:when>
				<c:when test="${localeCode == 'es'}">
					<display:column property="closeDate" title="${closeDate1}"
						sortable="true" format="{0,date,dd-MM-yyyy}" />
				</c:when>
			</c:choose>

			<spring:message code="application.visa" var="visa1" />
			<display:column property="visa" title="${visa1}" sortable="true" />
			<spring:message code="application.closed" var="closed1" />
			<display:column property="closed" title="${closed1}" sortable="true" />
			<security:authorize access="hasRole('OFFICER')">
				<jstl:if test="${row.officer == loggedOfficer}">


					<jstl:if test="${row.status == 'PENDING'}">
						<display:column>
							<a class="button2"
								href="application/accept.do?applicationId=${row.id}"> <spring:message
									code="application.accept" />
							</a>
						</display:column>
						<display:column>
							<a class="btn btn-danger"
								href="application/deny.do?applicationId=${row.id}"> <spring:message
									code="application.deny" />
							</a>
						</display:column>
					</jstl:if>
					<jstl:if test="${row.status == 'ACCEPTED'}">
						<display:column>
							<a class="btn btn-danger"
								href="application/deny.do?applicationId=${row.id}"> <spring:message
									code="application.deny" />
							</a>
						</display:column>
					</jstl:if>
					<jstl:if test="${row.status == 'REJECTED'}">
						<display:column>
							<a class="button2"
								href="application/accept.do?applicationId=${row.id}"> <spring:message
									code="application.accept" />
							</a>
						</display:column>
					</jstl:if>
				</jstl:if>
			</security:authorize>
	</div>
	<security:authorize access="hasRole('INMIGRANT')">
		<a href="personalSection/view.do?applicationId=${id}"> <spring:message
				code="application.personalSection" /></a>
		<br />
		<a href="socialSection/view.do?applicationId=${id}"><spring:message
				code="application.socialSection" /></a>
		<br />
		<a href="contactSection/view.do?applicationId=${id}"><spring:message
				code="application.contactSection" /></a>
		<br />
		<a href="educationSection/view.do?applicationId=${id}"> <spring:message
				code="application.educationSection" /></a>
		<br />
		<a href="workSection/view.do?applicationId=${id}"><spring:message
				code="application.workSection" /></a>
	</security:authorize>
	</display:table>
	<br> <br> <br>

</div>