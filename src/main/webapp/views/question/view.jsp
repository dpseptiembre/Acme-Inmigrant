<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">


    <%--<security:authorize access="hasRole('ADMINISTRATOR')">--%>
    <%--<a class="btn btn-primary" href="question/create.do"> <spring:message code="general.create" /></a>--%>
    <%--</security:authorize>--%>
    <!-- Listing grid -->

    <spring:message code="question.statement" var="statement1"/>
    <h3><jstl:out value="${statement1}"/></h3>
    <jstl:out value="${statement}"/>
    <br>
    <spring:message code="question.date" var="writeDate1"/>
    <h3><jstl:out value="${writeDate1}"/></h3>
    <jstl:out value="${writeDate}"/>
    <br>
    <spring:message code="question.answer" var="answer1"/>
    <h3><jstl:out value="${answer1}"/></h3>
    <jstl:out value="${answer}"/>
    <br>
    <spring:message code="question.inmigrant" var="applicant1"/>
    <h3><jstl:out value="${applicant1}"/></h3>
    <jstl:out value="${applicant}"/>
    <br>
    <spring:message code="question.maker" var="maker1"/>
    <h3><jstl:out value="${maker1}"/></h3>
    <jstl:out value="${maker}"/>
    <br>
    <spring:message code="question.relatedApplication" var="relatedApplication1"/>
    <h3><jstl:out value="${relatedApplication1}"/></h3>
    <jstl:out value="${relatedApplication}"/>
    <br>
    <input class="button" type="button" name="goBack"
           value="<spring:message code="general.goback" />"
           onclick="relativeRedir('application/view.do?applicationId=${relatedAppId}');"/>
    <br/>

    <br>
    <br>
</div>