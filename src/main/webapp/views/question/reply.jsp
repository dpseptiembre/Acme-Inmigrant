<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>
<div class="container">
    <form:form action="question/reply.do" modelAttribute="question">

        <form:hidden path="id"/>
        <form:hidden path="version"/>
        <form:hidden path="applicant"/>
        <form:hidden path="maker"/>
        <form:hidden path="relatedApplication"/>
        <form:hidden path="writeDate"/>
        <form:hidden path="statement"/>

        <b><spring:message code="question.statement" var="statementt"/></b>
        <p><jstl:out value="${statementt}"/></p>

        <b> <acme:textbox path="answer" code="question.answer"/></b>
        <br/>

        <!---------------------------- BOTONES -------------------------->
        <input class="button2" type="submit" name="save"
               value="<spring:message code="general.save"/>"
        />&nbsp;

        <input class="button" type="button" name="cancel"
               value="<spring:message code="general.cancel" />"
               onclick="relativeRedir('application/view.do?applicationId=${question.relatedApplication.id}');"/>
        <br/>


    </form:form>
</div>