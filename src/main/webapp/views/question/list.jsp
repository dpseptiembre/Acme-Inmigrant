<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="container">
    <security:authorize access="hasRole('ADMINISTRATOR')">
        <a class="btn btn-primary" href="question/create.do"> <spring:message code="general.create"/></a>
    </security:authorize>
    <!-- Listing grid -->
    <display:table pagesize="10" class="displaytag" keepStatus="true"
                   name="categories" requestURI="${requestURI}" id="row">
<c:set var="localeCode" value="${pageContext.response.locale}" />

        <spring:message code="question.statement" var="statement1"/>
        <display:column property="statement" title="${statement1}" sortable="true"/>
        <spring:message code="question.answer" var="answer1"/>
        <display:column property="answer" title="${answer1}" sortable="true"/>
        <spring:message code="question.date" var="writeDate1"/>
        <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="writeDate" title="${writeDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="writeDate" title="${writeDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>
        <spring:message code="question.inmigrant" var="applicant1"/>
        <display:column property="applicant" title="${applicant1}" sortable="true"/>
        <spring:message code="question.maker" var="maker1"/>
        <display:column property="maker" title="${maker1}" sortable="true"/>
        <spring:message code="question.relatedApplication" var="relatedApplication1"/>
        <display:column property="relatedApplication1" title="${relatedApplication1}" sortable="true"/>

    </display:table>

</div>