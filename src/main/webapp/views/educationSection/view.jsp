<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="localeCode" value="${pageContext.response.locale}" />
<spring:message code="educationSection.degree" var="degree1" />
<h3>
	<jstl:out value="${degree1}" />
</h3>
<jstl:out value="${degree}" />

<spring:message code="educationSection.institution" var="institution1" />
<h3>
	<jstl:out value="${institution1}" />
</h3>
<jstl:out value="${institution}" />

<spring:message code="educationSection.awardedDate" var="awardedDate1" />
<c:choose>
	<c:when test="${localeCode == 'en'}">
		<h3>
			<jstl:out value="${awardedDate1}"  />
		</h3>
		<jstl:out value="${awardedDate}" />
	</c:when>
	<c:when test="${localeCode == 'es'}">
		<h3>
			<jstl:out value="${awardedDate1}"  />
		</h3>
		<jstl:out value="${awardedDate}"/>
	</c:when>
</c:choose>

<spring:message code="educationSection.level" var="level1" />
<h3>
	<jstl:out value="${level1}" />
</h3>
<jstl:out value="${level}" />


<security:authorize access="hasRole('INMIGRANT')">
	<a class="button2"
		href="educationSection/edit.do?educationSectionId=${id}"> <spring:message
			code="application.educationSection.edit" />
	</a>
</security:authorize>

<a class="button" href="/educationSection/list.do"><spring:message
		code="general.cancel" /></a>
