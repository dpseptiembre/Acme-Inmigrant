<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
<c:set var="localeCode" value="${pageContext.response.locale}" />
    <security:authorize access="hasRole('INIGRANT')">
        <a class="btn btn-primary" href="educationSection/create.do"> <spring:message code="application.create"/></a>
    </security:authorize>
    <!-- Listing grid -->
    <display:table pagesize="10" class="table table-condensed" keepStatus="true"
                   name="educationSections" requestURI="${requestURI}" id="row">

        <spring:message code="educationSection.degree" var="degree1"/>
        <display:column property="degree" title="${degree1}" sortable="true"/>
        <spring:message code="educationSection.institution" var="institution1"/>
        <display:column property="institution" title="${institution1}" sortable="true"/>
        <spring:message code="educationSection.awardedDate" var="awardedDate1"/>
        <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="link" title="${awardedDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="link" title="${awardedDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>
        
        <spring:message code="educationSection.level" var="level1"/>
        <display:column property="level" title="${level1}" sortable="true"/>


        <security:authorize access="hasRole('INMIGRANT')">
            <display:column>
                <a href="educationSection/edit.do?educationSectionId=${row.id}"> <spring:message
                        code="application.educationSection.edit"/>
                </a>
            </display:column>
        </security:authorize>
    </display:table>
</div>