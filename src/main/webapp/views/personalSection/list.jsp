<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
<c:set var="localeCode" value="${pageContext.response.locale}" />
    <security:authorize access="hasRole('INMIGRANT')">
        <a class="btn btn-primary" href="personalSection/create.do"> <spring:message code="application.create"/></a>
    </security:authorize>
    <!-- Listing grid -->
    <display:table pagesize="10" class="table table-condensed" keepStatus="true"
                   name="personalSections" requestURI="${requestURI}" id="row">

        <security:authorize access="hasRole('INMIGRANT')">
            <display:column>
                <a href="personalSection/edit.do?personalSectionId=${id}"> <spring:message
                        code="application.personalSection.edit"/>
                </a>
            </display:column>
        </security:authorize>

        <spring:message code="personalSection.names" var="names1"/>
        <display:column property="names" title="${names1}" sortable="true"/>

        <spring:message code="personalSection.birthPlace" var="birthPlace1"/>
        <display:column property="birthPlace" title="${birthPlace1}" sortable="true"/>

        <spring:message code="personalSection.birthDate" var="birthDate1"/>
        <c:choose>
			<c:when test="${localeCode == 'en'}">
				<display:column property="birthDate" title="${birthDate1}" sortable="true" format="{0,date,yyyy/MM/dd}" />
			</c:when>
			<c:when test="${localeCode == 'es'}">
				<display:column property="birthDate" title="${birthDate1}" sortable="true" format="{0,date,dd-MM-yyyy}" />
			</c:when>
		</c:choose>

        <spring:message code="personalSection.picture" var="picture1"/>
        <display:column property="picture" title="${picture1}" sortable="true"/>

    </display:table>
</div>