<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>




<div class="container">

    <p><jstl:out value="${report.text}"/></p>

    <h2><jstl:out value="${spam}"/></h2>
    <a href="report/createAttachment.do?reportId=${report.id}" class="btn btn-primary"><spring:message code="general.add.pucture" /></a>

    <table class="table table-striped">
        <c:forEach var = "listValue" items = "${attachments}">
            <tr>
                <td>
                    <a href="${listValue}"><c:out value="${listValue}"/></a>
                </td>
                </td>
                <td>
                    <a href="report/deleteAttachment.do?attachment=${listValue}&reportId=${report.id}" class="btn btn-danger"><spring:message code="general.delete" /></a>
                </td>
            </tr>
        </c:forEach>

    </table>

</div>
