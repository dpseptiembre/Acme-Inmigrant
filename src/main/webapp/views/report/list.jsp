<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<div class="container">
<!-- Listing grid -->
<display:table pagesize="10" class="displaytag" keepStatus="true"
               name="reports" requestURI="${requestURI}" id="row">

    <spring:message code="report.text" var="text"/>
    <display:column property="text" title="${text}" sortable="true"/>

    <spring:message code="report.attendant" var="attendant"/>
    <display:column property="attendant" title="${attendant}" sortable="true"/>

        <display:column>
            <a href="report/view.do?reportId=${row.id}" class="btn btn-primary">
                <spring:message code="general.view"/>
            </a>
        </display:column>


</display:table>
</div>