<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container">


    <form:form action="report/saveAttachment.do" method="get">

        <input placeholder="<spring:message code="report.enter.attachment"/>" name="attachment" class="form-group"
               type="text"/>
        <input type="hidden" name="reportId" value="${id}">
        <input type="submit" value="<spring:message code="general.save"/>" class="btn btn-primary"/>
    </form:form>


    <%--<form:form action="application/link.do" modelAttribute="application">--%>

    <%--<form:hidden path="id"/>--%>
    <%--<form:hidden path="version"/>--%>
    <%--<form:hidden path="officer"/>--%>

    <%--<form:hidden path="applicant"/>--%>
    <%--<form:hidden path="openDate"/>--%>
    <%--<form:hidden path="closeDate"/>--%>
    <%--<form:hidden path="statusChange"/>--%>
    <%--<form:hidden path="statusComment"/>--%>
    <%--<form:hidden path="visa"/>--%>
    <%--&lt;%&ndash;<form:hidden path="ticker"/>&ndash;%&gt;--%>
    <%--<form:hidden path="closed"/>--%>
    <%--<form:hidden path="linkedApplications"/>--%>

    <%--<form:hidden path="personalSection"/>--%>
    <%--<form:hidden path="contactSections"/>--%>
    <%--<form:hidden path="socialSections"/>--%>
    <%--<form:hidden path="educationSections"/>--%>
    <%--<form:hidden path="workSections"/>--%>


    <%--<br/>--%>

    <%--<acme:textbox path="ticker" code="application.ticker"/>--%>
    <%--<br/>--%>

    <%--<!---------------------------- BOTONES -------------------------->--%>
    <%--<input type="submit" class="btn btn-primary" name="save"--%>
    <%--value="<spring:message code="application.save"/>"--%>
    <%--/>&nbsp;--%>


    <%--<input type="button" name="cancel" class="btn btn-warning"--%>
    <%--value="<spring:message code="general.cancel" />"--%>
    <%--onclick="relativeRedir('application/view.do?applicationId=${application.id}');"/>--%>
    <%--</form:form>--%>

    <%--<br>--%>
    <%--<br>--%>

</div>

