<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<div class="container">
<form:form action="report/edit.do" modelAttribute="report">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="owner"/>
    <form:hidden path="attendant"/>
    <form:hidden path="pictures"/>


    <acme:textbox path="text" code="report.text"/>
    <br/>
    <input placeholder="Picture to add" name="pictures" class="form-group" type="text"/>

<br>
    <!---------------------------- BOTONES -------------------------->
    <input type="submit" name="save" class="btn btn-primary"
           value="<spring:message code="country.save"/>"/>&nbsp;

    <input type="button" name="cancel" class="btn btn-warning"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('inmigrant/listUnderInvestigation.do');"/>
    <br/>


</form:form>
</div>