/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Application;
import domain.Officer;
import domain.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface OfficerRepository extends JpaRepository<Officer, Integer> {

   @Query("select officer from Officer officer where officer.userAccount.id = ?1")
   Officer findByUserAccountId(int userAccountId);

   @Query("select a from Application a where a.officer.id = ?1 and a.status = 0")
   Collection<Application> acceptedApplicationsInWichImOfficer(int officerId);

   @Query("select a from Application a where a.officer.id = ?1 and a.status = 1")
   Collection<Application> rejectedApplicationsInWichImOfficer(int officerId);

   @Query("select a from Application a where a.officer.id = ?1 and a.status = 2")
   Collection<Application> pendingApplicationsInWichImOfficer(int officerId);

   @Query("select a.reports from Investigator a join a.underInvestigation i join i.applications e where e.officer.id = ?1")
   Collection<Report> reportOfApplicationThatIOffice(int officerId);
}
