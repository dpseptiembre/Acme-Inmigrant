/*
 * AdministratorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Inmigrant;
import domain.Investigator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface InvestigatorRepository extends JpaRepository<Investigator, Integer> {

    @Query("select officer from Investigator officer where officer.userAccount.id = ?1")
    Investigator findByUserAccountId(int userAccountId);


    @Query("select c.underInvestigation from Investigator c where c.id=?1")
   Collection<Inmigrant> underInvestigation(int investigatorId);

}
