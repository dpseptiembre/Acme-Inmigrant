/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Inmigrant;
import domain.Officer;
import domain.Supervisor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Repository
public interface SupervisorRepository extends JpaRepository<Supervisor, Integer> {


    @Query("select c from Supervisor c where c.userAccount.id = ?1")
    Supervisor findByUserAccountId(int userAccountId);

    @Query("select officers from Officer officers where officers.lastEvaluationDate < ?1")
    Set<Officer> officerWithMoreThan(Date XMonths);

    @Query("select officers from Officer officers where officers.lastEvaluationDate > ?1")
    Set<Officer> officerWithLessThan(Date XMonths);

    @Query("select officers from Officer officers where officers.lastEvaluationDate between ?1 and ?2")
    Set<Officer> officerBetewnThan(Date startDate, Date endDate);

    @Query("select c.receiver from Evaluation c where c.owner.id = ?1")
    Set<Officer> myOfficers(int supervisorID);
}
