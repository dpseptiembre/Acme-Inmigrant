/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Finder;
import domain.FinderMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;

@Repository
public interface FinderMessageRepository extends JpaRepository<FinderMessage, Integer> {

    @Query("select c from FinderMessage c where c.owner.id = ?1 and c.creationMoment > ?2")
    Collection<FinderMessage> findermessagebyactor(int actorId, Date cacheHour);

}
