/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {


 @Query("select c from Administrator c where c.userAccount.id = ?1")
 Administrator findByUserAccountId(int userAccountId);

 @Query("select avg(applications.size) from Inmigrant")
 Double averageNumberOfApplicationsPerUser();
 @Query("select max(applications.size) from Inmigrant")
 int maxNumberOfApplicationPerUser();
 @Query("select min(applications.size) from Inmigrant ")
 int minNumberOfApplicationPerUser();
 @Query("select stddev(applications.size) from Inmigrant")
 Double standarDesviationOfApplicationPerUser();

 @Query("select avg(applications.size) from Officer")
 Double averageNumberOfApplicationsPerOfficer();
 @Query("select max(applications.size) from Officer")
 int maxNumberOfApplicationsPerOfficer();
 @Query("select min(applications.size) from Officer")
 int minNumberOfApplicationPerOfficer();
 @Query("select stddev(applications.size) from Officer")
 Double standarDesviationOfApplicationPerOfficer();

 @Query("select avg(v.price) from Visa v where v.hidden = false")
 Double averageNumberofVisaPrices();
 @Query("select max(v.price) from Visa v where v.hidden = false")
 int maxNumberOfVisaPrices();
 @Query("select min(v.price) from Visa v where v.hidden = false")
 int minNumberOfVisaPrices();
 @Query("select stddev(v.price) from Visa v where v.hidden = false")
 Double standarDesviationOfVisaPrices();

 @Query("select avg(reports.size) from Investigator")
 Double averageNumberOfInmigrantIvestigatedPerInvestigator();
 @Query("select max(reports.size) from Investigator")
 int maxNumberOfInmigrantIvestigatedPerInvestigator();
 @Query("select min(reports.size) from Investigator")
 int minNumberOfInmigrantIvestigatedPerInvestigator();
 @Query("select stddev(reports.size) from Investigator")
 Double standarDesviationOfNumberOfInmigrantIvestigatedPerInvestigator();


 @Query("select ap.elapsedTime from Application ap")
 List<Long> elapsedTimes();

 @Query("select avg(ap.elapsedTime) from Application ap")
 Double averageNumberOfElapsedTimes();
 @Query("select max(ap.elapsedTime) from Application ap")
 int maxNumberOfElapsedTimes();
 @Query("select min(ap.elapsedTime) from Application ap")
 int minNumberOfElapsedTimes();
 @Query("select stddev(ap.elapsedTime) from Application ap")
 Double standarDesviationNumberOfElapsedTime();



 @Query("select avg(c.visas.size) from Category c where c.hidden = false")
 Double averageNumberOfVisaPerCategory();
 @Query("select max(c.visas.size) from Category c where c.hidden = false")
 int maxNumberOfVisaPerCategory();
 @Query("select min(c.visas.size) from Category c where c.hidden = false")
 int minNumberOfVisaPerCategory();
 @Query("select stddev(c.visas.size) from Category c where c.hidden = false")
 Double standarDesviationNumberOfVisaPerCategory();

 @Query("select avg(c.legislation.size) from Country c where c.hidden = false")
 Double averageNumberOfLawsPerCountry();
 @Query("select max(c.legislation.size) from Country c where c.hidden = false")
 int maxNumberOfLawsPerCountry();
 @Query("select min(c.legislation.size) from Country c where c.hidden = false")
 int minNumberOfLawsPerCountry();
 @Query("select stddev(c.legislation.size) from Country c where c.hidden = false")
 Double standarDesviationOfLawsPerCountry();

 @Query("select avg(c.requirements.size) from Visa c where c.hidden = false")
 Double averageNumberOfRequirementsPerVisa();
 @Query("select max(c.requirements.size) from Visa c where c.hidden = false")
 int maxNumberOfRequirementsPerVisa();
 @Query("select min(c.requirements.size) from Visa c where c.hidden = false")
 int minNumberOfRequirementsPerVisa();
 @Query("select stddev(c.requirements.size) from Visa c where c.hidden = false")
 Double standarDesviationOfRequirementsPerVisa();

 @Query("select avg(mezzages.size) from Folder")
 Double averageNumberOfMessagesPerFolder();
 @Query("select max(mezzages.size) from Folder")
 int maxNumberOfMessagesPerFolder();
 @Query("select min(mezzages.size) from Folder")
 int minNumberOfMessagesPerFolder();
 @Query("select stddev(mezzages.size) from Folder")
 Double standarDesviationOfMesasgesPerFolder();

 @Query("select avg(f.mezzages.size) from Actor a join a.folders f")
 Double averageNumberOfMessagesPerActor();
 @Query("select max(f.mezzages.size) from Actor a join a.folders f")
 int maxNumberOfMessagesPerActor();
 @Query("select min(f.mezzages.size) from Actor a join a.folders f")
 int minNumberOfMessagesPerActor();
 @Query("select stddev(f.mezzages.size) from Actor a join a.folders f")
 Double standarDesviationOfMesasgesPerActor();


 @Query("select count(m) from Mezzage m where m.spam = true and m.hidden = false")
 int mezzagesWithSpam();
 @Query("select count(m) from Mezzage m where m.hidden = false ")
 int totalMezzages();
}
