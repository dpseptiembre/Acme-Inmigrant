/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Application;
import domain.Inmigrant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface InmigrantRepository extends JpaRepository<Inmigrant, Integer> {


   @Query("select c from Inmigrant c where c.userAccount.id = ?1")
   Inmigrant findByUserAccountId(int userAccountId);

   @Query("select c from Application c where c.closed = false and c.applicant.id =?1")
   Collection<Application> oppened(int userId);


   @Query("select c from Application c where c.closed = true and c.status=0 and c.applicant.id =?1")
   Collection<Application> closedAndAccepted(int userId);


   @Query("select c from Application c where c.closed = true and c.status=1 and c.applicant.id =?1")
   Collection<Application> closedAndDenied(int userId);


   @Query("select c from Application c where c.closed = true and c.status=2 and c.applicant.id =?1")
   Collection<Application> closedAndAwaiting(int userId);



}
