/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public interface MezzageRepository extends JpaRepository<Mezzage, Integer> {


    @Query("select c from Mezzage c where c.folder.folderType = 0 and c.hidden = false")
    Collection<Mezzage> publicMezzages();

    @Query("select c from Mezzage c where c.body like ?1 and c.hidden = false and c.sendDate between ?2 and ?3")
    List<Mezzage> sarchedMezzages(String keyword, Date startDate, Date endDate);

   @Query("select c from Mezzage c where c.folder.folderType = 0 and c.hidden = false and (c.body LIKE CONCAT('%',:spamWord,'%') or c.subject LIKE CONCAT('%',:spamWord,'%'))")
   Collection<Mezzage> spamMezzage(@Param("spamWord") String spamWord);

   @Query("select c from Folder c where c.owner.id = ?1")
   Collection<Folder>getFolderByOwner(int ownerId);

   @Query("select c from Actor c where c.email = ?1")
   List<Actor> getActorByEmail(String email);

}
