/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Country;
import domain.Visa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface VisaRepository extends JpaRepository<Visa, Integer> {


   @Query("select m from Visa v join v.country c join c.offeredVisas m where v=?1")
   Collection<Country> countriesFromVisa(Visa visa);

   @Query("Select c.offeredVisas from Country c where c.id=?1")
   Collection<Visa> visasFromCountry(int countryId);

   @Query("select a.visa from Inmigrant c join c.applications a where c.id =?1")
   Collection<Visa> myVisas(int inmigrantId);


   @Query("select v from Visa v where v.hidden=false")
   Collection<Visa> visasNotHidden();

}
