/*
 * AdministratorRepository.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer> {


   @Query("select c from Application c where c.ticker = ?1")
   Application getApplicationByTicker(String ticker);

   @Query("select c from Application  c where c.officer IS NULL")
   Collection<Application> applicationswithoutOfficer();

   @Query("select c from Application c where c.visa.country.id =?1")
   Collection<Application> applicationsFromCountry(int countryId);

}
