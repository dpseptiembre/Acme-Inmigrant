package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class EducationSection extends DomainEntity {


   private String degree, institution;
   private Date awardedDate;
   private Level level;
   private Application application;
   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDegree() {
      return degree;
   }

   public void setDegree(String degree) {
      this.degree = degree;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getInstitution() {
      return institution;
   }

   public void setInstitution(String institution) {
      this.institution = institution;
   }

   @Temporal(TemporalType.DATE)
   @Past
   public Date getAwardedDate() {
      return awardedDate;
   }

   public void setAwardedDate(Date awardedDate) {
      this.awardedDate = awardedDate;
   }

   public Level getLevel() {
      return level;
   }

   public void setLevel(Level level) {
      this.level = level;
   }

   @ManyToOne
   public Application getApplication() {
      return application;
   }

   public void setApplication(Application application) {
      this.application = application;
   }

}
