/*
 * Inmigrant.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Inmigrant extends Actor {

	// Constructors -----------------------------------------------------------

   private CreditCard creditCard;
   private Collection<Application> applications;
   private Collection<Question> questionsForMe;
   private Collection<Report> reports;


	// Attributes -------------------------------------------------------------

	// Relationships ----------------------------------------------------------


   public Inmigrant() {
      super();
   }

   @OneToOne(cascade = CascadeType.ALL)
   public CreditCard getCreditCard() {
      return creditCard;
   }

   public void setCreditCard(CreditCard creditCard) {
      this.creditCard = creditCard;
   }

   @OneToMany
   public Collection<Question> getQuestionsForMe() {
      return questionsForMe;
   }

   public void setQuestionsForMe(Collection<Question> questionsForMe) {
      this.questionsForMe = questionsForMe;
   }

   @OneToMany(mappedBy = "applicant")
   public Collection<Application> getApplications() {
      return applications;
   }

   public void setApplications(Collection<Application> applications) {
      this.applications = applications;
   }

   @OneToMany
   public Collection<Report> getReports() {
      return reports;
   }


   public void setReports(Collection<Report> reports) {
      this.reports = reports;
   }

   @Override
   public String toString() {
      return super.getSurname() + ", " + super.getName();
   }
}

