package domain;

import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Finder extends DomainEntity {

   // Attributes ------------------------------------------------------------


   private String keyword;
   private Collection<Visa> results;
   // Relationships ---------------------------------------------------------

   // Constructors -----------------------------------------------------------
   public Finder() {
      super();
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getKeyword() {
      return keyword;
   }

   public void setKeyword(String keyword) {
      this.keyword = keyword;
   }

   @OneToMany
   public Collection<Visa> getResults() {
      return results;
   }

   public void setResults(Collection<Visa> results) {
      this.results = results;
   }
}