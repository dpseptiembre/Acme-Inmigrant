/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

public enum Level {

   NONE, ELEMENTARY, PRIMARY, SECONDARY, HIGH, BACHELOR, UNIVERSITY_DEGREE, MASTER, DOCTORATE
}
