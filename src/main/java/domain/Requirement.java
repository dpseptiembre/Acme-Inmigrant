package domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class Requirement extends DomainEntity {

   private String title, description;
   private Boolean abrogated;
   private Law law;
   private Boolean hidden;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Boolean getAbrogated() {
      return abrogated;
   }

   public void setAbrogated(Boolean abrogated) {
      this.abrogated = abrogated;


   }

   @NotNull
   @OneToOne
   public Law getLaw() {
      return law;
   }

   public void setLaw(Law law) {
      this.law = law;
   }

   public Boolean getHidden() {
      return hidden;
   }

   public void setHidden(Boolean hidden) {
      this.hidden = hidden;
   }

   @Override
   public String toString() {
      return title +
              " [" + description + "]";
   }
}
