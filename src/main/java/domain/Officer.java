/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Access(AccessType.PROPERTY)
public class Officer extends Actor {

	// Constructors -----------------------------------------------------------

	// Attributes -------------------------------------------------------------
   private Collection<Question> questions;
   private Collection<Evaluation> evaluations;
   private Collection<Application> applications;
   private boolean underSupervision;
   private Supervisor supervisor;
   public Officer() {
      super();
	}

	// Relationships ----------------------------------------------------------

   @OneToMany
   public Collection<Question> getQuestions() {
      return questions;
   }

   public void setQuestions(Collection<Question> questions) {
      this.questions = questions;
   }

   @OneToMany
   public Collection<Evaluation> getEvaluations() {
      return evaluations;
   }

   public void setEvaluations(Collection<Evaluation> evaluations) {
      this.evaluations = evaluations;
   }

   @OneToMany(mappedBy = "officer")
   public Collection<Application> getApplications() {
      return applications;
   }

   public void setApplications(Collection<Application> applications) {
      this.applications = applications;
   }

   @Override
   public String toString() {
      return super.getSurname() + ", " + super.getName();
   }

   @NotNull
   public boolean isUnderSupervision() {
      return underSupervision;
   }

   public void setUnderSupervision(boolean underEvaluation) {
      this.underSupervision = underEvaluation;
   }

   public Date getLastEvaluationDate() {
      try {
         if (this.getEvaluations().isEmpty()){
            return new Date();
         }else {
            List<Evaluation> evaluations = new ArrayList<>(this.getEvaluations());
            Evaluation lastEvaluatio = evaluations.get(evaluations.size()-1);
            Date lastDate = lastEvaluatio.getPostDate();
            return lastDate;
         }
      } catch (Exception e) {
         e.printStackTrace();
         return new Date();
      }
   }
   public void setLastEvaluationDate(Date lastEvaluationDate) {
   }

   @ManyToOne
   public Supervisor getSupervisor() {
      return supervisor;
   }

   public void setSupervisor(Supervisor supervisor) {
      this.supervisor = supervisor;
   }
}
