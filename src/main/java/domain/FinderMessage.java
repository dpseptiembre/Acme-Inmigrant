package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class FinderMessage extends DomainEntity {

   // Attributes ------------------------------------------------------------


   private String keyword;
   private Collection<Mezzage> results;
   private Date startDate, endDate;
   private Actor owner;
   private Date creationMoment;
   // Relationships ---------------------------------------------------------

   // Constructors -----------------------------------------------------------
   public FinderMessage() {
      super();
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @NotBlank
   public String getKeyword() {
      return keyword;
   }

   public void setKeyword(String keyword) {
      this.keyword = keyword;
   }

   @OneToMany
   public Collection<Mezzage> getResults() {
      return results;
   }

   public void setResults(Collection<Mezzage> results) {
      this.results = results;
   }

   @Temporal(TemporalType.DATE)
   public Date getStartDate() {
      return startDate;
   }

   public void setStartDate(Date startDate) {
      this.startDate = startDate;
   }
   @Temporal(TemporalType.DATE)
   public Date getEndDate() {
      return endDate;
   }

   public void setEndDate(Date endDate) {
      this.endDate = endDate;
   }

   @NotNull
   @ManyToOne
   public Actor getOwner() {
      return owner;
   }

   public void setOwner(Actor owner) {
      this.owner = owner;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getCreationMoment() {
      return creationMoment;
   }

   public void setCreationMoment(Date creationMoment) {
      this.creationMoment = creationMoment;
   }
}