/*
 * Question.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Question extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Question() {
		super();
	}


	// Attributes -------------------------------------------------------------

   private String statement, answer;
   private Date writeDate;

   private Inmigrant applicant;
   private Officer maker;
   private Application relatedApplication;

   @Temporal(TemporalType.TIMESTAMP)
   public Date getWriteDate() {
      return writeDate;
   }

   public void setWriteDate(Date writeDate) {
      this.writeDate = writeDate;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getAnswer() {
      return answer;
   }

   public void setAnswer(String answer) {
      this.answer = answer;
   }

   @ManyToOne
   public Inmigrant getApplicant() {
      return applicant;
   }

   public void setApplicant(Inmigrant applicant) {
      this.applicant = applicant;
   }

   @ManyToOne
   public Officer getMaker() {
      return maker;
   }

   public void setMaker(Officer maker) {
      this.maker = maker;
   }

   @ManyToOne
   public Application getRelatedApplication() {
      return relatedApplication;
   }

   public void setRelatedApplication(Application relatedApplication) {
      this.relatedApplication = relatedApplication;
   }

   // Relationships ----------------------------------------------------------


}
