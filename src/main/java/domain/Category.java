/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * Created by daviddelatorre on 15/5/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class Category extends DomainEntity {

   private String name;
   private String description;
   private Category father;
   private Collection<Category> sons;
   private Collection<Visa> visas;
   private Boolean hidden;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @NotNull
   @OneToOne
   public Category getFather() {
      return father;
   }

   public void setFather(Category father) {
      this.father = father;
   }

   @OneToMany
   public Collection<Category> getSons() {
      return sons;
   }

   public void setSons(Collection<Category> sons) {
      this.sons = sons;
   }

   @OneToMany
   public Collection<Visa> getVisas() {
      return visas;
   }

   public void setVisas(Collection<Visa> visas) {
      this.visas = visas;
   }

   public Boolean getHidden() {
      return hidden;
   }

   public void setHidden(Boolean hidden) {
      this.hidden = hidden;
   }

   @Override
   public String toString() {
      return name;
   }


}
