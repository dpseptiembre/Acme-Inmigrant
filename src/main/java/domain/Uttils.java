/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Uttils extends DomainEntity {

   private Collection<String> spamWords;
   private int numberOfFinderResults;
   private String welcomeMezzage;
   private String banner;
   private String systemName;
   private String welcomeMezzageES;
   private int finderCarche;

   @ElementCollection
   public Collection<String> getSpamWords() {
      return spamWords;
   }

   public void setSpamWords(Collection<String> spamWords) {
      this.spamWords = spamWords;
   }

   @NotNull
   @Range(min = 1, max = 100)
   public int getNumberOfFinderResults() {
      return numberOfFinderResults;
   }

   public void setNumberOfFinderResults(int numberOfFinderResults) {
      this.numberOfFinderResults = numberOfFinderResults;
   }


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getWelcomeMezzage() {
      return welcomeMezzage;
   }

   public void setWelcomeMezzage(String welcomeMezzage) {
      this.welcomeMezzage = welcomeMezzage;
   }
   @NotBlank
   @URL
   public String getBanner() {
      return banner;
   }

   public void setBanner(String banner) {
      this.banner = banner;
   }
   
   @NotNull
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSystemName() {
      return systemName;
   }

   public void setSystemName(String systemName) {
      this.systemName = systemName;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getWelcomeMezzageES() {
        return welcomeMezzageES;
    }

    public void setWelcomeMezzageES(String welcomeMezzageES) {
        this.welcomeMezzageES = welcomeMezzageES;
    }


    @NotNull
    @Range(min = 1, max = 24)
   public int getFinderCarche() {
      return finderCarche;
   }

   public void setFinderCarche(int finderCarche) {
      this.finderCarche = finderCarche;
   }
}

