package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Pattern;
import java.util.Collection;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class Country extends DomainEntity {

   private String name, isoCode, flag, wikiLink;
   private Collection<Law> legislation;
   private Collection<Visa> offeredVisas;
   private boolean hidden;
   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @Pattern(regexp = "^[A-Z]{2}$")
   public String getIsoCode() {
      return isoCode;
   }

   public void setIsoCode(String isoCode) {
      this.isoCode = isoCode;
   }

   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getFlag() {
      return flag;
   }

   public void setFlag(String flag) {
      this.flag = flag;
   }

   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getWikiLink() {
      return wikiLink;
   }

   public void setWikiLink(String wikiLink) {
      this.wikiLink = wikiLink;
   }

   @OneToMany
   public Collection<Law> getLegislation() {
      return legislation;
   }

   public void setLegislation(Collection<Law> legislation) {
      this.legislation = legislation;
   }

   @OneToMany
   public Collection<Visa> getOfferedVisas() {
      return offeredVisas;
   }

   public void setOfferedVisas(Collection<Visa> offeredVisas) {
      this.offeredVisas = offeredVisas;
   }


   public boolean isHidden() {
      return hidden;
   }

   public void setHidden(boolean hidden) {
      this.hidden = hidden;
   }

   @Override
   public String toString() {
      return name + " [" + isoCode +
              "]";
   }
}
