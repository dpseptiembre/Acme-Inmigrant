/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Visa extends DomainEntity {

   private String clazz, description;
   private Double price;
   private Category category;
   private Collection<Application> applications;
   private Collection<Requirement> requirements;
   private Country country;
   private String currency;
   private boolean abrogated;
   private boolean hidden;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getClazz() {
      return clazz;
   }

   public void setClazz(String clazz) {
      this.clazz = clazz;
   }

   public boolean isHidden() {
      return hidden;
   }

   public void setHidden(boolean hidden) {
      this.hidden = hidden;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @NotNull
   @Range(min = 0)
   public Double getPrice() {
      return price;
   }

   public void setPrice(Double price) {
      this.price = price;
   }

   @OneToOne
   public Category getCategory() {
      return category;
   }

   public void setCategory(Category category) {
      this.category = category;
   }

   @OneToMany
   public Collection<Application> getApplications() {
      return applications;
   }

   public void setApplications(Collection<Application> applications) {
      this.applications = applications;
   }

   @ManyToMany
   public Collection<Requirement> getRequirements() {
      return requirements;
   }

   public void setRequirements(Collection<Requirement> requirements) {
      this.requirements = requirements;
   }

   @NotNull
   @ManyToOne
   public Country getCountry() {
      return country;
   }

   public void setCountry(Country country) {
      this.country = country;
   }

   @NotBlank
   @Pattern(regexp = "^([£\u20ac$])$")
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getCurrency() {
      return currency;
   }

   public void setCurrency(String currency) {
      this.currency = currency;
   }

   @NotNull
   public boolean isAbrogated() {
      return abrogated;
   }

   public void setAbrogated(boolean abrogated) {
      this.abrogated = abrogated;
   }

   @Override
   public String toString() {
      return description + " (" + price + " ) " + "- [ " + category + " ] - " + country;
   }
}

