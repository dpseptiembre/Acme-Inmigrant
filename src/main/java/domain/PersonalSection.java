package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Collection;
import java.util.Date;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class PersonalSection extends DomainEntity {

   private Collection<String> names;
   private String birthPlace, picture;
   private Date birthDate;
   private Application application;

   @NotEmpty
   @ElementCollection
   public Collection<String> getNames() {
      return names;
   }

   public void setNames(Collection<String> names) {
      this.names = names;
   }

   public void addName(String name) {
      this.names.add(name);
   }

   public void removeName(String name) {
      this.names.remove(name);
   }


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getBirthPlace() {
      return birthPlace;
   }

   public void setBirthPlace(String birthPlace) {
      this.birthPlace = birthPlace;
   }

   @URL
   @NotNull
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getPicture() {
      return picture;
   }

   public void setPicture(String picture) {
      this.picture = picture;
   }

   @Temporal(TemporalType.DATE)
   @Past
   public Date getBirthDate() {
      return birthDate;
   }

   public void setBirthDate(Date birthDate) {
      this.birthDate = birthDate;
   }

   @OneToOne
   public Application getApplication() {
      return application;
   }

   public void setApplication(Application application) {
      this.application = application;
   }
}
