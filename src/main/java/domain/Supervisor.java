/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Supervisor extends Actor {

	// Constructors -----------------------------------------------------------

   private Collection<Evaluation> evaluations;
   private Collection<Officer> officersToSupervist;


   public Supervisor() {
		super();
	}
	// Attributes -------------------------------------------------------------

	// Relationships ----------------------------------------------------------

   @OneToMany(mappedBy = "owner")
   public Collection<Evaluation> getEvaluations() {
      return evaluations;
   }

   public void setEvaluations(Collection<Evaluation> evaluations) {
      this.evaluations = evaluations;
   }

   @OneToMany(mappedBy = "supervisor")
   public Collection<Officer> getOfficersToSupervist() {
      return officersToSupervist;
   }

   public void setOfficersToSupervist(Collection<Officer> officersToSupervist) {
      this.officersToSupervist = officersToSupervist;
   }

   @Override
   public String toString() {
      return super.getName()+ " " + super.getSurname();
   }
}
