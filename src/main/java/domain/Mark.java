/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;


public enum Mark {
   F, C, B, A, A_PLUS,
}
