/*
 * Evaluation.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Evaluation extends DomainEntity {


   // Attributes -------------------------------------------------------------
   private Mark mark;
   private String description;
   private Date postDate;
   private Supervisor owner;
   private Officer receiver;

   public Evaluation() {
      super();

   }

   public Mark getMark() {
      return mark;
   }

   public void setMark(Mark mark) {
      this.mark = mark;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @NotNull
   @ManyToOne
   public Supervisor getOwner() {
      return owner;
   }

   public void setOwner(Supervisor owner) {
      this.owner = owner;
   }

   @NotNull
   @ManyToOne
   public Officer getReceiver() {
      return receiver;
   }

   public void setReceiver(Officer receiver) {
      this.receiver = receiver;
   }

   @NotNull
   @Temporal(TemporalType.DATE)
   public Date getPostDate() {
      return postDate;
   }

   public void setPostDate(Date postDate) {
      this.postDate = postDate;
   }

   // Relationships ----------------------------------------------------------


}
