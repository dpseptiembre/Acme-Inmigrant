/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Investigator extends Actor {

	// Constructors -----------------------------------------------------------

   private Collection<Inmigrant> underInvestigation;


   public Investigator() {
		super();
	}

	// Attributes -------------------------------------------------------------

	private Collection<Report> reports;

   @OneToMany(mappedBy = "owner")
	public Collection<Report> getReports() {
		return reports;
	}

	public void setReports(Collection<Report> reports) {
		this.reports = reports;
	}

	// Relationships ----------------------------------------------------------

   @OneToMany
   public Collection<Inmigrant> getUnderInvestigation() {
      return underInvestigation;
   }

   public void setUnderInvestigation(Collection<Inmigrant> underInvestigation) {
      this.underInvestigation = underInvestigation;
   }
}
