package domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class ContactSection extends DomainEntity {


   private String email, phoneNumber, pageNumber;

   private Application application;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @Email
   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getPhoneNumber() {
      return phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getPageNumber() {
      return pageNumber;
   }

   public void setPageNumber(String pageNumber) {
      this.pageNumber = pageNumber;
   }

   @ManyToOne
   public Application getApplication() {
      return application;
   }

   public void setApplication(Application application) {
      this.application = application;
   }
}
