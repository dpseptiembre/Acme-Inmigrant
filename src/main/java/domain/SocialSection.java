package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class SocialSection extends DomainEntity {


   private String nickname, socialNetwork, link;
   private Application application;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getNickname() {
      return nickname;
   }

   public void setNickname(String nickname) {
      this.nickname = nickname;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSocialNetwork() {
      return socialNetwork;
   }

   public void setSocialNetwork(String socialNetwork) {
      this.socialNetwork = socialNetwork;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @URL
   public String getLink() {
      return link;
   }

   public void setLink(String link) {
      this.link = link;
   }


   @ManyToOne
   public Application getApplication() {
      return application;
   }

   public void setApplication(Application application) {
      this.application = application;
   }

}
