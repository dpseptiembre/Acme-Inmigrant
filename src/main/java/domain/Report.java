package domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class Report extends DomainEntity {

   private String text;
   private Collection<String> pictures;
   private Investigator owner;
   private Inmigrant attendant;


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }


   @NotEmpty
   @ElementCollection
   public Collection<String> getPictures() {
      return pictures;
   }

   public void setPictures(Collection<String> pictures) {
      this.pictures = pictures;
   }


   public void addPicture(String picture) {
      this.pictures.add(picture);
   }

   public void removePicture(String picture) {
      this.pictures.remove(picture);
   }

   @ManyToOne
   public Investigator getOwner() {
      return owner;
   }

   public void setOwner(Investigator owner) {
      this.owner = owner;
   }

   @ManyToOne
   public Inmigrant getAttendant() {
      return attendant;
   }

   public void setAttendant(Inmigrant attendant) {
      this.attendant = attendant;
   }
}
