/*
 * Actor.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import security.UserAccount;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Actor extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Actor() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String	name;
   private String surname;
	private String	email;
	private String	phone;
	private String	address;
   private Collection<Folder> folders;
   private Collection<Finder> finders;
   private Collection<FinderMessage> finderMessages;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSurname() {
      return surname;
   }

   public void setSurname(String surname) {
      this.surname = surname;
   }

   @Email
   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
//   @Pattern(regexp = "(?:(\\+?\\d{1,3}) )?(?:([/(]?\\d+[/)]?)[ -])?(\\d{1,5}[\\- ]?\\d{1,5})")
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}


	// Relationships ----------------------------------------------------------

	private UserAccount	userAccount;


	@NotNull
	@Valid
	@OneToOne
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

   @OneToMany(mappedBy = "owner")
   public Collection<Folder> getFolders() {
      return folders;
   }

   public void setFolders(Collection<Folder> folders) {
      this.folders = folders;
   }

	@OneToMany
	public Collection<Finder> getFinders() {
		return finders;
	}

	public void setFinders(Collection<Finder> finders) {
		this.finders = finders;
	}
	@OneToMany(mappedBy = "owner")
	public Collection<FinderMessage> getFinderMessages() {
		return finderMessages;
	}

	public void setFinderMessages(Collection<FinderMessage> finderMessages) {
		this.finderMessages = finderMessages;
	}
}
