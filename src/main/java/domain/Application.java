package domain;

import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Date;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Entity
@Access(AccessType.PROPERTY)
public class Application extends DomainEntity {

   private Inmigrant applicant;
   private Officer officer;
   private String ticker;
   private Date openDate, closeDate, statusChange;
   private Status status;
   private String statusComment;
   private Visa visa;
   private PersonalSection personalSection;
   private Collection<ContactSection> contactSections;
   private Collection<SocialSection> socialSections;
   private Collection<EducationSection> educationSections;
   private Collection<WorkSection> workSections;
   private Boolean isClosed;
   private Collection<Application> linkedApplications;
   private Long elapsedTime;
   private Boolean assigned;

   @GeneratedValue()
   @Pattern(regexp = "^[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([^a-zA-Z]*[A-Za-z]){3}[\\s\\S][0-9]{2}")
   public String getTicker() {
      return ticker;
   }

   public void setTicker(String ticker) {
      this.ticker = ticker;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getOpenDate() {
      return openDate;
   }


   public void setOpenDate(Date openDate) {
      this.openDate = openDate;
   }


   @Temporal(TemporalType.TIMESTAMP)
   public Date getCloseDate() {
      return closeDate;
   }

   public void setCloseDate(Date closeDate) {
      this.closeDate = closeDate;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getStatusChange() {
      return statusChange;
   }

   public void setStatusChange(Date statusChange) {
      this.statusChange = statusChange;
   }

   public Long getElapsedTime() {
    return elapsedTime;

   }
   public void setElapsedTime(Long elapsedTime) {
         this.elapsedTime = elapsedTime;
   }

   public Status getStatus() {
      return status;
   }

   public void setStatus(Status status) {
      this.status = status;
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getStatusComment() {
      return statusComment;
   }

   public void setStatusComment(String statusComment) {
      this.statusComment = statusComment;
   }

   @ManyToOne
   public Inmigrant getApplicant() {
      return applicant;
   }

   public void setApplicant(Inmigrant applicant) {
      this.applicant = applicant;
   }

   @ManyToOne
   public Officer getOfficer() {
      return officer;
   }

   public void setOfficer(Officer officer) {
      this.officer = officer;
   }

   @OneToOne
   public Visa getVisa() {
      return visa;
   }

   public void setVisa(Visa visa) {
      this.visa = visa;
   }

   @OneToOne
   public PersonalSection getPersonalSection() {
      return personalSection;
   }

   public void setPersonalSection(PersonalSection personalSection) {
      this.personalSection = personalSection;
   }

   @OneToMany(mappedBy = "application")
   public Collection<ContactSection> getContactSections() {
      return contactSections;
   }

   public void setContactSections(Collection<ContactSection> contactSections) {
      this.contactSections = contactSections;
   }

   @OneToMany(mappedBy = "application")
   public Collection<SocialSection> getSocialSections() {
      return socialSections;
   }

   public void setSocialSections(Collection<SocialSection> socialSections) {
      this.socialSections = socialSections;
   }

   @OneToMany(mappedBy = "application")
   public Collection<EducationSection> getEducationSections() {
      return educationSections;
   }

   public void setEducationSections(Collection<EducationSection> educationSections) {
      this.educationSections = educationSections;
   }

   @OneToMany(mappedBy = "application")
   public Collection<WorkSection> getWorkSections() {
      return workSections;
   }

   public void setWorkSections(Collection<WorkSection> workSections) {
      this.workSections = workSections;
   }

   @ManyToMany
   public Collection<Application> getLinkedApplications() {
      return linkedApplications;
   }

   public void setLinkedApplications(Collection<Application> linkedApplications) {
      this.linkedApplications = linkedApplications;
   }

   public Boolean getClosed() {
      return isClosed;
   }

   public void setClosed(Boolean closed) {
      isClosed = closed;
   }
   @Override
   public String toString() {
      return ticker + " [ " + status + " ] ";
   }

   public Boolean getAssigned() {
      return assigned;
   }

   public void setAssigned(Boolean assigned) {
      this.assigned = assigned;
   }
}
