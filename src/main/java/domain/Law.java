/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Law extends DomainEntity {

   private String title, text;
   private Date enatctment, abrogation;
   private Collection<Requirement> requirements;
   private Collection<Law> relatedLaws;
   private Country country;
   private Boolean hidden;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }

   @DateTimeFormat(pattern = "dd/MM/yyyy")
   @Temporal(TemporalType.DATE)
   @NotNull
   public Date getEnatctment() {
      return enatctment;
   }

   public void setEnatctment(Date enatctment) {
      this.enatctment = enatctment;
   }

   @DateTimeFormat(pattern = "dd/MM/yyyy")
   @Temporal(TemporalType.DATE)
   public Date getAbrogation() {
      return abrogation;
   }

   public void setAbrogation(Date abrogation) {
      this.abrogation = abrogation;
   }

   @OneToMany
   public Collection<Requirement> getRequirements() {
      return requirements;
   }

   public void setRequirements(Collection<Requirement> requirements) {
      this.requirements = requirements;
   }

   @OneToMany
   public Collection<Law> getRelatedLaws() {
      return relatedLaws;
   }

   public void setRelatedLaws(Collection<Law> relatedLaws) {
      this.relatedLaws = relatedLaws;
   }

   @NotNull
   @ManyToOne
   public Country getCountry() {
      return country;
   }

   public void setCountry(Country country) {
      this.country = country;
   }


   public Boolean getHidden() {
      return hidden;
   }

   public void setHidden(Boolean hidden) {
      this.hidden = hidden;
   }

   @Override
   public String toString() {
      return title + " [" + enatctment + " - " + abrogation + "] " + "[ " + country + " ]";
   }
}
