/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Folder extends DomainEntity {

   private String name;
   private Collection<Mezzage> mezzages;
   private Actor owner;
   private FolderType folderType;


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @OneToMany(mappedBy = "folder")
   public Collection<Mezzage> getMezzages() {
      return mezzages;
   }

   public void setMezzages(Collection<Mezzage> mezzages) {
      this.mezzages = mezzages;
   }


   @ManyToOne
   public Actor getOwner() {
      return owner;
   }

   public void setOwner(Actor owner) {
      this.owner = owner;
   }


   public FolderType getFolderType() {
      return folderType;
   }

   public void setFolderType(FolderType folderType) {
      this.folderType = folderType;
   }

   @Override
   public String toString() {
      return "Folder{" +
              "name='" + name + '\'' +
              '}';
   }
}
