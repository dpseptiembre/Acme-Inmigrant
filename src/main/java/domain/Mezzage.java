/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Mezzage extends DomainEntity {


   private String senderEmail;
   private String receiverEmail;
   private Actor sender;
   private Actor receiver;
   private Date sendDate;
   private String subject;
   private String body;
   private Collection<String> attachments;
   private Folder folder;
   private boolean spam;
   private boolean hidden;



   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSenderEmail() {
      return senderEmail;
   }

   public void setSenderEmail(String senderEmail) {
      this.senderEmail = senderEmail;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getReceiverEmail() {
      return receiverEmail;
   }

   public void setReceiverEmail(String receiverEmail) {
      this.receiverEmail = receiverEmail;
   }

   @ManyToOne(targetEntity = Actor.class)
   public Actor getSender() {
      return sender;
   }

   public void setSender(Actor sender) {
      this.sender = sender;
   }

   @ManyToOne(targetEntity = Actor.class)
   public Actor getReceiver() {
      return receiver;
   }

   public void setReceiver(Actor receiver) {
      this.receiver = receiver;
   }

   @Past
   @Temporal(TemporalType.TIMESTAMP)
   public Date getSendDate() {
      return sendDate;
   }

   public void setSendDate(Date sendDate) {
      this.sendDate = sendDate;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSubject() {
      return subject;
   }

   public void setSubject(String subject) {
      this.subject = subject;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getBody() {
      return body;
   }

   public void setBody(String body) {
      this.body = body;
   }


   @ManyToOne
   public Folder getFolder() {
      return folder;
   }

   public void setFolder(Folder folder) {
      this.folder = folder;
   }


   @ElementCollection
   public Collection<String> getAttachments() {
      return attachments;
   }

   public void setAttachments(Collection<String> attachments) {
      this.attachments = attachments;
   }

   public void addAttachment(String attachment) {
      this.attachments.add(attachment);
   }

   public void removeAttachment(String attachment) {
      this.attachments.remove(attachment);
   }


   public boolean isSpam() {
      return spam;
   }

   public void setSpam(boolean spam) {
      this.spam = spam;
   }

   public boolean isHidden() {
      return hidden;
   }

   public void setHidden(boolean hidden) {
      this.hidden = hidden;
   }

   @Override
   public String toString() {
      return "Mezzage{" +
              "senderEmail='" + senderEmail + '\'' +
              ", receiverEmail='" + receiverEmail + '\'' +
              ", sendDate=" + sendDate +
              ", subject='" + subject + '\'' +
              ", body='" + body + '\'' +
              '}';
   }
}
