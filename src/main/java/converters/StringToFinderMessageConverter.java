/*
 * StringToQuestionConverter.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package converters;

import domain.Finder;
import domain.FinderMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.FinderMessageRepository;
import repositories.FinderRepository;

@Component
@Transactional
public class StringToFinderMessageConverter implements Converter<String, FinderMessage> {

	@Autowired
	FinderMessageRepository questionRepository;


	@Override
	public FinderMessage convert(final String text) {
		FinderMessage result;
		int id;

		try {
			id = Integer.valueOf(text);
			result = this.questionRepository.findOne(id);
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}

}
