/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import security.Authority;

/**
 * Created by daviddelatorre on 29/3/17.
 */
@Component
@Transactional
public class StringToAuthoriyConverter implements Converter<String, Authority> {

   @Override
   public Authority convert(String text){
      Authority result;

      try{
         if(StringUtils.isEmpty(text)){
            result = null;
         }else{
            result = new Authority();
            result.setAuthority(text);
         }
      }catch (Throwable oops){
         throw new IllegalArgumentException(oops);
      }

      return result;
   }


}


