/*
 * StringToQuestionConverter.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package converters;

import domain.EducationSection;
import domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.EducationSectionRepository;
import repositories.QuestionRepository;

@Component
@Transactional
public class StringToEducationSectionConverter implements Converter<String, EducationSection> {

	@Autowired
	EducationSectionRepository questionRepository;


	@Override
	public EducationSection convert(final String text) {
		EducationSection result;
		int id;

		try {
			id = Integer.valueOf(text);
			result = this.questionRepository.findOne(id);
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}

}
