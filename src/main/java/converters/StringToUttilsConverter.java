/*
 * StringToActorConverter.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package converters;

import domain.Uttils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.UttilsRepository;

@Component
@Transactional
public class StringToUttilsConverter implements Converter<String, Uttils> {

	@Autowired
	UttilsRepository actorRepository;


	@Override
	public Uttils convert(final String text) {
		Uttils result;
		int id;

		try {
			id = Integer.valueOf(text);
			result = this.actorRepository.findOne(id);
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}

}
