/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Investigator;
import domain.Officer;
import domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("/report")
public class ReportController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private ReportService reportService;

   @Autowired
   private InvestigatorService investigatorService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private InmigrantController inmigrantController;
   @Autowired
   private OfficerService officerService;
   @Autowired
   private ApplicationService applicationService;

   //Constructors----------------------------------------------

   public ReportController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Report report) {
      ModelAndView result;

      result = createEditModelAndView(report, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Report report, String message) {
      ModelAndView result;

      result = new ModelAndView("report/edit");
      result.addObject("report", report);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView reportsList() {

      ModelAndView result;
      Collection<Report> report;

      report = reportService.findAll();
      result = new ModelAndView("report/list");
      result.addObject("reports", report);
      result.addObject("requestURI", "report/list.do");

      return result;
   }


   @RequestMapping(value = "/myReports", method = RequestMethod.GET)
   public ModelAndView myReports() {

      ModelAndView result;
      try {
         Collection<Report> report;
         Investigator investigator = investigatorService.findByPrincipal();

         report = investigator.getReports();
         result = new ModelAndView("report/list");
         result.addObject("reports", report);
         result.addObject("requestURI", "report/myReports.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/doneReports", method = RequestMethod.GET)
   public ModelAndView doneReports() {

      ModelAndView result;
      try {
         Officer officer = officerService.findByPrincipal();
         result = new ModelAndView("report/list");
         Set<Report> reports = new HashSet<>(officerService.reportOfApplicationThatIOffice(officer.getId()));
         result.addObject("reports", reports);
         result.addObject("requestURI", "report/doneReports.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int inmigrantId) {

      ModelAndView result;

      Report report = reportService.create();
      report.setOwner(investigatorService.findByPrincipal());
      report.setAttendant(inmigrantService.findOne(inmigrantId));
      result = createEditModelAndView(report);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int reportId) {
      ModelAndView result;
      Report report;

      report = reportService.findOne(reportId);
      Assert.notNull(report);
      Assert.isTrue(investigatorService.findByPrincipal().getReports().contains(report));
      result = createEditModelAndView(report);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Report report, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(report);
      } else {
         try {
           Report report1=  reportService.save(report);
            investigatorService.findByPrincipal().getReports().add(report1);
            report1.getAttendant().getReports().add(report1);
            reportService.save(report1);
            inmigrantService.save(report1.getAttendant());

            result = new ModelAndView("redirect:/inmigrant/listUnderInvestigation.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(report, "report.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int reportId) {
      ModelAndView result;
      try {
         Report report = reportService.findOne(reportId);
         Assert.isTrue(investigatorService.findByPrincipal().getReports().contains(report));
         reportService.delete(report);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         Report report = reportService.findOne(reportId);
         result = createEditModelAndView(report, "report.commit.error");
      }

      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int reportId) {

      ModelAndView result;

      try {
         Report report = reportService.findOne(reportId);
         result = new ModelAndView("report/view");
         result.addObject("report", report);
         result.addObject("attachments", report.getPictures());
         result.addObject("requestURI", "report/list.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }



   @RequestMapping(value = "/createAttachment", method = RequestMethod.GET)
   public ModelAndView createAttachment(@RequestParam int reportId) {

      ModelAndView result;
      try {
         result = new ModelAndView("report/createAttachment");
         result.addObject("id",reportId);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/saveAttachment", method = RequestMethod.GET)
   public ModelAndView saveAttachment(@RequestParam String reportId, @RequestParam String attachment) {

      ModelAndView result;
      try {
         reportService.createAttachment(reportId,attachment);
         result = new ModelAndView("officer/success");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/deleteAttachment", method = RequestMethod.GET)
   public ModelAndView deleteAttachment(@RequestParam String reportId, @RequestParam String attachment) {

      ModelAndView result;
      try {
         reportService.removeAttachment(reportId,attachment);
         result = new ModelAndView("officer/success");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }
}
