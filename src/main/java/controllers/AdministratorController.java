/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Administrator;
import domain.Mezzage;
import domain.Uttils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.AdministratorService;
import services.UttilsService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

    //Services ----------------------------------------------------------------


    @Autowired
    private AdministratorService administratorService;
    @Autowired
    private UttilsService uttilsService;


    //Constructors----------------------------------------------

    public AdministratorController() {
        super();
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView administratorList() {

        ModelAndView result;
        Collection<Administrator> administrators;

        administrators = administratorService.findAll();
        result = new ModelAndView("administrator/list");
        result.addObject("administrators", administrators);
        result.addObject("requestURI", "administrator/list.do");

        return result;
    }



    //Create Method -----------------------------------------------------------

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int administratorId) {
        ModelAndView result;
        Administrator administrator;

        administrator = administratorService.findOne(administratorId);
        Assert.notNull(administrator);
        result = createEditModelAndView(administrator);

        return result;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Administrator administrator, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(administrator);
        } else {
            try {
                administratorService.save(administrator);
                result = new ModelAndView("redirect:list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(administrator, "administrator.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
    public ModelAndView delete(Administrator administrator) {
        ModelAndView result;
        try {
            administratorService.delete(administrator);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {
            result = createEditModelAndView(administrator, "administrator.commit.error");
        }

        return result;
    }

    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Administrator administrator) {
        ModelAndView result;

        result = createEditModelAndView(administrator, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Administrator administrator, String message) {
        ModelAndView result;

        result = new ModelAndView("administrator/edit");
        result.addObject("administrator", administrator);
        result.addObject("message", message);

        return result;

    }

    @RequestMapping(value = "/utilsView", method = RequestMethod.GET)
    public ModelAndView utilsView(){

        ModelAndView result;
        ArrayList<Uttils> uttils= new ArrayList<>(uttilsService.findAll());
        Uttils uttils1 = uttils.get(0);

        Collection<String> strings = uttils1.getSpamWords();
        result = new ModelAndView("administrator/utils");
        result.addObject("requestURI", "administrator/list.do");
        result.addObject("strings",strings);
        result.addObject("searchN", administratorService.getUttils().getNumberOfFinderResults());
        result.addObject("welcome", administratorService.getUttils().getWelcomeMezzage());
       result.addObject("welcomeES", administratorService.getUttils().getWelcomeMezzageES());
       result.addObject("systemName", administratorService.getUttils().getSystemName());
        result.addObject("banner", administratorService.getUttils().getBanner());
        result.addObject("cache", administratorService.getUttils().getFinderCarche());

        return result;
    }


    @RequestMapping(value = "/addSpamWord", method = RequestMethod.GET)
    public ModelAndView addSpamWord(@RequestParam String word){

        ModelAndView result;

        try {
            administratorService.addSpamWord(word);
            result = new ModelAndView("officer/success");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }

    @RequestMapping(value = "/deleteSpam", method = RequestMethod.GET)
    public ModelAndView deleteSpam(@RequestParam String word){

        ModelAndView result;

        try {
            administratorService.deleteSpamWord(word);
            result = new ModelAndView("officer/success");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }

    @RequestMapping(value = "/resultNumber", method = RequestMethod.GET)
    public ModelAndView resultNumber(@RequestParam String num){

        ModelAndView result;
        try {
            administratorService.changeSearchNumber(num);
            result = new ModelAndView("officer/success");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }


    @RequestMapping(value = "/setFinder", method = RequestMethod.GET)
    public ModelAndView setFinder(@RequestParam String cache){

        ModelAndView result;
        try {
            administratorService.setFinder(cache);
            result = new ModelAndView("officer/success");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }

    @RequestMapping(value = "/customize", method = RequestMethod.GET)
    public ModelAndView customize(@RequestParam String systemName, @RequestParam String banner, @RequestParam String welcome, @RequestParam String welcomeES){

        ModelAndView result;
        try {
            administratorService.customize(systemName,banner,welcome,welcomeES);
            result = new ModelAndView("officer/success");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }


    @RequestMapping(value = "/createSpam", method = RequestMethod.GET)
    public ModelAndView createSpam(){

        ModelAndView result;

        try {
            result = new ModelAndView("administrator/spam");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }


    @RequestMapping(value = "/spamMessages", method = RequestMethod.GET)
    public ModelAndView mezzagesListPublic() {
        ModelAndView result;
        try {

            Collection<Mezzage> mezzage;
            mezzage = administratorService.spamMezzages();
            result = new ModelAndView("mezzage/list");
            result.addObject("mezzages", mezzage);
            result.addObject("requestURI", "mezzage/list.do");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }
    }


    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashboard() {

        ModelAndView result;

        try {
            Double q1 =  administratorService.averageNumberOfApplicationsPerUser();
            int q2 = administratorService.maxNumberOfApplicationPerUser();
            int q3 = administratorService.minNumberOfApplicationPerUser();
            Double q4 = administratorService.standarDesviationOfApplicationPerUser();

            Double q5 = administratorService.averageNumberOfApplicationsPerOfficer();
            int q6 = administratorService.maxNumberOfApplicationsPerOfficer();
            int q7 =administratorService.minNumberOfApplicationPerOfficer();
            Double q8 = administratorService.standarDesviationOfApplicationPerOfficer();

            Double q10 = administratorService.averageNumberofVisaPrices();
            int q11 = administratorService.maxNumberOfVisaPrices();
            int q12 = administratorService.minNumberOfVisaPrices();
            Double q13 = administratorService.standarDesviationOfVisaPrices();

            Double q14 = administratorService.averageNumberOfInmigrantIvestigatedPerInvestigator();
            int q15 = administratorService.maxNumberOfInmigrantIvestigatedPerInvestigator();
            int q16 = administratorService.minNumberOfInmigrantIvestigatedPerInvestigator();
            Double q17 = administratorService.standarDesviationOfNumberOfInmigrantIvestigatedPerInvestigator();


            Double q18 = administratorService.averageNumberOfVisaPerCategory();
            int q20 = administratorService.maxNumberOfVisaPerCategory();
            int q21 = administratorService.minNumberOfVisaPerCategory();
            Double q22 = administratorService.standarDesviationNumberOfVisaPerCategory();

            Double q23 = administratorService.averageNumberOfLawsPerCountry();
            int q24 = administratorService.maxNumberOfLawsPerCountry();
            int q25 = administratorService.minNumberOfLawsPerCountry();
            Double q26 = administratorService.standarDesviationOfLawsPerCountry();

            Double q27 = administratorService.averageNumberOfRequirementsPerVisa();
            int q28 = administratorService.maxNumberOfRequirementsPerVisa();
            int q29 = administratorService.minNumberOfRequirementsPerVisa();
            Double q30 = administratorService.standarDesviationOfRequirementsPerVisa();

            Double q31 = administratorService.averageNumberOfMessagesPerFolder();
            int q32 = administratorService.maxNumberOfMessagesPerFolder();
            int q33 = administratorService.minNumberOfMessagesPerFolder();
            Double q34 = administratorService.standarDesviationOfMesasgesPerFolder();

            Double q35 = administratorService.averageNumberOfMessagesPerActor();
            int q36 =  administratorService.maxNumberOfMessagesPerActor();
            int q37 = administratorService.minNumberOfMessagesPerActor();
            Double q38 = administratorService.standarDesviationOfMesasgesPerActor();


            Double q39 = administratorService.ratioOfSpamMezzages();


            //Chart

            List<Long> elapsedTime =  administratorService.elapsedTimes();

            Double averageTime = administratorService.averageNumberOfElapsedTimes();
            int maxTime =  administratorService.maxNumberOfElapsedTimes();
            int minTime = administratorService.minNumberOfElapsedTimes();
            Double standarTime = administratorService.standarDesviationNumberOfElapsedTime();
           String dataobject = administratorService.createChart();

            result = new ModelAndView("administrator/dashboard");


            result.addObject("q1", q1);
            result.addObject("q2", q2);
            result.addObject("q3", q3);
            result.addObject("q4", q4);
            result.addObject("q5", q5);
            result.addObject("q6", q6);
            result.addObject("q7", q7);
            result.addObject("q8", q8);
            result.addObject("q10", q10);
            result.addObject("q11", q11);
            result.addObject("q12", q12);
            result.addObject("q13", q13);
            result.addObject("q14", q14);
            result.addObject("q15", q15);
            result.addObject("q16", q16);
            result.addObject("q17", q16);
            result.addObject("q18", q18);
            result.addObject("q20", q20);
            result.addObject("q21", q21);
            result.addObject("q22", q22);
            result.addObject("q23", q23);
            result.addObject("q24", q24);
            result.addObject("q25", q25);
            result.addObject("q26", q26);
            result.addObject("q27", q27);

            result.addObject("q28", q28);
            result.addObject("q29", q29);



            result.addObject("q30", q30);
            result.addObject("q31", q31);
            result.addObject("q32", q32);
            result.addObject("q33", q33);
            result.addObject("q34", q34);
            result.addObject("q35", q35);
            result.addObject("q36", q36);
            result.addObject("q37", q37);
            result.addObject("q38", q38);
            result.addObject("q39", q39);
           result.addObject("min00", administratorService.minNumberOfElapsedTimes());
           result.addObject("max00", administratorService.maxNumberOfElapsedTimes());
           result.addObject("avg00", administratorService.averageNumberOfElapsedTimes());
           result.addObject("std00", administratorService.standarDesviationNumberOfElapsedTime());
           result.addObject("dataP", dataobject);

        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }


        return result;

    }



}
