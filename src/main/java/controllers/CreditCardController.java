/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.CreditCard;
import domain.Inmigrant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CreditCardService;
import services.InmigrantService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("creditCard")
public class CreditCardController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private CreditCardService creditCardService;
@Autowired
private InmigrantService inmigrantService;

   //Constructors----------------------------------------------

   public CreditCardController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView creditCardList() {

      ModelAndView result;
      Collection<CreditCard> creditCard;
      creditCard = creditCardService.findAll();
      result = new ModelAndView("creditCard/list");
      result.addObject("creditCard", creditCard);
      result.addObject("requestURI", "creditCard/list.do");

      return result;
   }


   //Create Method -----------------------------------------------------------

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      CreditCard creditCard = creditCardService.create();
      result = createEditModelAndView(creditCard);

      return result;

   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit() {
      ModelAndView result;
      CreditCard creditCard;
      Inmigrant inmigrant = inmigrantService.findByPrincipal();
      creditCard = inmigrant.getCreditCard();
      Assert.notNull(creditCard);
      result = createEditModelAndView(creditCard);
      result.addObject("creditCard", creditCard);


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid CreditCard creditCard, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(creditCard);
      } else {
         try {

            creditCardService.save(creditCard);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(creditCard, "property.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int creditCardId) {
      ModelAndView result;
      CreditCard p = creditCardService.findOne(creditCardId);

      try {
         creditCardService.delete(p);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(p, "creditCard.commit.error");
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int creditCardId) {
      ModelAndView res;
      CreditCard p = creditCardService.findOne(creditCardId);
      res = new ModelAndView("creditCard/view");
      res.addObject("creditCard", p);

      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(CreditCard creditCard) {
      ModelAndView result;

      result = createEditModelAndView(creditCard, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(CreditCard creditCard, String message) {
      ModelAndView result;

      result = new ModelAndView("creditCard/edit");
      result.addObject("creditCard", creditCard);
      result.addObject("message", message);

      return result;

   }


}








