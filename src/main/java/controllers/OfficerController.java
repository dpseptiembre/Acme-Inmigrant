/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Evaluation;
import domain.Inmigrant;
import domain.Officer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.OfficerService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;

@Controller
@RequestMapping("/officer")
public class OfficerController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private OfficerService officerService;


   //Constructors----------------------------------------------

   public OfficerController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Officer officer) {
      ModelAndView result;

      result = createEditModelAndView(officer, null);

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView(Officer officer, String message) {
      ModelAndView result;

      result = new ModelAndView("officer/edit");
      result.addObject("officer", officer);
      result.addObject("message", message);

      return result;

   }

   protected static ModelAndView createEditModelAndView2(Officer officer) {
      ModelAndView result;

      result = createEditModelAndView2(officer, null);

      return result;
   }
   // Edition ---------------------------------------------------------

   protected static ModelAndView createEditModelAndView2(Officer officer, String message) {
      ModelAndView result;

      result = new ModelAndView("officer/register");
      result.addObject("officer", officer);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView officerList() {

      ModelAndView result;
      Collection<Officer> officers;

      Collection<Officer> res = new HashSet<>();
      officers = officerService.findAll();


      result = new ModelAndView("officer/list");
      result.addObject("officers", res);
      result.addObject("requestURI", "officer/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Officer officer= officerService.create();

      result = createEditModelAndView2(officer);

      return result;

   }

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Officer officer, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView2(officer);
      } else {
         try {
            officerService.registerAsOfficer(officer);
            result = new ModelAndView("investigator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView2(officer, "general.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit() {
      ModelAndView result;
      Officer officer;
      officer = officerService.findByPrincipal();
      Assert.notNull(officer);
      result = createEditModelAndView(officer);

      return result;
   }
   

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Officer officer, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(officer);
      } else {
         try {
            officerService.save(officer);
            result = new ModelAndView("officer/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(officer, "officer.commit.error");
         }
      }
      return result;
   }
   
   @RequestMapping(value = "/viewEvaluations", method = RequestMethod.GET)
   public ModelAndView viewEvaluations() {

      ModelAndView result;

      try {
         Collection<Evaluation> evaluations;
         evaluations = officerService.findByPrincipal().getEvaluations();
         result = new ModelAndView("evaluation/list");
         result.addObject("evaluations", evaluations);
         result.addObject("requestURI", "officer/viewEvaluations.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());

      }

      return result;
   }


}
