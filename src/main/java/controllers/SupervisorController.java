/*
 * Copyright © 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Officer;
import domain.Supervisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.MezzageService;
import services.OfficerService;
import services.SupervisorService;

import java.util.*;

@Controller
@RequestMapping("/supervisor")
public class SupervisorController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private SupervisorService supervisorService;
   @Autowired
   private MezzageService mezzageService;


   //Constructors----------------------------------------------

   public SupervisorController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Supervisor supervisor) {
      ModelAndView result;

      result = createEditModelAndView(supervisor, null);

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView(Supervisor supervisor, String message) {
      ModelAndView result;

      result = new ModelAndView("supervisor/edit");
      result.addObject("supervisor", supervisor);
      result.addObject("message", message);

      return result;

   }

   protected static ModelAndView createEditModelAndView2(Supervisor supervisor) {
      ModelAndView result;

      result = createEditModelAndView2(supervisor, null);

      return result;
   }
   // Edition ---------------------------------------------------------

   protected static ModelAndView createEditModelAndView2(Supervisor supervisor, String message) {
      ModelAndView result;

      result = new ModelAndView("supervisor/register");
      result.addObject("supervisor", supervisor);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView supervisorList() {

      ModelAndView result;
      Collection<Supervisor> supervisors;

      Collection<Supervisor> res = new HashSet<>();
      supervisors = supervisorService.findAll();


      result = new ModelAndView("supervisor/list");
      result.addObject("supervisors", res);
      result.addObject("requestURI", "supervisor/list.do");

      return result;
   }

   @RequestMapping(value = "/listOfficer", method = RequestMethod.GET)
   public ModelAndView listOfficer() {

       ModelAndView result;

       try {
           List<Set<Officer>> officersPerMonth = supervisorService.officersPerMonthBeforRevisions();
           result = new ModelAndView("officer/listMonth");
           result.addObject("officers0", officersPerMonth.get(0));
           result.addObject("officers1", officersPerMonth.get(2));
           result.addObject("officers2", officersPerMonth.get(3));
           result.addObject("officers3", officersPerMonth.get(1));
           return result;
       } catch (Exception e) {
           result = new ModelAndView("administrator/error");
           result.addObject("trace", e.getMessage());
           return result;
       }

   }

    @RequestMapping(value = "/listMyOfficers", method = RequestMethod.GET)
    public ModelAndView listMyOfficers() {
        ModelAndView result;
        try {
            Collection<Officer> myOfficer = supervisorService.myOfficers();
            result = new ModelAndView("officer/list");
            result.addObject("isMy",true);
            result.addObject("officers", myOfficer);
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }

    }

    @RequestMapping(value = "/supervise", method = RequestMethod.GET)
    public ModelAndView supervise(@RequestParam int officerId, Locale locale){

        ModelAndView result;

        try {
            supervisorService.superviseOfficer(officerId);
            if("es".equals(locale.getLanguage())){
                mezzageService.createActionMessage("Estas siendo evaluado","La evaluación ha empezado", officerId);
            }else{
                mezzageService.createActionMessage("You are under evaluation","Investigation starts",officerId);
            }
            result = new ModelAndView("officer/success");
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            return result;
        }


    }



}
