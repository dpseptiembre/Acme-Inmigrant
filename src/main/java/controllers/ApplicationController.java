/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/application")
public class ApplicationController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private QuestionService questionService;
   @Autowired
   private OfficerService officerService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private MezzageService mezzageService;
@Autowired
private InvestigatorService investigatorService;
   @Autowired
private VisaService visaSer;
   @Autowired
private PersonalSectionService personalSectionService;
@Autowired
private ContactSectionService contactSectionService;
@Autowired
private CreditCardService creditCardService;


   //Constructors----------------------------------------------
   public ApplicationController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Application application) {
      ModelAndView result;

      result = createEditModelAndView(application, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Application application, String message) {
      ModelAndView result;

      result = new ModelAndView("application/edit");
      result.addObject("application", application);
      result.addObject("message", message);

      return result;

   }




   //Create Method -----------------------------------------------------------
   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int visaId) {
      ModelAndView result;
      try {
         Visa visa = visaSer.findOne(visaId);
         if (!visa.getPrice().equals(0.0)){
            Assert.notNull(inmigrantService.findByPrincipal().getCreditCard(),"The credit card is not valid, add a new one on your profile/ Tu tarjeta de cr�dito no es v�lida, a�ade una nueva en tu perfil");
            creditCardService.checkCreditCard2(inmigrantService.findByPrincipal().getCreditCard());
         }
         Application application = applicationService.create();
         Date date = new Date(System.currentTimeMillis());
         SimpleDateFormat myFormat = new SimpleDateFormat("yyMMdd", Locale.ENGLISH);
         String YYYMMMDDD = myFormat.format(date);
         char randomChar = (char)((int)'A'+Math.random()*((int)'Z'-(int)'A'+1));
         char randomChar1 = (char)((int)'A'+Math.random()*((int)'Z'-(int)'A'+1));
         char randomChar2 = (char)((int)'A'+Math.random()*((int)'Z'-(int)'A'+1));
         char randomChar3 = (char)((int)'A'+Math.random()*((int)'Z'-(int)'A'+1));
         int max = 99;
         int min = 10;
         Random rand = new Random();
         int randomNum = rand.nextInt((max - min) + 1) + min;
         String num = String.valueOf(randomNum);
         String tickr = YYYMMMDDD+"-"+randomChar+randomChar1+randomChar2+randomChar3+num;
         application.setTicker(tickr);
         application.setApplicant(inmigrantService.findByPrincipal());
         application.setStatus(Status.PENDING);
         application.setOpenDate(date);
         application.setClosed(false);
         Application application1 = applicationService.save(application);
         ContactSection contactSection = contactSectionService.create();
         contactSection.setApplication(application1);
         contactSection.setEmail(inmigrantService.findByPrincipal().getEmail());
         contactSection.setPhoneNumber(inmigrantService.findByPrincipal().getPhone());
         contactSection.setPageNumber("0");
         ContactSection cs1 = contactSectionService.save(contactSection);
         Collection<ContactSection> contactSections = new ArrayList<>();
         contactSections.add(cs1);
         application1.setContactSections(contactSections);
         application1.setVisa(visa);
         applicationService.save(application1);
         applicationService.flush();
         visaSer.save(visa);
         result = new ModelAndView("administrator/success");
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;

   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView applicationsList() {

      ModelAndView result;
      Collection<Application> applications;
      applications = applicationService.findAll();
      result = new ModelAndView("application/list");
      result.addObject("applications", applications);
      result.addObject("requestURI", "application/list.do");
      return result;
   }

   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
   public ModelAndView listLoggedImmigrantApplications() {

      ModelAndView result;
      Collection<Application> applications;
      Inmigrant inmigrant = inmigrantService.findByPrincipal();
      applications = inmigrant.getApplications();
      result = new ModelAndView("application/list");
      result.addObject("applications", applications);
      result.addObject("requestURI", "application/listMy.do");
      return result;
   }


   @RequestMapping(value = "/listAll", method = RequestMethod.GET)
   public ModelAndView listAllAppsForOfficerRevision() {

      ModelAndView result;
      Collection<Application> applications;
      applications = applicationService.findAll();
      result = new ModelAndView("application/list");
      result.addObject("applications", applications);
      result.addObject("applicationswithoutOfficer", applicationService.applicationswithoutOfficer());
      result.addObject("requestURI", "application/listAll.do");
      try {
         result.addObject("loggedOfficer", actorService.findByPrincipal());
      } catch (Exception e) {
         e.printStackTrace();
      }

      return result;
   }




   // Ancillary methods ------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int applicationId) {
      ModelAndView result;
      Application application;

      application = applicationService.findOne(applicationId);
      Assert.notNull(application);
      Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(application));
      result = createEditModelAndView(application);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Application application, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(application);
      } else {
         try {
            applicationService.save(application);
            result = new ModelAndView("redirect:listMy.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(application, "application.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int applicationId) {
      ModelAndView result;
      try {
         Application application = applicationService.findOne(applicationId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(application));
         applicationService.delete(application);
         result = applicationView(application.getId());
      } catch (Throwable oops) {
         Application application = applicationService.findOne(applicationId);
         result = createEditModelAndView(application, "application.commit.error");
      }

      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView applicationView(@RequestParam int applicationId) {

      ModelAndView result;
      Application application = applicationService.findOne(applicationId);


      result = new ModelAndView("application/view");
      applicationService.linkApplicationsfFromSameCountry(application);
      result.addObject("ticker", application.getTicker());
      result.addObject("officer", application.getOfficer());
      result.addObject("applicant", application.getApplicant());
      result.addObject("openDate", application.getOpenDate());
      result.addObject("closeDate", application.getCloseDate());
      result.addObject("statusChange", application.getStatusChange());
      result.addObject("statusComment", application.getStatusComment());
      result.addObject("visa", application.getVisa());
      result.addObject("assigneddd",application.getAssigned());

      result.addObject("personalSection", application.getPersonalSection());
      result.addObject("requestURLPersonalSection", "personalSection/list.do");

      result.addObject("contactSection", application.getContactSections());
      result.addObject("requestURLContactSection", "contactSection/list.do");

      result.addObject("socialSection", application.getSocialSections());
      result.addObject("requestURLSocialSection", "socialSection/list.do");

      result.addObject("educationSection", application.getEducationSections());
      result.addObject("requestURLEducationSection", "educationSection/list.do");

      result.addObject("workSection", application.getWorkSections());
      result.addObject("isClosed", application.getClosed());

      result.addObject("linkedApplications", application.getLinkedApplications());

      result.addObject("relatedQuestions", questionService.questionsOfApplication(application.getId()));
      result.addObject("requestURIQuestions", "question/list.do");

      try {
         result.addObject("loggedOfficer", actorService.findByPrincipal());
      } catch (Exception e) {
         e.printStackTrace();
      }

      result.addObject("appID", application.getId());
      result.addObject("requestURI", "application/view.do");

      return result;
   }

   @RequestMapping(value = "/statusViewOfficer", method = RequestMethod.GET)
   public ModelAndView applicationListStatus() {

      ModelAndView result;
      result = new ModelAndView("application/statusViewOfficer");

      result.addObject("accepted", officerService.acceptedApplicationsInWichImOfficer(officerService.findByPrincipal().getId()));
      result.addObject("pending", officerService.pendingApplicationsInWichImOfficer(officerService.findByPrincipal().getId()));
      result.addObject("rejected", officerService.rejectedApplicationsInWichImOfficer(officerService.findByPrincipal().getId()));

      result.addObject("requestURIaccepted", "application/statusViewOfficer.do");
      result.addObject("requestURIrejected", "application/statusViewOfficer.do");
      result.addObject("requestURIpending", "application/statusViewOfficer.do");
      try {
         result.addObject("loggedOfficer", actorService.findByPrincipal());
      } catch (Exception e) {
         e.printStackTrace();
      }

      result.addObject("requestURI", "application/statusViewOfficer.do");

      return result;
   }

   @RequestMapping(value = "/statusView", method = RequestMethod.GET)
   public ModelAndView applicationListStatusOfficer() {

      ModelAndView result;
      result = new ModelAndView("application/statusView");
      Collection<Application> oppended;
      oppended = inmigrantService.opennedAppsByImmigrantId(inmigrantService.findByPrincipal().getId());
      Collection<Application> closedaccepted;
      closedaccepted = inmigrantService.closedAndAcceptedAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      Collection<Application> closeddenied;
      closeddenied = inmigrantService.closedAndDeniedAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      Collection<Application> closedawaiting;
      closedawaiting = inmigrantService.closedAndAwaitingAppsByInmigrantId(inmigrantService.findByPrincipal().getId());
      result.addObject("oppended", oppended);
      result.addObject("closedaccepted", closedaccepted);
      result.addObject("closeddenied", closeddenied);
      result.addObject("closedawaiting", closedawaiting);
      result.addObject("requestURIoppended", "application/statusView.do");
      result.addObject("requestURIclosedaccepted", "application/statusView.do");
      result.addObject("requestURIcloseddenied", "application/statusView.do");
      result.addObject("requestURIclosedawaiting", "application/statusView.do");

      result.addObject("requestURI", "application/statusViewOfficer.do");

      return result;
   }
   @RequestMapping(value = "/close", method = RequestMethod.GET)
   public ModelAndView closeApplication(@RequestParam int applicationId, Locale locale) {
      ModelAndView result;
      try {
         Application application = applicationService.findOne(applicationId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(application));
         Date now = new Date(System.currentTimeMillis() - 100);

         application.setCloseDate(now);
         application.setClosed(true);

         Application aux = applicationService.save(application);
         List<Application> auxApplications = new ArrayList<>(aux.getLinkedApplications());
         for (Application a : auxApplications) {
            a.setClosed(true);
            a.setCloseDate(new Date(System.currentTimeMillis()-200));
            applicationService.save(a);
            applicationService.flush();
         }
         if("es".equals(locale.getLanguage())){
            mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " cerrada: " +" .","Solicitud cerrada", application.getApplicant().getId());
         }else{
            mezzageService.createActionMessage("Your application" +application.getTicker() + "it's been closed "  + " .","Application closed",application.getApplicant().getId());
         }
         result = applicationView(application.getId());
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }
   }

   @RequestMapping(value = "/open", method = RequestMethod.GET)
   public ModelAndView openApplication(@RequestParam int applicationId, Locale locale) {
      ModelAndView result;
      try {
         Application application = applicationService.findOne(applicationId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(application));
         application.setCloseDate(null);
         application.setClosed(false);
         Application aux = applicationService.save(application);
         List<Application> auxApplications = new ArrayList<>(aux.getLinkedApplications());
         for (Application a : auxApplications) {
            a.setClosed(false);
            a.setCloseDate(null);
            applicationService.save(a);
            applicationService.flush();
         }
         if("es".equals(locale.getLanguage())){
            mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " abierta: " +" .","Solicitud abierta", application.getApplicant().getId());
         }else{
            mezzageService.createActionMessage("Your application" +application.getTicker() + "it's been opened "  + " .","Application abierta",application.getApplicant().getId());
         }
         result = applicationView(application.getId());
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }
   }


   @RequestMapping(value = "/link", method = RequestMethod.GET)
   public ModelAndView linkTo(@RequestParam int applicationId) {
      ModelAndView result;
      try {
         Application application;
         application = applicationService.findOne(applicationId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(application));
         Assert.notNull(application);
         result = new ModelAndView("application/link");
         result.addObject("ap", application);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }


      return result;
   }

   @RequestMapping(value = "/selfassign", method = RequestMethod.GET)
   public ModelAndView selfassign(@RequestParam int applicationId, Locale locale) {

      ModelAndView result;
      try {
         Application application = applicationService.findOne(applicationId);
         application.setOfficer(officerService.findByPrincipal());

         Application application1 = applicationService.save(application);
         officerService.findByPrincipal().getApplications().add(application1);
         for (Application app : application1.getLinkedApplications()) {
            app.setOfficer(officerService.findByPrincipal());

            applicationService.save(app);
            officerService.findByPrincipal().getApplications().add(app);
         }
         if("es".equals(locale.getLanguage())){
            mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " ha est� asignada a: " + officerService.findByPrincipal().getName() + " .","Solicitud asignada", application.getApplicant().getId());
         }else{
            mezzageService.createActionMessage("Your application" +application.getTicker() + "it's been assigned to: " + officerService.findByPrincipal().getName() + " .","Application assigned",application.getApplicant().getId());
         }
         result = applicationView(application1.getId());
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", "Por favor, recarga la web para ver los cambios / Please, reload the page to view the changes");
         return result;
      }
      return result;
   }

   @RequestMapping(value = "/linkSubmit", method = RequestMethod.GET)
   public ModelAndView linkToSave(@RequestParam String ticker, @RequestParam String appId) {
      ModelAndView result;

      try {
         applicationService.linkTwoApplications(ticker, appId);
         Application applicationToLink = applicationService.getApplicationByTicker(ticker);
         result = applicationView(applicationService.getApplicationByTicker(ticker).getId());
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

   }
   @RequestMapping(value = "/accept", method = RequestMethod.GET)
   public ModelAndView acceptApplication(@RequestParam int applicationId, Locale locale) {
      ModelAndView result;
      try {
         Application application = applicationService.findOne(applicationId);
         Assert.isTrue(officerService.findByPrincipal().getApplications().contains(application));

         application.setStatus(Status.ACCEPTED);
         application.setStatusComment(null);
         application.setStatusChange(new Date(System.currentTimeMillis()-300));
//         application.setClosed(true);
//         application.setCloseDate(new Date(System.currentTimeMillis()-300));
         Application apsaved = applicationService.save(application);

         Long elapsedTime = applicationService.estimatedElapsedTime(apsaved);
         apsaved.setElapsedTime(elapsedTime);
         applicationService.save(apsaved);

         if("es".equals(locale.getLanguage())){
            mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " ha sido aceptada","Solicitud aprobada", application.getApplicant().getId());
         }else{
            mezzageService.createActionMessage("Your application" +application.getTicker() + "has been accepted","Application accepted",application.getApplicant().getId());
         }
         result = applicationView(application.getId());
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }
      return result;
   }
   @RequestMapping(value = "/deny", method = RequestMethod.GET)
   public ModelAndView deny(@RequestParam int applicationId) {
      ModelAndView result;
      try {
         Application application;
         application = applicationService.findOne(applicationId);
         Assert.isTrue(officerService.findByPrincipal().getApplications().contains(application));
         Assert.notNull(application);

         result = new ModelAndView("application/deny");
         result.addObject("ap", application);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }


      return result;
   }
   @RequestMapping(value = "/denySubmit", method = RequestMethod.GET)
   public ModelAndView denyCommentSave(@RequestParam String statusComment, @RequestParam String appId, Locale locale) {
      ModelAndView result;

      try {
         Assert.notNull(statusComment);
         Integer intAppId = new Integer(appId);
         Application application = applicationService.findOne(intAppId);
         application.setStatus(Status.REJECTED);
         application.setStatusComment(statusComment);
//         application.setClosed(true);
//         application.setCloseDate(new Date(System.currentTimeMillis()-300));
         application.setStatusChange(new Date(System.currentTimeMillis()-300));
         Application aux = applicationService.save(application);

         Long elapsedTime = applicationService.estimatedElapsedTime(aux);
         aux.setElapsedTime(elapsedTime);
         applicationService.save(aux);


         if("es".equals(locale.getLanguage())){
            mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " ha sido denegada. Raz�n: " + application.getStatusComment() + " .","Solicitud denegada", application.getApplicant().getId());
         }else{
            mezzageService.createActionMessage("Your application" +application.getTicker() + "has been rejected. Cause: " + application.getStatusComment() + " .","Application rejected",application.getApplicant().getId());
         }
         result = applicationView(aux.getId());
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

   }

   @RequestMapping(value = "/toInvestigate", method = RequestMethod.GET)
   public ModelAndView investigate(@RequestParam int applicationId) {
      ModelAndView result;
      try {
         Application application;
         application = applicationService.findOne(applicationId);
         Assert.isTrue(officerService.findByPrincipal().getApplications().contains(application));
         Assert.notNull(application);
         application.setAssigned(true);
         result = new ModelAndView("application/investigate");
         result.addObject("ap", application);
         result.addObject("investigators",investigatorService.findAll());
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }


      return result;
   }
   @RequestMapping(value = "/investigate", method = RequestMethod.GET)
   public ModelAndView investigateSubmit(@RequestParam String investigatorId, @RequestParam String appId, Locale locale) {
      ModelAndView result;

      try {
         Assert.notNull(investigatorId);
         Integer invId = new Integer(investigatorId);
         Investigator investigator = investigatorService.findOne(invId);
         Integer intAppId = new Integer(appId);
         Application application = applicationService.findOne(intAppId);
         investigator.getUnderInvestigation().add(application.getApplicant());
         Investigator aux = investigatorService.save(investigator);
         if("es".equals(locale.getLanguage())){
            mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " ha est� siendo investigada por: " + aux.getName() + " .","Solicitud bajo investigaci�n", application.getApplicant().getId());
         }else{
            mezzageService.createActionMessage("Your application" +application.getTicker() + "it's been investigated by: " + aux.getName() + " .","Application under investigation",application.getApplicant().getId());
         }
         result = applicationView(application.getId());
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

   }

}