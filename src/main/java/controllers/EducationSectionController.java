/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Application;
import domain.EducationSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ApplicationService;
import services.EducationSectionService;
import services.InmigrantService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/educationSection")
public class EducationSectionController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private EducationSectionService educationSectionService;
   @Autowired
   private ApplicationController applicationController;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private InmigrantService inmigrantService;

   //Constructors----------------------------------------------

   public EducationSectionController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(EducationSection educationSection) {
      ModelAndView result;

      result = createEditModelAndView(educationSection, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(EducationSection educationSection, String message) {
      ModelAndView result;

      result = new ModelAndView("educationSection/edit");
      result.addObject("educationSection", educationSection);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView educationSectionsList() {

      ModelAndView result;
      Collection<EducationSection> educationSection;

      educationSection = educationSectionService.findAll();
      result = new ModelAndView("educationSection/list");
      result.addObject("educationSections", educationSection);
      result.addObject("requestURI", "educationSection/list.do");

      return result;
   }

//   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
//   public ModelAndView listLoggedImmigrantEducationSections() {
//
//      ModelAndView result;
//      Collection<EducationSection> educationSection;
//      Inmigrant inmigrant = inmigrantService.findByPrincipal();
//
//      educationSection = inmigrant.getEducationSections();
//      result = new ModelAndView("educationSection/list");
//      result.addObject("educationSections", educationSection);
//      result.addObject("requestURI", "educationSection/list.do");
//
//      return result;
//   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int applicationId) {

      ModelAndView result;

      EducationSection educationSection = educationSectionService.create();
      Application application = applicationService.findOne(applicationId);

      educationSection.setApplication(application);
      applicationService.save(application);
      result = createEditModelAndView(educationSection);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int educationSectionId) {
      ModelAndView result;
      try {
         EducationSection educationSection;

         educationSection = educationSectionService.findOne(educationSectionId);
         Assert.notNull(educationSection);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(educationSection.getApplication()));
         result = createEditModelAndView(educationSection);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid EducationSection educationSection, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(educationSection);
      } else {
         try {
            educationSectionService.save(educationSection);
            applicationService.save(educationSection.getApplication());
            result = applicationController.applicationView(educationSection.getApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndView(educationSection, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int educationSectionId, @RequestParam int appId) {
      ModelAndView result;
      try {
         EducationSection educationSection = educationSectionService.findOne(educationSectionId);
         Application application = applicationService.findOne(appId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(educationSection.getApplication()));
         application.getEducationSections().remove(educationSection);
         applicationService.save(application);
         educationSection.setApplication(null);
         educationSectionService.delete(educationSection);
         result = applicationController.applicationView(application.getId());
      } catch (Throwable oops) {
         EducationSection educationSection = educationSectionService.findOne(educationSectionId);
         result = createEditModelAndView(educationSection, "general.commit.error");
      }

      return result;
   }

//   @RequestMapping(value = "/view", method = RequestMethod.GET)
//   public ModelAndView lessorViewAn(@RequestParam int educationSectionId) {
//
//      ModelAndView result;
//      EducationSection educationSection = educationSectionService.findOne(educationSectionId);
//
//
//      result = new ModelAndView("educationSection/view");
//      result.addObject("ticker", educationSection.getTicker());
//      result.addObject("officer", educationSection.getOfficer());
//      result.addObject("applicant", educationSection.getApplicant());
//      result.addObject("openDate", educationSection.getOpenDate());
//      result.addObject("closeDate", educationSection.getCloseDate());
//      result.addObject("statusChange", educationSection.getStatusChange());
//      result.addObject("statusComment", educationSection.getStatusComment());
//      result.addObject("visa", educationSection.getVisa());
//
//      result.addObject("educationSection", educationSection.getEducationSection());
//      result.addObject("requestURLEducationSection", "educationSection/list.do");
//
//      result.addObject("contactSection", educationSection.getContactSections());
//      result.addObject("requestURLContactSection", "contactSection/list.do");
//
//      result.addObject("socialSection", educationSection.getSocialSections());
//      result.addObject("requestURLSocialSection", "socialSection/list.do");
//
//      result.addObject("educationSection", educationSection.getEducationSections());
//      result.addObject("requestURLEducationSection", "educationSection/list.do");
//
//      result.addObject("workSection", educationSection.getWorkSections());
//      result.addObject("requestURLWorkSection", "workSection/list.do");
//
//
//      result.addObject("requestURI", "educationSection/view.do");
//      return result;
//   }

}
