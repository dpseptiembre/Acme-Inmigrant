package controllers;


import domain.Evaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.EvaluationService;
import services.MezzageService;
import services.SupervisorService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Locale;

@Controller
@RequestMapping("/evaluation")
public class EvaluationController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private EvaluationService evaluationService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private SupervisorService supervisorService;

   //Constructors----------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView evaluationList() {

      ModelAndView result;
      Collection<Evaluation> evaluations;

      evaluations = evaluationService.findAll();
      result = new ModelAndView("evaluation/list");
      result.addObject("evaluations", evaluations);
      result.addObject("requestURI", "evaluation/list.do");

      return result;
   }


   //Create Method -----------------------------------------------------------

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int officerId) {

      ModelAndView result;

      Evaluation evaluation = evaluationService.create(officerId);
      result = createEditModelAndView(evaluation);

      return result;

   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Evaluation evaluation, BindingResult binding, Locale locale) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(evaluation);
      } else {
         try {
            evaluationService.save(evaluation);
            if("es".equals(locale.getLanguage())){
               mezzageService.createActionMessage("Estas siendo evaluado","La evaluación ha empezado",evaluation.getReceiver().getId());
            }else{
               mezzageService.createActionMessage("You are under evaluation","Investigation starts",evaluation.getReceiver().getId());
            }
            result = new ModelAndView("officer/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(evaluation, "general.commit.error");
         }
      }
      return result;
   }
   
   
   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int evaluationId) {
      ModelAndView result;

      try {
         Evaluation evaluation;
         evaluation = evaluationService.findOne(evaluationId);
         Assert.notNull(evaluation);
         Assert.isTrue(supervisorService.findByPrincipal().getEvaluations().contains(evaluation));
         result = createEditModelAndView(evaluation);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
   public ModelAndView delete(Evaluation evaluation) {
      ModelAndView result;
      try {
         evaluationService.delete(evaluation);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(evaluation, "evaluation.commit.error");
      }

      return result;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Evaluation evaluation) {
      ModelAndView result;

      result = createEditModelAndView(evaluation, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Evaluation evaluation, String message) {
      ModelAndView result;

      result = new ModelAndView("evaluation/edit");
      result.addObject("evaluation", evaluation);
      result.addObject("message", message);


      return result;

   }


}
