/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Application;
import domain.Inmigrant;
import domain.SocialSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ApplicationService;
import services.InmigrantService;
import services.SocialSectionService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/socialSection")
public class SocialSectionController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private SocialSectionService socialSectionService;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private ApplicationController applicationController;
   @Autowired
   private InmigrantService inmigrantService;



   //Constructors----------------------------------------------

   public SocialSectionController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(SocialSection socialSection) {
      ModelAndView result;

      result = createEditModelAndView(socialSection, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(SocialSection socialSection, String message) {
      ModelAndView result;

      result = new ModelAndView("socialSection/edit");
      result.addObject("socialSection", socialSection);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView socialSectionsList() {

      ModelAndView result;
      Collection<SocialSection> socialSection;

      socialSection = socialSectionService.findAll();
      result = new ModelAndView("socialSection/list");
      result.addObject("socialSections", socialSection);
      result.addObject("requestURI", "socialSection/list.do");

      return result;
   }

//   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
//   public ModelAndView listLoggedImmigrantSocialSections() {
//
//      ModelAndView result;
//      Collection<SocialSection> socialSection;
//      Inmigrant inmigrant = inmigrantService.findByPrincipal();
//
//      socialSection = inmigrant.getSocialSections();
//      result = new ModelAndView("socialSection/list");
//      result.addObject("socialSections", socialSection);
//      result.addObject("requestURI", "socialSection/list.do");
//
//      return result;
//   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int applicationId) {

      ModelAndView result;

      SocialSection socialSection = socialSectionService.create();
      Application application = applicationService.findOne(applicationId);

      socialSection.setApplication(application);
      applicationService.save(application);


      result = createEditModelAndView(socialSection);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int socialSectionId) {
      ModelAndView result;
      try {
         SocialSection socialSection;
         socialSection = socialSectionService.findOne(socialSectionId);
         Assert.notNull(socialSection);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(socialSection.getApplication()));
         result = createEditModelAndView(socialSection);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid SocialSection socialSection, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(socialSection);
      } else {
         try {
            Application application = socialSection.getApplication();
            application.getSocialSections().add(socialSection);
            SocialSection socialSection1 = socialSectionService.save(socialSection);
            applicationService.save(application);
            result = applicationController.applicationView(socialSection1.getApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndView(socialSection, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int socialSectionId, @RequestParam int appId) {
      ModelAndView result;
      try {
         SocialSection socialSection = socialSectionService.findOne(socialSectionId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(socialSection.getApplication()));
         Application application = applicationService.findOne(appId);
         application.getSocialSections().remove(socialSection);
         applicationService.save(application);
         socialSection.setApplication(null);
         socialSectionService.delete(socialSection);
         result = applicationController.applicationView(application.getId());
      } catch (Throwable oops) {
         SocialSection socialSection = socialSectionService.findOne(socialSectionId);
         result = createEditModelAndView(socialSection, "general.commit.error");
      }

      return result;
   }

//   @RequestMapping(value = "/view", method = RequestMethod.GET)
//   public ModelAndView lessorViewAn(@RequestParam int socialSectionId) {
//
//      ModelAndView result;
//      SocialSection socialSection = socialSectionService.findOne(socialSectionId);
//
//
//      result = new ModelAndView("socialSection/view");
//      result.addObject("ticker", socialSection.getTicker());
//      result.addObject("officer", socialSection.getOfficer());
//      result.addObject("applicant", socialSection.getApplicant());
//      result.addObject("openDate", socialSection.getOpenDate());
//      result.addObject("closeDate", socialSection.getCloseDate());
//      result.addObject("statusChange", socialSection.getStatusChange());
//      result.addObject("statusComment", socialSection.getStatusComment());
//      result.addObject("visa", socialSection.getVisa());
//
//      result.addObject("socialSection", socialSection.getSocialSection());
//      result.addObject("requestURLSocialSection", "socialSection/list.do");
//
//      result.addObject("contactSection", socialSection.getContactSections());
//      result.addObject("requestURLContactSection", "contactSection/list.do");
//
//      result.addObject("socialSection", socialSection.getSocialSections());
//      result.addObject("requestURLSocialSection", "socialSection/list.do");
//
//      result.addObject("educationSection", socialSection.getEducationSections());
//      result.addObject("requestURLEducationSection", "educationSection/list.do");
//
//      result.addObject("workSection", socialSection.getWorkSections());
//      result.addObject("requestURLWorkSection", "workSection/list.do");
//
//
//      result.addObject("requestURI", "socialSection/view.do");
//      return result;
//   }

}
