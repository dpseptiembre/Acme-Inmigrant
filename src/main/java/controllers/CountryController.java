/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CountryService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/country")
public class CountryController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private CountryService countryService;


   //Constructors----------------------------------------------

   public CountryController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Country country) {
      ModelAndView result;

      result = createEditModelAndView(country, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Country country, String message) {
      ModelAndView result;

      result = new ModelAndView("country/edit");
      result.addObject("country", country);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView countrysList() {

      ModelAndView result;
      Collection<Country> country;

      country = countryService.findAll();
      result = new ModelAndView("country/list");
      result.addObject("countries", country);
      result.addObject("requestURI", "country/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Country country = countryService.create();
      result = createEditModelAndView(country);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int countryId) {
      ModelAndView result;
      Country country;

      country = countryService.findOne(countryId);
      Assert.notNull(country);
      result = createEditModelAndView(country);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Country country, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(country);
      } else {
         try {
            countryService.save(country);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(country, "country.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int countryId) {
      ModelAndView result;
      try {
         Country country = countryService.findOne(countryId);
         country.setHidden(true);
         countryService.save(country);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         Country country = countryService.findOne(countryId);
         result = createEditModelAndView(country, "country.commit.error");
      }

      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int countryId) {
      ModelAndView result;
      try {
         Country country = countryService.findOne(countryId);


         String str=country.getWikiLink();
         String[] parts = str.split("/");
         List<String> partsList = new ArrayList<String>();
         Collections.addAll(partsList,parts);
         String page = partsList.get(partsList.size()-1);

         result = new ModelAndView("country/view");
         result.addObject("c",country);
         result.addObject("page",page);
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
         return result;
      }

      return result;
   }

}
