/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.FinderMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.FinderMessageService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/finderMessage")
public class FinderMessageController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private FinderMessageService finderMessageService;


   //Constructors----------------------------------------------

   public FinderMessageController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(FinderMessage finderMessage) {
      ModelAndView result;

      result = createEditModelAndView(finderMessage, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(FinderMessage finderMessage, String message) {
      ModelAndView result;

      result = new ModelAndView("finderMessage/edit");
      result.addObject("finderMessage", finderMessage);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView finderMessagesList() {

      ModelAndView result;
      Collection<FinderMessage> finderMessage;

      finderMessage = finderMessageService.finderMessagesPerActor();
      result = new ModelAndView("finderMessages/list");
      result.addObject("findermessages", finderMessage);
      result.addObject("requestURI", "finderMessage/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      FinderMessage finderMessage = finderMessageService.create();
      result = createEditModelAndView(finderMessage);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int finderMessageId) {
      ModelAndView result;
      FinderMessage finderMessage;

      finderMessage = finderMessageService.findOne(finderMessageId);
      Assert.notNull(finderMessage);
      result = createEditModelAndView(finderMessage);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid FinderMessage finderMessage, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(finderMessage);
      } else {
         try {
            finderMessageService.save(finderMessage);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(finderMessage, "finderMessage.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int finderMessageId) {
      ModelAndView result;
      try {
         FinderMessage finderMessage = finderMessageService.findOne(finderMessageId);
         finderMessageService.delete(finderMessage);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         FinderMessage finderMessage = finderMessageService.findOne(finderMessageId);
         result = createEditModelAndView(finderMessage, "finderMessage.commit.error");
      }

      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int finderMessageId) {
      ModelAndView result;
      try {
         FinderMessage finderMessage = finderMessageService.findOne(finderMessageId);
         result = new ModelAndView("finderMessages/search");
         result.addObject("mezzages",finderMessage.getResults());
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
         return result;
      }

      return result;
   }

}
