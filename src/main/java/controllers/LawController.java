/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Country;
import domain.Law;
import domain.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CountryService;
import services.LawService;
import services.RequirementService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/law")
public class LawController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private LawService lawService;
   @Autowired
   private CountryService countryService;
   @Autowired
   private RequirementService requirementService;


   //Constructors----------------------------------------------

   public LawController() {
      super();
   }

   private ModelAndView createEditModelAndView(Law law) {
      ModelAndView result;

      result = createEditModelAndView(law, null);

      return result;
   }


   private ModelAndView createEditModelAndView(Law law, String message) {
      ModelAndView result;
      Collection<Country> countries = countryService.findAll();
      result = new ModelAndView("law/edit");
      result.addObject("law", law);
      result.addObject("message", message);
      result.addObject("countries", countries);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView lawsList() {

      ModelAndView result;
      Collection<Law> law;

      law = lawService.notHiddenLaws();
      result = new ModelAndView("law/list");
      result.addObject("laws", law);
      result.addObject("requestURI", "law/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Law law = lawService.create();
      result = createEditModelAndView(law);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int lawId) {
      ModelAndView result;
      Law law;

      law = lawService.findOne(lawId);
      Assert.notNull(law);
      result = createEditModelAndView(law);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Law law, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(law);
      } else {
         try {
            lawService.save(law);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(law, "law.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int lawId) {
      ModelAndView result;
      try {
         Law law = lawService.findOne(lawId);
         law.setHidden(true);
         lawService.save(law);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         Law law = lawService.findOne(lawId);
         result = createEditModelAndView(law, "law.commit.error");
      }

      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int lawId) {
      ModelAndView result;
      try {
         Law law = lawService.findOne(lawId);
         result = new ModelAndView("law/view");
         result.addObject("l", law);
         result.addObject("requirements", law.getRequirements());
         result.addObject("laws", law.getRelatedLaws());
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }

}
