/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CategoryService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private CategoryService categoryService;


   //Constructors----------------------------------------------

   public CategoryController() {
      super();
   }

   private ModelAndView createEditModelAndView(Category category) {
      ModelAndView result;

      result = createEditModelAndView(category, null);

      return result;
   }


   private ModelAndView createEditModelAndView(Category category, String message) {
      ModelAndView result;

      result = new ModelAndView("category/edit");
      Set<Category> categories = new HashSet<>(categoryService.findAll());
      categories.remove(category);
      result.addObject("category", category);
      result.addObject("categories", categories);
      result.addObject("nm",category.getName());
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView categorysList() {

      ModelAndView result;
      Collection<Category> category;

      category = categoryService.notHiddenCategorys();
      result = new ModelAndView("category/list");
      result.addObject("categories", category);
      result.addObject("requestURI", "category/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Category category = categoryService.create();
      result = createEditModelAndView(category);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int categoryId) {
      ModelAndView result;
      Category category;

      category = categoryService.findOne(categoryId);
      Assert.notNull(category);
      result = createEditModelAndView(category);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Category category, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(category);
      } else {
         try {
            categoryService.save(category);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(category, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int categoryId) {
      ModelAndView result;
      try {
         Category category = categoryService.findOne(categoryId);
         if (category.getName().equals("category 0")){
            throw new UnsupportedOperationException("That's the parent category, it can't be erased / Esta es la categor�a padre, no puede ser borrada");
         }
         category.setHidden(true);
         categoryService.save(category);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int categoryId) {
      ModelAndView result;
      try {
         Category category = categoryService.findOne(categoryId);
         result = new ModelAndView("category/view");
         result.addObject("cat", category);
         result.addObject("sonnz",category.getSons());
         result.addObject("requestURIsonnz", "category/list.do");
      } catch (Throwable oops) {
         Category category = categoryService.findOne(categoryId);
         result = createEditModelAndView(category, "category.commit.error");
      }

      return result;
   }

}
