/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.MezzageService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/mezzage")
public class MezzageController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ActorService actorService;


   //Constructors----------------------------------------------

   public MezzageController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Mezzage mezzage) {
      ModelAndView result;

      result = createEditModelAndView(mezzage, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Mezzage mezzage, String message) {
      ModelAndView result;

      result = new ModelAndView("mezzage/edit");
      result.addObject("mezzage", mezzage);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------





   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView mezzagesList() {

      ModelAndView result;
      Collection<Mezzage> mezzage;

      mezzage = mezzageService.findAll();
      result = new ModelAndView("mezzage/list");
      result.addObject("countries", mezzage);
      result.addObject("requestURI", "mezzage/list.do");

      return result;
   }


   @RequestMapping(value = "/listPublic", method = RequestMethod.GET)
   public ModelAndView mezzagesListPublic() {

      ModelAndView result;
      Collection<Mezzage> mezzage;

      mezzage = mezzageService.findAllPublic();
      result = new ModelAndView("mezzage/list");
      result.addObject("mezzages", mezzage);
      result.addObject("requestURI", "mezzage/listPublic.do");

      return result;
   }


   @RequestMapping(value = "/listPublicWithSpamWord", method = RequestMethod.GET)
   public ModelAndView mezzagesListPublicWithSpamWords() {

      ModelAndView result;
      Collection<Mezzage> mezzage;

      mezzage = mezzageService.publicFoldersWithSpamMessages();
      result = new ModelAndView("mezzage/list");
      result.addObject("mezzages", mezzage);
      result.addObject("requestURI", "mezzage/listPublicWithSpamWord.do");

      return result;
   }

   @RequestMapping(value = "/finder", method = RequestMethod.GET)
   public ModelAndView finder(@RequestParam String keyword, @RequestParam String startDate, @RequestParam String endDate) {

      ModelAndView result;
      try {
         Collection<Mezzage> mezzages = mezzageService.finder(keyword,startDate,endDate);
         result = new ModelAndView("mezzage/list");
         result.addObject("mezzages",mezzages);
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }


   @RequestMapping(value = "/find", method = RequestMethod.GET)
   public ModelAndView find() {
      ModelAndView result;
      try {
         result = new ModelAndView("mezzage/finder");
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Mezzage mezzage = mezzageService.create();
      result = createEditModelAndView(mezzage);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int mezzageId) {
      ModelAndView result;
      try {
         Mezzage mezzage;
         mezzage = mezzageService.findOne(mezzageId);
         Assert.notNull(mezzage);
         Assert.isTrue(actorService.findByPrincipal().getFolders().contains(mezzage));
         result = createEditModelAndView(mezzage);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Mezzage mezzage, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(mezzage);
      } else {
         try {
            Actor receiver = mezzageService.getActorByEmail(mezzage.getReceiverEmail());
            if (receiver == null){
               throw new IllegalArgumentException("We can't find the destination email");
            }
            List<Folder> receiverFolders = new ArrayList<>(receiver.getFolders());
            Folder destination = receiverFolders.get(0);
            mezzage.setReceiver(receiver);
            mezzage.setFolder(destination);
            Mezzage saved = mezzageService.save(mezzage);

            destination.getMezzages().add(saved);

            result = new ModelAndView("officer/success");
         } catch (Throwable oops) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", oops.getMessage());
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int mezzageId) {
      ModelAndView result;
      try {
         Mezzage mezzage = mezzageService.findOne(mezzageId);
         Assert.isTrue(actorService.findByPrincipal().getFolders().contains(mezzage));
         mezzage.setHidden(true);
         mezzageService.save(mezzage);
         result = new ModelAndView("officer/success");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace",oops.getStackTrace());
      }

      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView viewMezzage(@RequestParam int mezzageId){
      ModelAndView result;

      try {
         Mezzage mezzage = mezzageService.findOne(mezzageId);
         result = new ModelAndView("mezzage/view");
         result.addObject("m", mezzage);
      } catch (Throwable oops) {
         Mezzage mezzage = mezzageService.findOne(mezzageId);
         result = createEditModelAndView(mezzage, "mezzage.commit.error");
      }

      return result;
   }


}
