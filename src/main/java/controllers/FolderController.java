/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.FolderService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/folder")
public class FolderController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private FolderService folderService;
   @Autowired
   private ActorService actorService;


   //Constructors----------------------------------------------

   public FolderController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Folder folder) {
      ModelAndView result;

      result = createEditModelAndView(folder, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Folder folder, String message) {
      ModelAndView result;

      result = new ModelAndView("folder/edit");
      result.addObject("folder", folder);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView foldersList() {

      ModelAndView result;
      Collection<Folder> folder;

      folder = folderService.findAll();
      result = new ModelAndView("folder/list");
      result.addObject("countries", folder);
      result.addObject("requestURI", "folder/list.do");

      return result;
   }

   @RequestMapping(value = "/listMyFolders", method = RequestMethod.GET)
   public ModelAndView listMyFolders() {

      ModelAndView result;
      try {
         Collection<Folder> folder;
         folder = folderService.myFolders(actorService.findByPrincipal());
         result = new ModelAndView("folder/list");
         result.addObject("folders", folder);
         result.addObject("requestURI", "folder/list.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Folder folder = folderService.create();
      folder.setOwner(actorService.findByPrincipal());
      result = createEditModelAndView(folder);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int folderId) {
      ModelAndView result;
      try {
         Folder folder;
         folder = folderService.findOne(folderId);
         Assert.notNull(folder);
         Assert.isTrue(actorService.findByPrincipal().getFolders().contains(folder));
         result = createEditModelAndView(folder);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Folder folder, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(folder);
      } else {
         try {
            folder.setMezzages(new ArrayList<Mezzage>());
            folderService.save(folder);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(folder, "folder.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int folderId) {
      ModelAndView result;
      try {
         Folder folder = folderService.findOne(folderId);
         Assert.isTrue(actorService.findByPrincipal().getFolders().contains(folder));
         folderService.delete(folder);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         Folder folder = folderService.findOne(folderId);
         result = createEditModelAndView(folder, "folder.commit.error");
      }

      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int folderId) {
      ModelAndView result;
      try {
         Folder folder = folderService.findOne(folderId);
         result = new ModelAndView("folder/view");
         result.addObject("name",folder.getName());
         result.addObject("mezzages",folder.getMezzages());
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
         return result;
      }

      return result;
   }

}
