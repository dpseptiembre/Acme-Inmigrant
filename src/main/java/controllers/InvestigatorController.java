/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Investigator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.InvestigatorService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;

@Controller
@RequestMapping("/investigator")
public class InvestigatorController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private InvestigatorService investigatorService;


   //Constructors----------------------------------------------

   public InvestigatorController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Investigator investigator) {
      ModelAndView result;

      result = createEditModelAndView(investigator, null);

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView(Investigator investigator, String message) {
      ModelAndView result;

      result = new ModelAndView("investigator/edit");
      result.addObject("investigator", investigator);
      result.addObject("message", message);

      return result;

   }

   protected static ModelAndView createEditModelAndView2(Investigator investigator) {
      ModelAndView result;

      result = createEditModelAndView2(investigator, null);

      return result;
   }
   // Edition ---------------------------------------------------------

   protected static ModelAndView createEditModelAndView2(Investigator investigator, String message) {
      ModelAndView result;

      result = new ModelAndView("investigator/register");
      result.addObject("investigator", investigator);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView investigatorList() {

      ModelAndView result;
      Collection<Investigator> investigators;

      Collection<Investigator> res = new HashSet<>();
      investigators = investigatorService.findAll();


      result = new ModelAndView("investigator/list");
      result.addObject("investigator", res);
      result.addObject("requestURI", "investigator/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Investigator investigator= investigatorService.create();

      result = createEditModelAndView2(investigator);

      return result;

   }

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Investigator investigator, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView2(investigator);
      } else {
         try {
            investigatorService.registerAsInvestigator(investigator);
            result = new ModelAndView("investigator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView2(investigator, "general.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit() {
      ModelAndView result;
      Investigator investigator;
      investigator = investigatorService.findByPrincipal();
      Assert.notNull(investigator);
      result = createEditModelAndView(investigator);
      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Investigator investigator, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(investigator);
      } else {
         try {
            investigatorService.save(investigator);
            result = new ModelAndView("investigator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(investigator, "investigator.commit.error");
         }
      }
      return result;
   }
   

   // Ancillary methods ------------------------------------------------

}
