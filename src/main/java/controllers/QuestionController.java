/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Application;
import domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ApplicationService;
import services.MezzageService;
import services.OfficerService;
import services.QuestionService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

@Controller
@RequestMapping("/question")
public class QuestionController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private QuestionService questionService;
   @Autowired
   private ApplicationController applicationController;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private OfficerService officerService;
   @Autowired
   private MezzageService mezzageService;

   //Constructors----------------------------------------------

   public QuestionController() {
      super();
   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView questionsList() {

      ModelAndView result;
      Collection<Question> question;

      question = questionService.findAll();
      result = new ModelAndView("question/list");
      result.addObject("questions", question);
      result.addObject("requestURI", "question/list.do");

      return result;
   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int applicationId, Locale locale) {
      ModelAndView result;
      Question question = questionService.create();
      Application application = applicationService.findOne(applicationId);
      question.setMaker(officerService.findByPrincipal());
      question.setApplicant(application.getApplicant());
      question.setRelatedApplication(application);
      applicationService.save(application);
      if("es".equals(locale.getLanguage())){
         mezzageService.createActionMessage("Tu solicitud " +application.getTicker() + " ha sido cuestionada  por: " + officerService.findByPrincipal().getName() + " .","Solicitud bajo cuestionamiento", application.getApplicant().getId());
      }else{
         mezzageService.createActionMessage("Your application" +application.getTicker() + "it's been questioned by: " + officerService.findByPrincipal().getName() + " .","Application under question",application.getApplicant().getId());
      }
      result = createEditModelAndView(question);
      return result;
   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/reply", method = RequestMethod.GET)
   public ModelAndView reply(@RequestParam int questionId) {
      ModelAndView result;
      Question question;

      question = questionService.findOne(questionId);
      Assert.notNull(question);
      result = createEditModelAndViewReply(question);
      result.addObject("statementt", question.getStatement());
      return result;
   }


   @RequestMapping(value = "/reply", method = RequestMethod.POST, params = "save")
   public ModelAndView reply(@Valid Question question, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndViewReply(question);
      } else {
         try {
            question.setWriteDate(new Date(System.currentTimeMillis() - 500));
            questionService.save(question);
            result = applicationController.applicationView(question.getRelatedApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndViewReply(question, "question.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int questionId) {
      ModelAndView result;
      Question question;

      question = questionService.findOne(questionId);
      Assert.notNull(question);
      result = createEditModelAndView(question);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Question question, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(question);
      } else {
         try {

            question.setWriteDate(new Date(System.currentTimeMillis() - 100));
            questionService.save(question);
            result = applicationController.applicationView(question.getRelatedApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndView(question, "question.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView questionView(@RequestParam int questionId) {

      ModelAndView result;
      Question question = questionService.findOne(questionId);

      result = new ModelAndView("question/view");
      result.addObject("statement", question.getStatement());
      result.addObject("writeDate", question.getWriteDate());
      result.addObject("answer", question.getAnswer());
      result.addObject("applicant", question.getApplicant());
      result.addObject("maker", question.getMaker());
      result.addObject("relatedApplication", question.getRelatedApplication());
      result.addObject("relatedAppId", question.getRelatedApplication().getId());
      result.addObject("requestURI", "question/view.do");

      return result;
   }


   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int questionId) {
      ModelAndView result;
      try {
         Question question = questionService.findOne(questionId);
         questionService.delete(question);
         result = applicationController.applicationView(question.getRelatedApplication().getId());
      } catch (Throwable oops) {
         Question question = questionService.findOne(questionId);
         result = createEditModelAndView(question, "question.commit.error");
      }

      return result;
   }


   // Model&View -----------------------------------------------------------------------------

   private ModelAndView createEditModelAndView(Question question) {
      ModelAndView result;
      result = createEditModelAndView(question, null);
      return result;
   }


   private ModelAndView createEditModelAndView(Question question, String message) {
      ModelAndView result;
      result = new ModelAndView("question/edit");
      result.addObject("question", question);
      result.addObject("message", message);
      return result;

   }

   private ModelAndView createEditModelAndViewReply(Question question) {
      ModelAndView result;

      result = createEditModelAndViewReply(question, null);

      return result;
   }


   private ModelAndView createEditModelAndViewReply(Question question, String message) {
      ModelAndView result;

      result = new ModelAndView("question/reply");
      result.addObject("question", question);
      result.addObject("message", message);

      return result;

   }

}
