/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Law;
import domain.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.LawService;
import services.RequirementService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/requirement")
public class RequirementController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private RequirementService requirementService;
   @Autowired
   private LawService lawService;


   //Constructors----------------------------------------------

   public RequirementController() {
      super();
   }

   private ModelAndView createEditModelAndView(Requirement requirement) {
      ModelAndView result;

      result = createEditModelAndView(requirement, null);

      return result;
   }


   private ModelAndView createEditModelAndView(Requirement requirement, String message) {
      ModelAndView result;
      Collection<Law> laws = lawService.findAll();
      result = new ModelAndView("requirement/edit");
      result.addObject("requirement", requirement);
      result.addObject("message", message);
      result.addObject("laws", laws);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView requirementsList() {

      ModelAndView result;
      Collection<Requirement> requirement;

      requirement = requirementService.notHiddenRequirements();
      result = new ModelAndView("requirement/list");
      result.addObject("requirements", requirement);
      result.addObject("requestURI", "requirement/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Requirement requirement = requirementService.create();
      result = createEditModelAndView(requirement);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int requirementId) {
      ModelAndView result;
      Requirement requirement;

      requirement = requirementService.findOne(requirementId);
      Assert.notNull(requirement);
      result = createEditModelAndView(requirement);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Requirement requirement, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(requirement);
      } else {
         try {
            requirementService.save(requirement);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(requirement, "requirement.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int requirementId) {
      ModelAndView result;
      try {
         Requirement requirement = requirementService.findOne(requirementId);
         requirement.setHidden(true);
         requirementService.save(requirement);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         Requirement requirement = requirementService.findOne(requirementId);
         result = createEditModelAndView(requirement, "requirement.commit.error");
      }

      return result;
   }

}
