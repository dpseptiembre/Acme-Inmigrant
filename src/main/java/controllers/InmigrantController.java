/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.CreditCard;
import domain.CreditCardType;
import domain.Inmigrant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import security.UserAccountService;
import services.CreditCardService;
import services.InmigrantService;
import services.InvestigatorService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;

@Controller
@RequestMapping("/inmigrant")
public class InmigrantController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private InvestigatorService investigatorService;

   //Constructors----------------------------------------------

   public InmigrantController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Inmigrant inmigrant) {
      ModelAndView result;

      result = createEditModelAndView(inmigrant, null);

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView(Inmigrant inmigrant, String message) {
      ModelAndView result;

      result = new ModelAndView("inmigrant/edit");
      result.addObject("inmigrant", inmigrant);
      result.addObject("message", message);

      return result;

   }

   protected static ModelAndView createEditModelAndView2(Inmigrant inmigrant) {
      ModelAndView result;

      result = createEditModelAndView2(inmigrant, null);

      return result;
   }
   // Edition ---------------------------------------------------------

   protected static ModelAndView createEditModelAndView2(Inmigrant inmigrant, String message) {
      ModelAndView result;

      result = new ModelAndView("inmigrant/register");
      result.addObject("inmigrant", inmigrant);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView inmigrantList() {

      ModelAndView result;
      Collection<Inmigrant> inmigrants;

      Collection<Inmigrant> res = new HashSet<>();
      inmigrants = inmigrantService.findAll();


      result = new ModelAndView("inmigrant/list");
      result.addObject("inmigrants", res);
      result.addObject("requestURI", "inmigrant/list.do");

      return result;
   }
   @RequestMapping(value = "/listUnderInvestigation", method = RequestMethod.GET)
   public ModelAndView inmigrantListUnderInvestigationToInvestigator() {

      ModelAndView result;
      Collection<Inmigrant> inmigrants;

      Collection<Inmigrant> res = new HashSet<>();
      inmigrants = investigatorService.underInvestigation();


      result = new ModelAndView("inmigrant/list");
      result.addObject("inmigrants", inmigrants);
      result.addObject("requestURI", "inmigrant/listUnderInvestigation.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Inmigrant inmigrant = inmigrantService.create();

      result = createEditModelAndView2(inmigrant);

      return result;

   }

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Inmigrant inmigrant, BindingResult binding) {
      ModelAndView result;
      if (!binding.hasErrors()) {
         result = createEditModelAndView2(inmigrant);
      } else {
         try {
            inmigrantService.registerAsInmigrant(inmigrant);
            result = new ModelAndView("investigator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView2(inmigrant, "general.commit.error");
         }
      }
      return result;
   }


   // Ancillary methods ------------------------------------------------


//    @RequestMapping(value = "/editp", method = RequestMethod.GET)
//    public ModelAndView editp() {
//        ModelAndView result;
//        Inmigrant inmigrant;
//
//        inmigrant = inmigrantService.findByPrincipal();
//        Assert.notNull(inmigrant);
//
//        result = createEditModelAndView(inmigrant);
//
//        return result;
//    }


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit() {
      ModelAndView result;
      Inmigrant inmigrant;
      inmigrant = inmigrantService.findByPrincipal();
      if (inmigrant.getCreditCard() == null) {
         CreditCard creditCard = creditCardService.create();
         creditCard.setOwner(inmigrant);
         creditCard.setCVV("000");
         creditCard.setHolderName(inmigrant.getName());
         creditCard.setMonth(1);
         creditCard.setYear(2021);
         creditCard.setType(CreditCardType.AMEX);
         creditCard.setNumber("00000000000000");
         inmigrant.setCreditCard(creditCard);
      }
      Assert.notNull(inmigrant);
      result = createEditModelAndView(inmigrant);

      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Inmigrant inmigrant, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(inmigrant);
      } else {
         try {
            inmigrantService.save(inmigrant);
//            creditCardService.save(inm1.getCreditCard());
            result = new ModelAndView("inmigrant/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(inmigrant, "inmigrant.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
   public ModelAndView delete(Inmigrant inmigrant) {
      ModelAndView result;
      try {
         inmigrantService.delete(inmigrant);
         result = new ModelAndView("welcome/index");
      } catch (Throwable oops) {
         result = createEditModelAndView(inmigrant, "inmigrant.commit.error");
      }

      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView lessorViewAn(@RequestParam int inmigrantId) {

      ModelAndView result;
      Inmigrant inmigrant = inmigrantService.findOne(inmigrantId);


      result = new ModelAndView("inmigrant/view");
      result.addObject("name", inmigrant.getName());
      result.addObject("surname", inmigrant.getSurname());
      result.addObject("email", inmigrant.getEmail());
      result.addObject("phoneNumber", inmigrant.getPhone());
      result.addObject("applications", inmigrant.getApplications());
      result.addObject("requestURI", "inmigrant/view.do");
      return result;
   }
}
