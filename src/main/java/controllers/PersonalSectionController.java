/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Application;
import domain.PersonalSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ApplicationService;
import services.InmigrantService;
import services.PersonalSectionService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/personalSection")
public class PersonalSectionController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private PersonalSectionService personalSectionService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private ApplicationService applicationService;

   @Autowired
   private ApplicationController applicationController;


   //Constructors----------------------------------------------

   public PersonalSectionController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(PersonalSection personalSection) {
      ModelAndView result;

      result = createEditModelAndView(personalSection, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(PersonalSection personalSection, String message) {
      ModelAndView result;

      result = new ModelAndView("personalSection/edit");
      result.addObject("personalSection", personalSection);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView personalSectionsList() {

      ModelAndView result;
      Collection<PersonalSection> personalSection;

      personalSection = personalSectionService.findAll();
      result = new ModelAndView("personalSection/list");
      result.addObject("personalSections", personalSection);
      result.addObject("requestURI", "personalSection/list.do");

      return result;
   }

//   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
//   public ModelAndView listLoggedImmigrantPersonalSections() {
//
//      ModelAndView result;
//      Collection<PersonalSection> personalSection;
//      Inmigrant inmigrant = inmigrantService.findByPrincipal();
//
//      personalSection = inmigrant.getPersonalSections();
//      result = new ModelAndView("personalSection/list");
//      result.addObject("personalSections", personalSection);
//      result.addObject("requestURI", "personalSection/list.do");
//
//      return result;
//   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int applicationId) {

      ModelAndView result;

      PersonalSection personalSection = personalSectionService.create();
      Application application = applicationService.findOne(applicationId);
      personalSection.setApplication(application);
      result = createEditModelAndView(personalSection);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int personalSectionId) {
      ModelAndView result;
      try {
         PersonalSection personalSection;

         personalSection = personalSectionService.findOne(personalSectionId);
         Assert.notNull(personalSection);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(personalSection.getApplication()));
         result = createEditModelAndView(personalSection);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid PersonalSection personalSection, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(personalSection);
      } else {
         try {
            personalSectionService.save(personalSection);
            applicationService.save(personalSection.getApplication());
            result = applicationController.applicationView(personalSection.getApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndView(personalSection, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/addName", method = RequestMethod.GET)
   public ModelAndView addName(@RequestParam int personalSectionId) {

      ModelAndView result;

      try {

         result = new ModelAndView("personalSection/addName");
         result.addObject("personalSectionId", personalSectionId);
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }


   }

   @RequestMapping(value = "/addNameSave", method = RequestMethod.GET)
   public ModelAndView addSaveName(@RequestParam String name, @RequestParam String personalSectionId) {

      ModelAndView result;

      try {
         personalSectionService.addName(name, personalSectionId);
         Integer psId = new Integer(personalSectionId);
         PersonalSection personalSection = personalSectionService.findOne(psId);
         result = applicationController.applicationView(personalSection.getApplication().getId());
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }


   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int personalSectionId) {
      ModelAndView result;
      try {
         PersonalSection personalSection = personalSectionService.findOne(personalSectionId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(personalSection.getApplication()));
         personalSectionService.delete(personalSection);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         PersonalSection personalSection = personalSectionService.findOne(personalSectionId);
         result = createEditModelAndView(personalSection, "general.commit.error");
      }

      return result;
   }

//   @RequestMapping(value = "/view", method = RequestMethod.GET)
//   public ModelAndView lessorViewAn(@RequestParam int personalSectionId) {
//
//      ModelAndView result;
//      PersonalSection personalSection = personalSectionService.findOne(personalSectionId);
//
//
//      result = new ModelAndView("personalSection/view");
//      result.addObject("ticker", personalSection.getTicker());
//      result.addObject("officer", personalSection.getOfficer());
//      result.addObject("applicant", personalSection.getApplicant());
//      result.addObject("openDate", personalSection.getOpenDate());
//      result.addObject("closeDate", personalSection.getCloseDate());
//      result.addObject("statusChange", personalSection.getStatusChange());
//      result.addObject("statusComment", personalSection.getStatusComment());
//      result.addObject("visa", personalSection.getVisa());
//
//      result.addObject("personalSection", personalSection.getPersonalSection());
//      result.addObject("requestURLPersonalSection", "personalSection/list.do");
//
//      result.addObject("contactSection", personalSection.getContactSections());
//      result.addObject("requestURLContactSection", "contactSection/list.do");
//
//      result.addObject("socialSection", personalSection.getSocialSections());
//      result.addObject("requestURLSocialSection", "socialSection/list.do");
//
//      result.addObject("educationSection", personalSection.getEducationSections());
//      result.addObject("requestURLEducationSection", "educationSection/list.do");
//
//      result.addObject("workSection", personalSection.getWorkSections());
//      result.addObject("requestURLWorkSection", "workSection/list.do");
//
//
//      result.addObject("requestURI", "personalSection/view.do");
//      return result;
//   }

}
