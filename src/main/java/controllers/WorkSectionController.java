/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Application;
import domain.WorkSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ApplicationService;
import services.InmigrantService;
import services.WorkSectionService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/workSection")
public class WorkSectionController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private WorkSectionService workSectionService;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private ApplicationController applicationController;
   @Autowired
   private InmigrantService inmigrantService;


   //Constructors----------------------------------------------

   public WorkSectionController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(WorkSection workSection) {
      ModelAndView result;

      result = createEditModelAndView(workSection, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(WorkSection workSection, String message) {
      ModelAndView result;

      result = new ModelAndView("workSection/edit");
      result.addObject("workSection", workSection);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView workSectionsList() {

      ModelAndView result;
      Collection<WorkSection> workSection;

      workSection = workSectionService.findAll();
      result = new ModelAndView("workSection/list");
      result.addObject("workSections", workSection);
      result.addObject("requestURI", "workSection/list.do");

      return result;
   }

//   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
//   public ModelAndView listLoggedImmigrantWorkSections() {
//
//      ModelAndView result;
//      Collection<WorkSection> workSection;
//      Inmigrant inmigrant = inmigrantService.findByPrincipal();
//
//      workSection = inmigrant.getWorkSections();
//      result = new ModelAndView("workSection/list");
//      result.addObject("workSections", workSection);
//      result.addObject("requestURI", "workSection/list.do");
//
//      return result;
//   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int applicationId) {

      ModelAndView result;
      Application application = applicationService.findOne(applicationId);

      WorkSection workSection = workSectionService.create();
      workSection.setApplication(application);
      applicationService.save(application);
      result = createEditModelAndView(workSection);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int workSectionId) {
      ModelAndView result;

      try {
         WorkSection workSection;
         workSection = workSectionService.findOne(workSectionId);
         Assert.notNull(workSection);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(workSection.getApplication()));
         result = createEditModelAndView(workSection);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid WorkSection workSection, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(workSection);
      } else {
         try {
            workSectionService.save(workSection);
            applicationService.save(workSection.getApplication());
            result = applicationController.applicationView(workSection.getApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndView(workSection, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int workSectionId, @RequestParam int appId) {
      ModelAndView result;
      try {
         WorkSection workSection = workSectionService.findOne(workSectionId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(workSection.getApplication()));
         Application application = applicationService.findOne(appId);
         application.getWorkSections().remove(workSection);
         applicationService.save(application);
         workSection.setApplication(null);
         workSectionService.delete(workSection);
         result = applicationController.applicationView(application.getId());
      } catch (Throwable oops) {
         WorkSection workSection = workSectionService.findOne(workSectionId);
         result = createEditModelAndView(workSection, "general.commit.error");
      }

      return result;
   }

//   @RequestMapping(value = "/view", method = RequestMethod.GET)
//   public ModelAndView lessorViewAn(@RequestParam int workSectionId) {
//
//      ModelAndView result;
//      WorkSection workSection = workSectionService.findOne(workSectionId);
//
//
//      result = new ModelAndView("workSection/view");
//      result.addObject("ticker", workSection.getTicker());
//      result.addObject("officer", workSection.getOfficer());
//      result.addObject("applicant", workSection.getApplicant());
//      result.addObject("openDate", workSection.getOpenDate());
//      result.addObject("closeDate", workSection.getCloseDate());
//      result.addObject("statusChange", workSection.getStatusChange());
//      result.addObject("statusComment", workSection.getStatusComment());
//      result.addObject("visa", workSection.getVisa());
//
//      result.addObject("workSection", workSection.getWorkSection());
//      result.addObject("requestURLWorkSection", "workSection/list.do");
//
//      result.addObject("contactSection", workSection.getContactSections());
//      result.addObject("requestURLContactSection", "contactSection/list.do");
//
//      result.addObject("socialSection", workSection.getSocialSections());
//      result.addObject("requestURLSocialSection", "socialSection/list.do");
//
//      result.addObject("educationSection", workSection.getEducationSections());
//      result.addObject("requestURLEducationSection", "educationSection/list.do");
//
//      result.addObject("workSection", workSection.getWorkSections());
//      result.addObject("requestURLWorkSection", "workSection/list.do");
//
//
//      result.addObject("requestURI", "workSection/view.do");
//      return result;
//   }

}
