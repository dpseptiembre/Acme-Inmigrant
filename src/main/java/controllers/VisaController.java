/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Visa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import security.Authority;
import services.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/visa")
public class VisaController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private VisaService visaService;
   @Autowired
   private CountryService countryService;
   @Autowired
   private CategoryService categoryService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private ContactSectionService contactSectionService;
   @Autowired
   private PersonalSectionService personalSectionService;
   @Autowired
   private ApplicationController applicationController;

   //Constructors----------------------------------------------

   public VisaController() {
      super();
   }

   private  ModelAndView createEditModelAndView(Visa visa) {
      ModelAndView result;

      result = createEditModelAndView(visa, null);

      return result;
   }


   private  ModelAndView createEditModelAndView(Visa visa, String message) {
      ModelAndView result;


      result = new ModelAndView("visa/edit");
      result.addObject("visa", visa);
      result.addObject("message", message);
      result.addObject("countries", countryService.findAll());
      result.addObject("categories", categoryService.findAll());

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView visasList() {

      ModelAndView result;
      Collection<Visa> visa;

      visa = visaService.findAll();
      result = new ModelAndView("visa/list");
      result.addObject("visas", visa);
      result.addObject("requestURI", "visa/list.do");

      return result;
   }

   @RequestMapping(value = "/listCountriesFromVIsa", method = RequestMethod.GET)
   public ModelAndView countriesFromVisaList(@RequestParam int countryId) {

      ModelAndView result;
      Collection<Visa> visas;
      visas = visaService.visasFromCountry(countryId);

      result = new ModelAndView("visa/list");
      result.addObject("visas", visas);
      result.addObject("requestURI", "visa/list.do");

      return result;
   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Visa visa = visaService.create();
      result = createEditModelAndView(visa);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int visaId) {
      ModelAndView result;
      Visa visa;

      visa = visaService.findOne(visaId);
      Assert.notNull(visa);
      result = createEditModelAndView(visa);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Visa visa, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(visa);
      } else {
         try {
            visaService.save(visa);
            result = new ModelAndView("officer/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(visa, "visa.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int visaId) {
      ModelAndView result;
      try {
         Visa visa = visaService.findOne(visaId);
         visa.setHidden(true);
         visaService.save(visa);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         Visa visa = visaService.findOne(visaId);
         result = createEditModelAndView(visa, "visa.commit.error");
      }

      return result;
   }

   @RequestMapping(value = "/abrogate", method = RequestMethod.GET)
   public ModelAndView abrogateVisa(@RequestParam int visaId){
      ModelAndView result;
      try {
         Visa visa = visaService.findOne(visaId);
         if(visa.isAbrogated()){
            throw new UnsupportedOperationException("This visa is already abrogated");
         }else {
            visa.setAbrogated(true);
            visaService.save(visa);
         }
         result = new ModelAndView("officer/success");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
      }

      return result;
   }

   @RequestMapping(value = "/viewAnon", method = RequestMethod.GET)
   public ModelAndView viewAnon(@RequestParam int visaId) {

      ModelAndView result;
      Visa visa = visaService.findOne(visaId);
      result = new ModelAndView("visa/view");
      result.addObject("clazz", visa.getClazz());
      result.addObject("description", visa.getDescription());
      result.addObject("price", visa.getPrice());
      result.addObject("currency", visa.getCurrency());
      result.addObject("category", visa.getCategory());
      result.addObject("country", visa.getCountry());
      result.addObject("applications", visa.getApplications());
      result.addObject("requirements", visa.getRequirements());
      result.addObject("requestURIRequs", "requirement/list.do");
      result.addObject("requestURI", "visa/viewAnon.do");
      return result;
   }
   @RequestMapping(value = "/viewAuthenticated", method = RequestMethod.GET)
   public ModelAndView viewAuthenticated(@RequestParam int visaId) {

      ModelAndView result;
      Visa visa = visaService.findOne(visaId);
      result = new ModelAndView("visa/view");
      Boolean noYetAppliedFor = false;
      result.addObject("clazz", visa.getClazz());
      result.addObject("description", visa.getDescription());
      result.addObject("price", visa.getPrice());
      result.addObject("currency", visa.getCurrency());
      result.addObject("category", visa.getCategory());
      result.addObject("country", visa.getCountry());
      result.addObject("applications", visa.getApplications());
      result.addObject("id", visa.getId());
      result.addObject("requirements", visa.getRequirements());
      result.addObject("requestURIRequs", "requirement/list.do");

      result.addObject("requestURI", "visa/viewAuthenticated.do");
      try {
         Authority authority = new Authority();
         authority.setAuthority(Authority.INMIGRANT);
         if (actorService.findByPrincipal().getUserAccount().getAuthorities().contains(authority)){
            Collection<Visa> visas = visaService.myVisas(inmigrantService.findByPrincipal().getId());
            noYetAppliedFor = !visas.contains(visa);
         }

      } catch (Exception e) {
         e.printStackTrace();
      }
      result.addObject("noYetAppliedFor",noYetAppliedFor);
      return result;
   }



}
