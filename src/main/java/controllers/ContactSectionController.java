/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Application;
import domain.ContactSection;
import domain.Inmigrant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ApplicationService;
import services.ContactSectionService;
import services.InmigrantService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/contactSection")
public class ContactSectionController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private ContactSectionService contactSectionService;
   @Autowired
   private InmigrantService inmigrantService;
   @Autowired
   private ApplicationController applicationController;
   @Autowired
   private ApplicationService applicationService;



   //Constructors----------------------------------------------

   public ContactSectionController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(ContactSection contactSection) {
      ModelAndView result;

      result = createEditModelAndView(contactSection, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(ContactSection contactSection, String message) {
      ModelAndView result;

      result = new ModelAndView("contactSection/edit");
      result.addObject("contactSection", contactSection);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView contactSectionsList() {

      ModelAndView result;
      Collection<ContactSection> contactSection;

      contactSection = contactSectionService.findAll();
      result = new ModelAndView("contactSection/list");
      result.addObject("contactSections", contactSection);
      result.addObject("requestURI", "contactSection/list.do");

      return result;
   }

//   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
//   public ModelAndView listLoggedImmigrantContactSections() {
//
//      ModelAndView result;
//      Collection<ContactSection> contactSection;
//      Inmigrant inmigrant = inmigrantService.findByPrincipal();
//
//      contactSection = inmigrant.getContactSections();
//      result = new ModelAndView("contactSection/list");
//      result.addObject("contactSections", contactSection);
//      result.addObject("requestURI", "contactSection/list.do");
//
//      return result;
//   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int applicationId) {

      ModelAndView result;

      ContactSection contactSection = contactSectionService.create();
      Application application = applicationService.findOne(applicationId);

      contactSection.setApplication(application);
      applicationService.save(application);
      result = createEditModelAndView(contactSection);

      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int contactSectionId) {
      ModelAndView result;
      try {
         ContactSection contactSection;

         contactSection = contactSectionService.findOne(contactSectionId);
         Assert.notNull(contactSection);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(contactSection.getApplication()));
         result = createEditModelAndView(contactSection);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
         return result;
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid ContactSection contactSection, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(contactSection);
      } else {
         try {
            Application application = contactSection.getApplication();
            application.getContactSections().add(contactSection);
            ContactSection contactSection1 = contactSectionService.save(contactSection);
            applicationService.save(application);
            result = applicationController.applicationView(contactSection1.getApplication().getId());
         } catch (Throwable oops) {
            result = createEditModelAndView(contactSection, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@RequestParam int contactSectionId, @RequestParam int appId) {
      ModelAndView result;
      try {
         ContactSection contactSection = contactSectionService.findOne(contactSectionId);
         Assert.isTrue(inmigrantService.findByPrincipal().getApplications().contains(contactSection.getApplication()));
         Application application = applicationService.findOne(appId);
         application.getContactSections().remove(contactSection);
         applicationService.save(application);
         contactSection.setApplication(null);
         contactSectionService.delete(contactSection);

         result = applicationController.applicationView(application.getId());
      } catch (Throwable oops) {
         ContactSection contactSection = contactSectionService.findOne(contactSectionId);
         result = createEditModelAndView(contactSection, "general.commit.error");
      }

      return result;
   }

//   @RequestMapping(value = "/view", method = RequestMethod.GET)
//   public ModelAndView lessorViewAn(@RequestParam int contactSectionId) {
//
//      ModelAndView result;
//      ContactSection contactSection = contactSectionService.findOne(contactSectionId);
//
//
//      result = new ModelAndView("contactSection/view");
//      result.addObject("ticker", contactSection.getTicker());
//      result.addObject("officer", contactSection.getOfficer());
//      result.addObject("applicant", contactSection.getApplicant());
//      result.addObject("openDate", contactSection.getOpenDate());
//      result.addObject("closeDate", contactSection.getCloseDate());
//      result.addObject("statusChange", contactSection.getStatusChange());
//      result.addObject("statusComment", contactSection.getStatusComment());
//      result.addObject("visa", contactSection.getVisa());
//
//      result.addObject("contactSection", contactSection.getContactSection());
//      result.addObject("requestURLContactSection", "contactSection/list.do");
//
//      result.addObject("contactSection", contactSection.getContactSections());
//      result.addObject("requestURLContactSection", "contactSection/list.do");
//
//      result.addObject("socialSection", contactSection.getSocialSections());
//      result.addObject("requestURLSocialSection", "socialSection/list.do");
//
//      result.addObject("educationSection", contactSection.getEducationSections());
//      result.addObject("requestURLEducationSection", "educationSection/list.do");
//
//      result.addObject("workSection", contactSection.getWorkSections());
//      result.addObject("requestURLWorkSection", "workSection/list.do");
//
//
//      result.addObject("requestURI", "contactSection/view.do");
//      return result;
//   }

}
