/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Finder;
import domain.Visa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.FinderRepository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class FinderService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private FinderRepository finderRepository;
   @Autowired
   private VisaService visaService;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public FinderService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public Finder create() {
      return new Finder();
   }

   public Collection<Finder> findAll() {
      Collection<Finder> result;

      result = finderRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Finder findOne(int finderId) {
      Assert.isTrue(finderId != 0);
      Finder result;
      result = finderRepository.findOne(finderId);
      Assert.notNull(result);

      return result;
   }

   public Finder save(Finder finder) {
      Assert.notNull(finder);

      Finder result;

      result = finderRepository.save(finder);

      return result;
   }

   public void delete(Finder finder) {
      Assert.notNull(finder);
      Assert.isTrue(finder.getId() != 0);
      Assert.isTrue(finderRepository.exists(finder.getId()));

      finderRepository.delete(finder);
   }

   // Other business methods -------------------------------------------------

   public Set<Visa> finder(String keyword) {
      Set<Visa> results = new HashSet<>(visaService.findAll());
      Set<Visa> aux = new HashSet<>();
      for (Visa visa : results) {
         if (visa.getDescription().contains(keyword)) {
            aux.add(visa);
         }
         if (visa.getClazz().contains(keyword)) {
            aux.add(visa);
         }
      }
      if (aux.isEmpty()) {
         aux = new HashSet<>(visaService.findAll());
      }
      return aux;
   }
}
