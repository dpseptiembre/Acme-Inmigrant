/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RequirementRepository;

import java.util.Collection;

@Service
@Transactional
public class RequirementService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private RequirementRepository requirementRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public RequirementService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Requirement create(){
      Requirement requirement = new Requirement();
      requirement.setAbrogated(false);
      requirement.setHidden(false);
      return requirement;
   }

   public Collection<Requirement> findAll() {
      Collection<Requirement> result;

      result = requirementRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Requirement findOne(int requirementId) {
      Assert.isTrue(requirementId != 0);
      Requirement result;
      result = requirementRepository.findOne(requirementId);
      Assert.notNull(result);

      return result;
   }

   public Requirement save(Requirement requirement) {
      Assert.notNull(requirement);

      Requirement result;

      result = requirementRepository.save(requirement);

      return result;
   }

   public void delete(Requirement requirement) {
      Assert.notNull(requirement);
      Assert.isTrue(requirement.getId() != 0);
      Assert.isTrue(requirementRepository.exists(requirement.getId()));

      requirementRepository.delete(requirement);
   }

   // Other business methods -------------------------------------------------


   public Collection<Requirement> notHiddenRequirements(){
      return requirementRepository.notHiddenRequirements();
   }

}
