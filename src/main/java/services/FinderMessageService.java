/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.FinderMessage;
import domain.Visa;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.FinderMessageRepository;

import java.util.*;

@Service
@Transactional
public class FinderMessageService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private FinderMessageRepository finderMessageRepository;
   @Autowired
   private ActorService actorService;
   @Autowired
   private AdministratorService administratorService;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public FinderMessageService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public FinderMessage create() {
      return new FinderMessage();
   }

   public Collection<FinderMessage> findAll() {
      Collection<FinderMessage> result;

      result = finderMessageRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public FinderMessage findOne(int finderMessageId) {
      Assert.isTrue(finderMessageId != 0);
      FinderMessage result;
      result = finderMessageRepository.findOne(finderMessageId);
      Assert.notNull(result);

      return result;
   }

   public FinderMessage save(FinderMessage finderMessage) {
      Assert.notNull(finderMessage);

      FinderMessage result;

      result = finderMessageRepository.save(finderMessage);

      return result;
   }

   public void delete(FinderMessage finderMessage) {
      Assert.notNull(finderMessage);
      Assert.isTrue(finderMessage.getId() != 0);
      Assert.isTrue(finderMessageRepository.exists(finderMessage.getId()));

      finderMessageRepository.delete(finderMessage);
   }

   // Other business methods -------------------------------------------------


   public Collection<FinderMessage> finderMessagesPerActor(){

      Collection<FinderMessage> res = new ArrayList<>();

      try {
         int actorId = actorService.findByPrincipal().getId();
         int numbeOfCacheHours = administratorService.getUttils().getFinderCarche();
         Date cacheprev = new DateTime().minusHours(numbeOfCacheHours).toDate();

         //Managing finder cache
         Assert.notNull(actorId, "Actor id is null");
         res = finderMessageRepository.findermessagebyactor(actorId, cacheprev);
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;

   }

}
