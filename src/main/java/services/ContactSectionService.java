/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.ContactSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ContactSectionRepository;

import java.util.Collection;

@Service
@Transactional
public class ContactSectionService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private ContactSectionRepository contactSectionRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public ContactSectionService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public ContactSection create() {
      return new ContactSection();
   }
   public Collection<ContactSection> findAll() {
      Collection<ContactSection> result;

      result = contactSectionRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public ContactSection findOne(int contactSectionId) {
      Assert.isTrue(contactSectionId != 0);
      ContactSection result;
      result = contactSectionRepository.findOne(contactSectionId);
      Assert.notNull(result);

      return result;
   }

   public ContactSection save(ContactSection contactSection) {
      Assert.notNull(contactSection);

      ContactSection result;

      result = contactSectionRepository.save(contactSection);

      return result;
   }

   public void delete(ContactSection contactSection) {
      Assert.notNull(contactSection);
      Assert.isTrue(contactSection.getId() != 0);
      Assert.isTrue(contactSectionRepository.exists(contactSection.getId()));

      contactSectionRepository.delete(contactSection);
   }

   // Other business methods -------------------------------------------------


}
