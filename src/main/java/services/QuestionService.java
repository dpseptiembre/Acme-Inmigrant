/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.QuestionRepository;

import java.util.Collection;

@Service
@Transactional
public class QuestionService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private QuestionRepository questionRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public QuestionService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Question create() {
      return new Question();
   }


   public Collection<Question> findAll() {
      Collection<Question> result;

      result = questionRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Question findOne(int questionId) {
      Assert.isTrue(questionId != 0);
      Question result;
      result = questionRepository.findOne(questionId);
      Assert.notNull(result);

      return result;
   }

   public Question save(Question question) {
      Assert.notNull(question);

      Question result;

      result = questionRepository.save(question);

      return result;
   }

   public void delete(Question question) {
      Assert.notNull(question);
      Assert.isTrue(question.getId() != 0);
      Assert.isTrue(questionRepository.exists(question.getId()));

      questionRepository.delete(question);
   }

   // Other business methods -------------------------------------------------
   public Collection<Question> questionsOfApplication(int applicationId) {
      Assert.notNull(applicationId);
      return questionRepository.questionsOfApplication(applicationId);
   }

}
