/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.InmigrantRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class InmigrantService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private InmigrantRepository inmigrantRepository;

   // Supporting services ----------------------------------------------------

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private CreditCardService creditCardService;


   // Constructors -----------------------------------------------------------

   public InmigrantService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public Inmigrant create() {
      Inmigrant res = new Inmigrant();
      authoritySet(res);
      return res;
   }

   public Collection<Inmigrant> findAll() {
      Collection<Inmigrant> result;

      result = inmigrantRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Inmigrant findOne(int inmigrantId) {
      Assert.isTrue(inmigrantId != 0);
      Inmigrant result;
      result = inmigrantRepository.findOne(inmigrantId);
      Assert.notNull(result);

      return result;
   }

   public Inmigrant save(Inmigrant inmigrant) {
      Assert.notNull(inmigrant);

      Inmigrant result;

      result = inmigrantRepository.save(inmigrant);

      return result;
   }

   public void delete(Inmigrant inmigrant) {
      Assert.notNull(inmigrant);
      Assert.isTrue(inmigrant.getId() != 0);
      Assert.isTrue(inmigrantRepository.exists(inmigrant.getId()));

      inmigrantRepository.delete(inmigrant);
   }

   // Other business methods -------------------------------------------------

   public UserAccount findUserAccount(Inmigrant inmigrant) {
      Assert.notNull(inmigrant);

      UserAccount result;

      result = userAccountService.findByInmigrant(inmigrant);

      return result;
   }

   public Inmigrant findByPrincipal() {
      Inmigrant result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   public Inmigrant findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Inmigrant result;

      result = inmigrantRepository.findByUserAccountId(userAccount.getId());

      return result;
   }

   public Actor registerAsInmigrant(Inmigrant u) {
      Assert.notNull(u);
      u.getUserAccount().setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      u.getUserAccount().setPassword(hash);
      UserAccount userAccount = userAccountService.save(u.getUserAccount());
      u.setUserAccount(userAccount);
      Authority autoh = new Authority();
      autoh.setAuthority(Authority.INMIGRANT);
      Collection<Authority> authorities = new ArrayList<>();
      authorities.add(autoh);
      u.getUserAccount().setAuthorities(authorities);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Inmigrant resu = inmigrantRepository.save(u);

      Folder folder = folderService.create();
      folder.setFolderType(FolderType.PUBLIC);
      folder.setName("public");
      folder.setOwner(resu);
      folderService.save(folder);
      Folder folder1 = folderService.create();
      folder1.setFolderType(FolderType.INBOX);
      folder1.setOwner(resu);
      folder1.setName("inbox");
      folderService.save(folder1);
      return resu;
   }

   private void authoritySet(Inmigrant inmigrant){
      Assert.notNull(inmigrant);
      Authority autoh = new Authority();
      autoh.setAuthority(Authority.INMIGRANT);
      Collection<Authority> authorities = new ArrayList<>();
      authorities.add(autoh);
      UserAccount res1 = new UserAccount();
      res1.setAuthorities(authorities);
      UserAccount userAccount = userAccountService.save(res1);
      inmigrant.setUserAccount(res1);
      Assert.notNull(inmigrant.getUserAccount().getAuthorities(), "authorities null al registrar");
   }

   public Collection<Application> opennedAppsByImmigrantId(int inmigrantId) {
      Assert.notNull(inmigrantId);
      return inmigrantRepository.oppened(inmigrantId);
   }

   public Collection<Application> closedAndAcceptedAppsByInmigrantId(int inmigrantId) {
      Assert.notNull(inmigrantId);
      return inmigrantRepository.closedAndAccepted(inmigrantId);
   }

   public Collection<Application> closedAndDeniedAppsByInmigrantId(int inmigrantId) {
      Assert.notNull(inmigrantId);
      return inmigrantRepository.closedAndDenied(inmigrantId);
   }

   public Collection<Application> closedAndAwaitingAppsByInmigrantId(int inmigrantId) {
      Assert.notNull(inmigrantId);
      return inmigrantRepository.closedAndAwaiting(inmigrantId);
   }

}
