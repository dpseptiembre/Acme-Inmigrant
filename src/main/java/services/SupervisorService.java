/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Officer;
import domain.Supervisor;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SupervisorRepository;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.*;

@Service
@Transactional
public class SupervisorService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private SupervisorRepository supervisorRepository;
   @Autowired
   private OfficerService officerService;

   // Supporting services ----------------------------------------------------

   @Autowired
   private UserAccountService userAccountService;


   // Constructors -----------------------------------------------------------

   public SupervisorService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Collection<Supervisor> findAll() {
      Collection<Supervisor> result;

      result = supervisorRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Supervisor create(){
      return new Supervisor();
   }

   public Supervisor findOne(int supervisorId) {
      Assert.isTrue(supervisorId != 0);
      Supervisor result;
      result = supervisorRepository.findOne(supervisorId);
      Assert.notNull(result);

      return result;
   }

   public Supervisor save(Supervisor supervisor) {
      Assert.notNull(supervisor);

      Supervisor result;

      result = supervisorRepository.save(supervisor);

      return result;
   }

   public void delete(Supervisor supervisor) {
      Assert.notNull(supervisor);
      Assert.isTrue(supervisor.getId() != 0);
      Assert.isTrue(supervisorRepository.exists(supervisor.getId()));

      supervisorRepository.delete(supervisor);
   }

   // Other business methods -------------------------------------------------
   public Supervisor findByPrincipal() {
      Supervisor result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   public Supervisor findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Supervisor result;

      result = supervisorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }



   public List<Set<Officer>> officersPerMonthBeforRevisions(){

      List<Set<Officer>> result = new ArrayList<>();

      try {

         Date threeMonthsAhead = new DateTime().minusMonths(3).toDate();
         Date sixMonthsAhead = new DateTime().minusMonths(6).toDate();
         Date nineMonthsAhead = new DateTime().minusMonths(9).toDate();

         Set<Officer> officersLessThan3 =  supervisorRepository.officerWithLessThan(threeMonthsAhead);
         Set<Officer> officerWithMoreThan9 =  supervisorRepository.officerWithMoreThan(nineMonthsAhead);
         Set<Officer> officersBetween3and6 = supervisorRepository.officerBetewnThan(threeMonthsAhead,sixMonthsAhead);
         Set<Officer> officersBetween6and9 = supervisorRepository.officerBetewnThan(sixMonthsAhead,nineMonthsAhead);

         result.add(officersLessThan3);
         result.add(officerWithMoreThan9);
         result.add(officersBetween3and6);
         result.add(officersBetween6and9);

      } catch (Exception e) {
         e.printStackTrace();
      }

      return result;

   }


   public Collection<Officer> myOfficers(){

      Collection<Officer> result = new HashSet<>();

      try {
         result = supervisorRepository.myOfficers(this.findByPrincipal().getId());
      } catch (Exception e) {
         e.printStackTrace();
      }

      return result;

   }


   public void superviseOfficer(int officerId){

      try {
         Officer officer = officerService.findOne(officerId);
         officer.setUnderSupervision(true);
         officer.setSupervisor(this.findByPrincipal());
         this.findByPrincipal().getOfficersToSupervist().add(officer);
         officerService.save(officer);
         this.save(this.findByPrincipal());
      } catch (Exception e) {
         e.printStackTrace();
      }


   }
}
