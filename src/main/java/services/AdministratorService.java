/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import com.google.gson.Gson;
import domain.Administrator;
import domain.Application;
import domain.Mezzage;
import domain.Uttils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.*;

@Service
@Transactional
public class AdministratorService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private AdministratorRepository administratorRepository;

   // Supporting services ----------------------------------------------------

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private UttilsService uttilsService;



   // Constructors -----------------------------------------------------------

   public AdministratorService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Collection<Administrator> findAll() {
      Collection<Administrator> result;

      result = administratorRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Administrator findOne(int administratorId) {
      Assert.isTrue(administratorId != 0);
      Administrator result;
      result = administratorRepository.findOne(administratorId);
      Assert.notNull(result);

      return result;
   }

   public Administrator save(Administrator administrator) {
      Assert.notNull(administrator);

      Administrator result;

      result = administratorRepository.save(administrator);

      return result;
   }

   public void delete(Administrator administrator) {
      Assert.notNull(administrator);
      Assert.isTrue(administrator.getId() != 0);
      Assert.isTrue(administratorRepository.exists(administrator.getId()));

      administratorRepository.delete(administrator);
   }

   public Administrator findByPrincipal() {
      Administrator result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   public Administrator findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Administrator result;

      result = administratorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }

   // Other business methods -------------------------------------------------

   public UserAccount findUserAccount(Administrator administrator) {
      Assert.notNull(administrator);

      UserAccount result;

      result = userAccountService.findByAdministrator(administrator);

      return result;
   }

   public Uttils getUttils(){
      try {
         List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
         if (uttils.isEmpty()){
            throw new NoSuchElementException("The util list is empty");
         }else {
            Uttils first = uttils.get(0);
            return first;
         }

      } catch (Exception e) {
         e.printStackTrace();
         return null;
      }
   }
   public void addSpamWord(String word){

      try {
         if (getUttils().getSpamWords().contains(word)){
            throw new UnsupportedOperationException("The list contains selected word");
         }else {
            getUttils().getSpamWords().add(word);
            uttilsService.save(getUttils());
         }
      } catch (Exception e) {
         e.printStackTrace();
      }

   }
   public void deleteSpamWord(String word){

      try {
         if (!getUttils().getSpamWords().contains(word)){
            throw new UnsupportedOperationException("The list contains selected word");
         }else {
            getUttils().getSpamWords().remove(word);
            uttilsService.save(getUttils());
         }
      } catch (Exception e) {
         e.printStackTrace();
      }

   }

   public void changeSearchNumber(String num){

      try {
         Integer nume = new Integer(num);
         getUttils().setNumberOfFinderResults(nume);
         uttilsService.save(getUttils());
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public void setFinder(String num){

      try {
         Integer nume = new Integer(num);
         getUttils().setFinderCarche(nume);
         uttilsService.save(getUttils());
      } catch (Exception e) {
         e.printStackTrace();
      }
   }


   public void customize(String systemName, String banner, String welcome, String welcomeES){

      try {
         getUttils().setBanner(banner);
         getUttils().setSystemName(systemName);
         getUttils().setWelcomeMezzage(welcome);
         getUttils().setWelcomeMezzageES(welcomeES);
         uttilsService.save(getUttils());
      } catch (Exception e) {
         e.printStackTrace();
      }
   }


   public Set<Mezzage> spamMezzages(){

//      try {
//         DatabaseUtil databaseUtil = new DatabaseUtil();
//         databaseUtil.initialise();
//         EntityManager em = databaseUtil.getEntityManager();
//         databaseUtil.setReadUncommittedIsolationLevel();
//      } catch (InstantiationException e) {
//         e.printStackTrace();
//      } catch (IllegalAccessException e) {
//         e.printStackTrace();
//      } catch (ClassNotFoundException e) {
//         e.printStackTrace();
//      }


      return null;


   }



   public Double averageNumberOfApplicationsPerUser(){
      return administratorRepository.averageNumberOfApplicationsPerUser();
   }
   public int maxNumberOfApplicationPerUser(){
      return administratorRepository.maxNumberOfApplicationPerUser();
   }
   public int minNumberOfApplicationPerUser(){
      return administratorRepository.minNumberOfApplicationPerUser();
   }
   public Double standarDesviationOfApplicationPerUser(){
      return administratorRepository.standarDesviationOfApplicationPerUser();
   }
   public Double averageNumberOfApplicationsPerOfficer(){
      return administratorRepository.averageNumberOfApplicationsPerOfficer();
   }
   public int maxNumberOfApplicationsPerOfficer(){
      return administratorRepository.maxNumberOfApplicationsPerOfficer();
   }
   public int minNumberOfApplicationPerOfficer(){
      return administratorRepository.minNumberOfApplicationPerOfficer();
   }
   public Double standarDesviationOfApplicationPerOfficer(){
      return administratorRepository.standarDesviationOfApplicationPerOfficer();
   }
   public Double averageNumberofVisaPrices(){
      return administratorRepository.averageNumberofVisaPrices();
   }
   public int maxNumberOfVisaPrices(){
      return administratorRepository.maxNumberOfVisaPrices();
   }
   public int minNumberOfVisaPrices(){
      return administratorRepository.minNumberOfVisaPrices();
   }
   public Double standarDesviationOfVisaPrices(){
      return administratorRepository.standarDesviationOfVisaPrices();
   }
   public Double averageNumberOfInmigrantIvestigatedPerInvestigator(){
      return administratorRepository.averageNumberOfInmigrantIvestigatedPerInvestigator();
   }
   public int maxNumberOfInmigrantIvestigatedPerInvestigator(){
      return administratorRepository.maxNumberOfInmigrantIvestigatedPerInvestigator();
   }
   public int minNumberOfInmigrantIvestigatedPerInvestigator(){
      return administratorRepository.minNumberOfInmigrantIvestigatedPerInvestigator();
   }
   public Double standarDesviationOfNumberOfInmigrantIvestigatedPerInvestigator(){
      return administratorRepository.standarDesviationOfNumberOfInmigrantIvestigatedPerInvestigator();
   }
   public Double averageNumberOfVisaPerCategory(){
      return administratorRepository.averageNumberOfVisaPerCategory();
   }
   public int maxNumberOfVisaPerCategory(){
      return administratorRepository.maxNumberOfVisaPerCategory();
   }
   public int minNumberOfVisaPerCategory(){
      return administratorRepository.minNumberOfVisaPerCategory();
   }
   public Double standarDesviationNumberOfVisaPerCategory(){
      return administratorRepository.standarDesviationNumberOfVisaPerCategory();
   }
   public Double averageNumberOfLawsPerCountry(){
      return administratorRepository.averageNumberOfLawsPerCountry();
   }
   public int maxNumberOfLawsPerCountry(){
      return administratorRepository.maxNumberOfLawsPerCountry();
   }
   public int minNumberOfLawsPerCountry(){
      return administratorRepository.minNumberOfLawsPerCountry();
   }
   public Double standarDesviationOfLawsPerCountry(){
      return administratorRepository.standarDesviationOfLawsPerCountry();
   }
   public Double averageNumberOfRequirementsPerVisa(){
      return administratorRepository.averageNumberOfRequirementsPerVisa();
   }
   public int maxNumberOfRequirementsPerVisa(){
      return administratorRepository.maxNumberOfRequirementsPerVisa();
   }
   public int minNumberOfRequirementsPerVisa(){
      return administratorRepository.minNumberOfRequirementsPerVisa();
   }
   public Double standarDesviationOfRequirementsPerVisa(){
      return administratorRepository.standarDesviationOfRequirementsPerVisa();
   }
   public Double averageNumberOfMessagesPerFolder(){
      return administratorRepository.averageNumberOfMessagesPerFolder();
   }
   public int maxNumberOfMessagesPerFolder(){
      return administratorRepository.maxNumberOfMessagesPerFolder();
   }
   public int minNumberOfMessagesPerFolder(){
      return administratorRepository.minNumberOfMessagesPerFolder();
   }
   public Double standarDesviationOfMesasgesPerFolder(){
      return administratorRepository.standarDesviationOfMesasgesPerFolder();
   }
   public Double averageNumberOfMessagesPerActor(){
      return administratorRepository.averageNumberOfMessagesPerActor();
   }
   public int maxNumberOfMessagesPerActor(){
      return administratorRepository.maxNumberOfMessagesPerActor();
   }
   public int minNumberOfMessagesPerActor(){
      return administratorRepository.minNumberOfMessagesPerActor();
   }
   public Double standarDesviationOfMesasgesPerActor(){
      return administratorRepository.standarDesviationOfMesasgesPerActor();
   }
   public Double ratioOfSpamMezzages(){
      return new Double(administratorRepository.mezzagesWithSpam()/administratorRepository.totalMezzages());
   }

   //Chart
   public List<Long> elapsedTimes(){
      return administratorRepository.elapsedTimes();
   }
   public Double averageNumberOfElapsedTimes(){
      return administratorRepository.averageNumberOfElapsedTimes();
   }
   public int maxNumberOfElapsedTimes(){
      return administratorRepository.maxNumberOfElapsedTimes();
   }
   public int minNumberOfElapsedTimes(){
      return administratorRepository.minNumberOfElapsedTimes();
   }
   public Double standarDesviationNumberOfElapsedTime(){
      return administratorRepository.standarDesviationNumberOfElapsedTime();
   }
   public String createChart() {
      Gson gsonObj = new Gson();
      Map<Object, Object> map = null;
      List<Map<Object, Object>> list = new ArrayList<>();

      Set<Application> applications = new HashSet<>(applicationService.findAll());
      Integer pos = 1;
      for (Application ap : applications) {
         map = new HashMap<Object, Object>();
         map.put("x", pos);
         map.put("y", new Double(ap.getElapsedTime()));
         map.put("indexLabel", ap.getTicker());
         list.add(map);
         pos++;
      }


//      map = new HashMap<Object,Object>(); map.put("x", 20); map.put("y", 65); map.put("indexLabel", "elapsedTime"); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 30); map.put("y", 40); map.put("indexLabel", "elapsedTime"); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 40); map.put("y", 84); map.put("indexLabel", "elapsedTime"); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 50); map.put("y", 68); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 60); map.put("y", 64); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 70); map.put("y", 38); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 80); map.put("y", 71); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 90); map.put("y", 54); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 100); map.put("y", 60); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 110); map.put("y", 21); map.put("indexLabel", "averageNumberOfElapsedTimes"); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 120); map.put("y", 49); list.add(map);
//      map = new HashMap<Object,Object>(); map.put("x", 130); map.put("y", 41); list.add(map);


      String dataPoints = gsonObj.toJson(list);
      return dataPoints;
   }

}
