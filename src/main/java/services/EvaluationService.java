/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Evaluation;
import domain.Officer;
import domain.Supervisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EvaluationRepository;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class EvaluationService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private EvaluationRepository evaluationRepository;
   @Autowired
   private SupervisorService supervisorService;
   @Autowired
   private OfficerService officerService;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public EvaluationService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Evaluation create(int offcierId) {
      Evaluation res = new Evaluation();
      Officer receiver = officerService.findOne(offcierId);
      Supervisor owner = supervisorService.findByPrincipal();
      Date postDate = new Date(System.currentTimeMillis()-1000);
      res.setOwner(owner);
      res.setReceiver(receiver);
      res.setPostDate(postDate);
//      this.save(res);
      return res;

   }


   public Collection<Evaluation> findAll() {
      Collection<Evaluation> result;

      result = evaluationRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Evaluation findOne(int evaluationId) {
      Assert.isTrue(evaluationId != 0);
      Evaluation result;
      result = evaluationRepository.findOne(evaluationId);
      Assert.notNull(result);

      return result;
   }

   public Evaluation save(Evaluation evaluation) {
      Assert.notNull(evaluation);

      Evaluation result;

      result = evaluationRepository.save(evaluation);

      result.getReceiver().getEvaluations().add(result);
      result.getOwner().getEvaluations().add(result);

      officerService.save(result.getReceiver());
      supervisorService.save(result.getOwner());



      return result;
   }

   public void delete(Evaluation evaluation) {
      Assert.notNull(evaluation);
      Assert.isTrue(evaluation.getId() != 0);
      Assert.isTrue(evaluationRepository.exists(evaluation.getId()));

      evaluationRepository.delete(evaluation);
   }

   // Other business methods -------------------------------------------------


}
