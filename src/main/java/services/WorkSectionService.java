/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.WorkSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.WorkSectionRepository;

import java.util.Collection;

@Service
@Transactional
public class WorkSectionService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private WorkSectionRepository workSectionRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public WorkSectionService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public WorkSection create() {
      return new WorkSection();
   }

   public Collection<WorkSection> findAll() {
      Collection<WorkSection> result;

      result = workSectionRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public WorkSection findOne(int workSectionId) {
      Assert.isTrue(workSectionId != 0);
      WorkSection result;
      result = workSectionRepository.findOne(workSectionId);
      Assert.notNull(result);

      return result;
   }

   public WorkSection save(WorkSection workSection) {
      Assert.notNull(workSection);

      WorkSection result;

      result = workSectionRepository.save(workSection);

      return result;
   }

   public void delete(WorkSection workSection) {
      Assert.notNull(workSection);
      Assert.isTrue(workSection.getId() != 0);
      Assert.isTrue(workSectionRepository.exists(workSection.getId()));

      workSectionRepository.delete(workSection);
   }

   // Other business methods -------------------------------------------------


}
