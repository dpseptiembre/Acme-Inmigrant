/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CountryRepository;

import java.util.Collection;

@Service
@Transactional
public class CountryService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private CountryRepository countryRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public CountryService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public Country create() {
      return new Country();
   }

   public Collection<Country> findAll() {
      Collection<Country> result;

      result = countryRepository.notHiddenCountries();
      Assert.notNull(result);

      return result;
   }

   public Country findOne(int countryId) {
      Assert.isTrue(countryId != 0);
      Country result;
      result = countryRepository.findOne(countryId);
      Assert.notNull(result);

      return result;
   }

   public Country save(Country country) {
      Assert.notNull(country);

      Country result;

      result = countryRepository.save(country);

      return result;
   }

   public void delete(Country country) {
      Assert.notNull(country);
      Assert.isTrue(country.getId() != 0);
      Assert.isTrue(countryRepository.exists(country.getId()));

      countryRepository.delete(country);
   }

   // Other business methods -------------------------------------------------


}
