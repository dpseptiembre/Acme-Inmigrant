/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Law;
import domain.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.LawRepository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class LawService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private LawRepository lawRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public LawService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Law create(){
      Law law = new Law();
      Set<Requirement> requirements = new HashSet<Requirement>();
      Set<Law> laws = new HashSet<>();
      law.setHidden(false);
      law.setRequirements(requirements);
      law.setRelatedLaws(laws);
      return law;
   }

   public Collection<Law> findAll() {
      Collection<Law> result;

      result = lawRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Law findOne(int lawId) {
      Assert.isTrue(lawId != 0);
      Law result;
      result = lawRepository.findOne(lawId);
      Assert.notNull(result);

      return result;
   }

   public Law save(Law law) {
      Assert.notNull(law);

      Law result;

      result = lawRepository.save(law);

      return result;
   }

   public void delete(Law law) {
      Assert.notNull(law);
      Assert.isTrue(law.getId() != 0);
      Assert.isTrue(lawRepository.exists(law.getId()));

      lawRepository.delete(law);
   }

   // Other business methods -------------------------------------------------
   public Collection<Law> notHiddenLaws(){
      return lawRepository.notHiddenLaws();
   }

}
