/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.PersonalSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.PersonalSectionRepository;

import java.util.Collection;

@Service
@Transactional
public class PersonalSectionService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private PersonalSectionRepository personalSectionRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public PersonalSectionService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public PersonalSection create() {
      return new PersonalSection();
   }


   public Collection<PersonalSection> findAll() {
      Collection<PersonalSection> result;

      result = personalSectionRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public PersonalSection findOne(int personalSectionId) {
      Assert.isTrue(personalSectionId != 0);
      PersonalSection result;
      result = personalSectionRepository.findOne(personalSectionId);
      Assert.notNull(result);

      return result;
   }

   public PersonalSection save(PersonalSection personalSection) {
      Assert.notNull(personalSection);

      PersonalSection result;

      result = personalSectionRepository.save(personalSection);

      return result;
   }

   public void delete(PersonalSection personalSection) {
      Assert.notNull(personalSection);
      Assert.isTrue(personalSection.getId() != 0);
      Assert.isTrue(personalSectionRepository.exists(personalSection.getId()));

      personalSectionRepository.delete(personalSection);
   }

   // Other business methods -------------------------------------------------

   public void addName(String name, String id) {
      try {
         if ("".equals(name)) {
            throw new IllegalArgumentException("The name can not be empty / El nombre no puede estar vac�o");
         }
         Integer personalId = new Integer(id);
         PersonalSection personalSection = personalSectionRepository.findOne(personalId);
         personalSection.addName(name);
         personalSectionRepository.save(personalSection);
      } catch (NumberFormatException e) {
         e.printStackTrace();
      }

   }

}
