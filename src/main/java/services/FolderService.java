/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.FolderRepository;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class FolderService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private FolderRepository folderRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public FolderService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------


   public Folder create() {
      Folder res = new Folder();
      return res;
   }
   public Collection<Folder> findAll() {
      Collection<Folder> result;

      result = folderRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Folder findOne(int folderId) {
      Assert.isTrue(folderId != 0);
      Folder result;
      result = folderRepository.findOne(folderId);
      Assert.notNull(result);

      return result;
   }

   public Folder save(Folder folder) {
      Assert.notNull(folder);

      Folder result;

      result = folderRepository.save(folder);

      return result;
   }

   public void delete(Folder folder) {
      Assert.notNull(folder);
      Assert.isTrue(folder.getId() != 0);
      Assert.isTrue(folderRepository.exists(folder.getId()));

      folderRepository.delete(folder);
   }

   // Other business methods -------------------------------------------------


   public Collection<Folder> myFolders(Actor actor){
      Collection<Folder> result = new ArrayList<>();
      try {
         result = folderRepository.myFolders(actor.getId());
      } catch (Exception e) {
         e.printStackTrace();
      }

      return result;

   }

}
