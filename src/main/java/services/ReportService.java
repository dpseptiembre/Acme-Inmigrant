/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ReportRepository;

import java.util.Collection;

@Service
@Transactional
public class ReportService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private ReportRepository reportRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public ReportService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Report create() {
      Report res = new Report();
      return res;
   }

   public Collection<Report> findAll() {
      Collection<Report> result;

      result = reportRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Report findOne(int reportId) {
      Assert.isTrue(reportId != 0);
      Report result;
      result = reportRepository.findOne(reportId);
      Assert.notNull(result);

      return result;
   }

   public Report save(Report report) {
      Assert.notNull(report);

      Report result;

      result = reportRepository.save(report);

      return result;
   }

   public void delete(Report report) {
      Assert.notNull(report);
      Assert.isTrue(report.getId() != 0);
      Assert.isTrue(reportRepository.exists(report.getId()));

      reportRepository.delete(report);
   }

   // Other business methods -------------------------------------------------


   public void createAttachment(String reportId, String attachment){


      try {
         Assert.notNull(reportId);
         Integer id = new Integer(reportId);
         Report report = this.findOne(id);
         if(report.getPictures().contains(attachment)){
            throw new IllegalArgumentException("The attachment already exist ");
         }else{
            report.getPictures().add(attachment);
            this.save(report);
         }
      } catch (IllegalArgumentException e) {
         e.printStackTrace();
      }


   }


   public void removeAttachment(String reportId, String attachment){


      try {
         Assert.notNull(reportId);
         Integer id = new Integer(reportId);
         Report report = this.findOne(id);
         if(!report.getPictures().contains(attachment)){
            throw new IllegalArgumentException("The attachment already exist ");
         }else{
            report.getPictures().remove(attachment);
            this.save(report);
         }
      } catch (IllegalArgumentException e) {
         e.printStackTrace();
      }


   }

}
