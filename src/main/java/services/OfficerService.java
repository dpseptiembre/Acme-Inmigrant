/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.OfficerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class OfficerService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private OfficerRepository officerRepository;

   // Supporting services ----------------------------------------------------

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;


   // Constructors -----------------------------------------------------------

   public OfficerService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Officer create() {
      Officer res = new Officer();
      res.setUnderSupervision(false);
      authoritySet(res);
      return res;
   }

   public Collection<Officer> findAll() {
      Collection<Officer> result;

      result = officerRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Officer findOne(int officerId) {
      Assert.isTrue(officerId != 0);
      Officer result;
      result = officerRepository.findOne(officerId);
      Assert.notNull(result);

      return result;
   }

   public Officer save(Officer officer) {
      Assert.notNull(officer);

      Officer result;

      result = officerRepository.save(officer);

      return result;
   }

   public void delete(Officer officer) {
      Assert.notNull(officer);
      Assert.isTrue(officer.getId() != 0);
      Assert.isTrue(officerRepository.exists(officer.getId()));

      officerRepository.delete(officer);
   }

   // Other business methods -------------------------------------------------

   public UserAccount findUserAccount(Officer officer) {
      Assert.notNull(officer);

      UserAccount result;

      result = userAccountService.findByOfficer(officer);

      return result;
   }

   public Officer findByPrincipal() {
      Officer result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   public Officer findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Officer result;

      result = officerRepository.findByUserAccountId(userAccount.getId());

      return result;
   }


   public Actor registerAsOfficer(Officer u) {
      Assert.notNull(u);
      u.getUserAccount().setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      u.getUserAccount().setPassword(hash);
      UserAccount userAccount = userAccountService.save(u.getUserAccount());
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Officer resu = officerRepository.save(u);
      Folder folder = folderService.create();
      folder.setFolderType(FolderType.PUBLIC);
      folder.setName("public");
      folder.setOwner(resu);
      folderService.save(folder);
      Folder folder1 = folderService.create();
      folder1.setFolderType(FolderType.INBOX);
      folder1.setOwner(resu);
      folder1.setName("inbox");
      folderService.save(folder1);
      return resu;
   }

   private void authoritySet(Officer investigator){
      Assert.notNull(investigator);
      Authority autoh = new Authority();
      autoh.setAuthority(Authority.OFFICER);
      Collection<Authority> authorities = new ArrayList<>();
      authorities.add(autoh);
      UserAccount res1 = new UserAccount();
      res1.setAuthorities(authorities);
      //UserAccount userAccount = userAccountService.save(res1);
      investigator.setUserAccount(res1);
      Assert.notNull(investigator.getUserAccount().getAuthorities(), "authorities null al registrar");
   }

   public Collection<Application> acceptedApplicationsInWichImOfficer(int officerId) {
      return officerRepository.acceptedApplicationsInWichImOfficer(officerId);
   }

   public Collection<Application> rejectedApplicationsInWichImOfficer(int officerId) {
      return officerRepository.rejectedApplicationsInWichImOfficer(officerId);
   }

   public Collection<Application> pendingApplicationsInWichImOfficer(int officerId) {
      return officerRepository.pendingApplicationsInWichImOfficer(officerId);
   }
   public Collection<Report> reportOfApplicationThatIOffice(int officerId){
      return officerRepository.reportOfApplicationThatIOffice(officerId);
   }


}
