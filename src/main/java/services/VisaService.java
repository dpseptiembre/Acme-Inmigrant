/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Application;
import domain.Country;
import domain.Requirement;
import domain.Visa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.VisaRepository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class VisaService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private VisaRepository visaRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public VisaService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public Visa create() {
      Visa visa = new Visa();
      Set<Application> applications = new HashSet<Application>();
      Set<Requirement> requirements = new HashSet<Requirement>();
      visa.setApplications(applications);
      visa.setRequirements(requirements);
      return visa;
   }

   public Collection<Visa> findAll() {
      Collection<Visa> result;

      result = visaRepository.visasNotHidden();
      Assert.notNull(result);

      return result;
   }

   public Visa findOne(int visaId) {
      Assert.isTrue(visaId != 0);
      Visa result;
      result = visaRepository.findOne(visaId);
      Assert.notNull(result);

      return result;
   }

   public Visa save(Visa visa) {
      Assert.notNull(visa);

      Visa result;

      result = visaRepository.save(visa);

      return result;
   }

   public void delete(Visa visa) {
      Assert.notNull(visa);
      Assert.isTrue(visa.getId() != 0);
      Assert.isTrue(visaRepository.exists(visa.getId()));

      visaRepository.delete(visa);
   }

   // Other business methods -------------------------------------------------

   public Collection<Country> countriesFromVisa(Visa v) {
      return visaRepository.countriesFromVisa(v);
   }

   public Collection<Visa> visasFromCountry(int countryId) {
      return visaRepository.visasFromCountry(countryId);
   }

   public Collection<Visa> myVisas(int inmigrantId){return visaRepository.myVisas(inmigrantId);}
}

