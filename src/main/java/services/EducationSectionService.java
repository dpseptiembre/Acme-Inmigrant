/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.EducationSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EducationSectionRepository;

import java.util.Collection;

@Service
@Transactional
public class EducationSectionService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private EducationSectionRepository educationSectionRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public EducationSectionService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public EducationSection create() {
      return new EducationSection();
   }

   public Collection<EducationSection> findAll() {
      Collection<EducationSection> result;

      result = educationSectionRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public EducationSection findOne(int educationSectionId) {
      Assert.isTrue(educationSectionId != 0);
      EducationSection result;
      result = educationSectionRepository.findOne(educationSectionId);
      Assert.notNull(result);

      return result;
   }

   public EducationSection save(EducationSection educationSection) {
      Assert.notNull(educationSection);

      EducationSection result;

      result = educationSectionRepository.save(educationSection);

      return result;
   }

   public void delete(EducationSection educationSection) {
      Assert.notNull(educationSection);
      Assert.isTrue(educationSection.getId() != 0);
      Assert.isTrue(educationSectionRepository.exists(educationSection.getId()));

      educationSectionRepository.delete(educationSection);
   }

   // Other business methods -------------------------------------------------


}
