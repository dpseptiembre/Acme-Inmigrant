/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.MezzageRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class MezzageService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private MezzageRepository mezzageRepository;
   @Autowired
   private ActorService actorService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private FinderMessageService finderMessageService;

   // Supporting services ----------------------------------------------------
   // Constructors -----------------------------------------------------------

   public MezzageService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Mezzage create() {
      Mezzage res = new Mezzage();
      res.setSenderEmail(actorService.findByPrincipal().getEmail());
      res.setSender(actorService.findByPrincipal());
      res.setSendDate(new Date(System.currentTimeMillis()-100));
      res.setSpam(false);
      return res;
   }

   public Collection<Mezzage> findAll() {
      Collection<Mezzage> result;

      result = mezzageRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Collection<Mezzage> findAllPublic() {
      Collection<Mezzage> result;

      result = mezzageRepository.publicMezzages();
      Assert.notNull(result);

      return result;
   }

   public Mezzage findOne(int mezzageId) {
      Assert.isTrue(mezzageId != 0);
      Mezzage result;
      result = mezzageRepository.findOne(mezzageId);
      Assert.notNull(result);

      return result;
   }

   public Mezzage save(Mezzage mezzage) {
      Assert.notNull(mezzage);

      Mezzage result;

      result = mezzageRepository.save(mezzage);

      return result;
   }

   public void delete(Mezzage mezzage) {
      Assert.notNull(mezzage);
      Assert.isTrue(mezzage.getId() != 0);
      Assert.isTrue(mezzageRepository.exists(mezzage.getId()));

      mezzageRepository.delete(mezzage);
   }

   // Other business methods -------------------------------------------------


   public List<Mezzage> finder(String keyword, String startDate, String endDate){
      List<Mezzage> mezzages = new ArrayList<>();
      List<Mezzage> finalresults = new ArrayList<>();
      try {
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
         Date startDateD = format.parse(startDate);
         Date endDateD = format.parse(endDate);
         mezzages = mezzageRepository.sarchedMezzages(keyword, startDateD, endDateD);

         if(mezzages.size() >= administratorService.getUttils().getNumberOfFinderResults()){
           finalresults = mezzages.subList(0,administratorService.getUttils().getNumberOfFinderResults());
         }else{
            finalresults = mezzages;
         }



         //Save finder
         FinderMessage finderMessage = finderMessageService.create();
         finderMessage.setKeyword(keyword);
         finderMessage.setStartDate(startDateD);
         finderMessage.setEndDate(endDateD);
         finderMessage.setOwner(actorService.findByPrincipal());
         finderMessage.setResults(finalresults);
         finderMessage.setCreationMoment(new Date(System.currentTimeMillis()-100));
         FinderMessage fm = finderMessageService.save(finderMessage);
         actorService.findByPrincipal().getFinderMessages().add(fm);

         return finalresults;
      } catch (ParseException e) {
         e.printStackTrace();
         return null;
      }


   }

   public Collection<Mezzage> publicFoldersWithSpamMessages() {


      Set<Mezzage> res = new HashSet<>();

      Collection<String> spamWords = administratorService.getUttils().getSpamWords();
      for (String s : spamWords) {
         res.addAll(mezzageRepository.spamMezzage(s));
      }


      return res;
   }


   public void createActionMessage(String body, String subject,int receiverActorId){

      try {
         Actor receiver = actorService.findOne(receiverActorId);
         Collection<Folder> folderToAchieve = mezzageRepository.getFolderByOwner(receiverActorId);
         Folder publicf = null;
         for (Folder f : folderToAchieve){
            if (f.getFolderType().equals(FolderType.PUBLIC)){
               publicf = f;
               break;
            }
         }
         Mezzage mezzage = this.create();
         mezzage.setBody(body);
         mezzage.setSubject(subject);
         mezzage.setFolder(publicf);
         mezzage.setReceiver(receiver);
         mezzage.setSenderEmail("no.replay@acme-inmigrant.com");
         mezzage.setReceiverEmail(receiver.getEmail());
         Mezzage saved = this.save(mezzage);
         publicf.getMezzages().add(saved);
         folderService.save(publicf);
      } catch (Exception e) {
         e.printStackTrace();
      }

   }


   public Actor getActorByEmail(String email){
      Actor res = null;
      try {
         Assert.notNull(email);
         List<Actor> actors = mezzageRepository.getActorByEmail(email);
         if(actors.isEmpty()){
            throw new IllegalArgumentException("We can't find the destination email");
         }else{
            res = actors.get(0);
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   return res;
   }

}
