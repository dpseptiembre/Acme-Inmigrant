/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;


import domain.Application;
import domain.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ApplicationRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.lang.StrictMath.abs;

@Service
@Transactional
public class ApplicationService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private ApplicationRepository applicationRepository;
   @Autowired
   private CountryService countryService;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public ApplicationService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Application create() {
      Application res = new Application();
      return res;
   }
   public Collection<Application> findAll() {
      Collection<Application> result;

      result = applicationRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Application findOne(int applicationId) {
      Assert.isTrue(applicationId != 0);
      Application result;
      result = applicationRepository.findOne(applicationId);
      Assert.notNull(result);

      return result;
   }

   public Application save(Application application) {
      Assert.notNull(application);

      Application result;

      result = applicationRepository.save(application);

      return result;
   }

   public void delete(Application application) {
      Assert.notNull(application);
      Assert.isTrue(application.getId() != 0);
      Assert.isTrue(applicationRepository.exists(application.getId()));

      applicationRepository.delete(application);
   }

   // Other business methods -------------------------------------------------

   public void linkTwoApplications(String ticker, String id) {

      try {
         Integer appId = new Integer(id);
         Application applicationToLink = applicationRepository.getApplicationByTicker(ticker);
         Application applicationLinked = this.findOne(appId);

         if (applicationLinked.getLinkedApplications().contains(applicationToLink)) {
            throw new UnsupportedOperationException();
         } else {
            applicationLinked.getLinkedApplications().add(applicationToLink);
            applicationToLink.getLinkedApplications().add(applicationLinked);
            this.save(applicationLinked);
            this.save(applicationToLink);
         }

      } catch (NumberFormatException e) {
         e.printStackTrace();
      }


   }
   public void flush() {
      applicationRepository.flush();
   }

   public Application getApplicationByTicker(String ticker) {
      return applicationRepository.getApplicationByTicker(ticker);
   }

   public Collection<Application> applicationswithoutOfficer() {
      return applicationRepository.applicationswithoutOfficer();
   }
   public Collection<Application> applicationsFromCountry(int countryId){
      return applicationRepository.applicationsFromCountry(countryId);
   }
   public void linkApplicationsfFromSameCountry(Application application){
      Country country = application.getVisa().getCountry();
      Set<Application> applicationsFromCountry = new HashSet<>(applicationRepository.applicationsFromCountry(country.getId()));
      for (Application a : applicationsFromCountry){
         if(!application.getLinkedApplications().contains(a)){
            application.getLinkedApplications().add(a);
            applicationRepository.save(a);
            a.getLinkedApplications().add(application);
            applicationRepository.save(application);
         }
      }

   }



   public Long estimatedElapsedTime(Application a){
      try {
         SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
         if (a.getCloseDate() == null || a.getStatusChange() == null) {
            return 0L;
         }

         String inputString1 = myFormat.format(a.getCloseDate());
         String inputString2 = myFormat.format(a.getStatusChange());

         Date date1 = myFormat.parse(inputString1);
         Date date2 = myFormat.parse(inputString2);
         long diff = date2.getTime() - date1.getTime();
         Long days = abs(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
         return days;
      } catch (ParseException e) {
         e.printStackTrace();
         return 0L;
      }

   }



}
