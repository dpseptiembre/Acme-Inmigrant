/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.SocialSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SocialSectionRepository;

import java.util.Collection;

@Service
@Transactional
public class SocialSectionService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private SocialSectionRepository socialSectionRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public SocialSectionService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------
   public SocialSection create() {
      return new SocialSection();
   }

   public Collection<SocialSection> findAll() {
      Collection<SocialSection> result;

      result = socialSectionRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public SocialSection findOne(int socialSectionId) {
      Assert.isTrue(socialSectionId != 0);
      SocialSection result;
      result = socialSectionRepository.findOne(socialSectionId);
      Assert.notNull(result);

      return result;
   }

   public SocialSection save(SocialSection socialSection) {
      Assert.notNull(socialSection);

      SocialSection result;

      result = socialSectionRepository.save(socialSection);

      return result;
   }

   public void delete(SocialSection socialSection) {
      Assert.notNull(socialSection);
      Assert.isTrue(socialSection.getId() != 0);
      Assert.isTrue(socialSectionRepository.exists(socialSection.getId()));

      socialSectionRepository.delete(socialSection);
   }

   // Other business methods -------------------------------------------------


}
