/*
 * ActorService.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.InvestigatorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class InvestigatorService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private InvestigatorRepository investigatorRepository;

   // Supporting services ----------------------------------------------------

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;


   // Constructors -----------------------------------------------------------

   public InvestigatorService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Investigator create() {
      Investigator res = new Investigator();
      authoritySet(res);
      return res;
   }

   public Collection<Investigator> findAll() {
      Collection<Investigator> result;

      result = investigatorRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Investigator findOne(int investigatorId) {
      Assert.isTrue(investigatorId != 0);
      Investigator result;
      result = investigatorRepository.findOne(investigatorId);
      Assert.notNull(result);

      return result;
   }

   public Investigator save(Investigator investigator) {
      Assert.notNull(investigator);

      Investigator result;

      result = investigatorRepository.save(investigator);

      return result;
   }

   public void delete(Investigator investigator) {
      Assert.notNull(investigator);
      Assert.isTrue(investigator.getId() != 0);
      Assert.isTrue(investigatorRepository.exists(investigator.getId()));

      investigatorRepository.delete(investigator);
   }

   // Other business methods -------------------------------------------------

   public UserAccount findUserAccount(Investigator investigator) {
      Assert.notNull(investigator);

      UserAccount result;

      result = userAccountService.findByInvestigator(investigator);

      return result;
   }

   public Investigator findByPrincipal() {
      Investigator result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   public Investigator findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Investigator result;

      result = investigatorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }


   public Actor registerAsInvestigator(Investigator u) {
      Assert.notNull(u);
      u.getUserAccount().setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      u.getUserAccount().setPassword(hash);
      UserAccount userAccount = userAccountService.save(u.getUserAccount());
      u.setUserAccount(userAccount);
      Authority autoh = new Authority();
      autoh.setAuthority(Authority.INVESTIGATOR);
      Collection<Authority> authorities = new ArrayList<>();
      authorities.add(autoh);
      u.getUserAccount().setAuthorities(authorities);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Investigator resu = investigatorRepository.save(u);
      Folder folder = folderService.create();
      folder.setFolderType(FolderType.PUBLIC);
      folder.setName("public");
      folder.setOwner(resu);
      folderService.save(folder);
      Folder folder1 = folderService.create();
      folder1.setFolderType(FolderType.INBOX);
      folder1.setOwner(resu);
      folder1.setName("inbox");
      folderService.save(folder1);
      return resu;
   }

   private void authoritySet(Investigator investigator){
      Assert.notNull(investigator);
      Authority autoh = new Authority();
      autoh.setAuthority(Authority.INVESTIGATOR);
      Collection<Authority> authorities = new ArrayList<>();
      authorities.add(autoh);
      UserAccount res1 = new UserAccount();
      res1.setAuthorities(authorities);
      //UserAccount userAccount = userAccountService.save(res1);
      investigator.setUserAccount(res1);
      Assert.notNull(investigator.getUserAccount().getAuthorities(), "authorities null al registrar");
   }
   public Collection<Inmigrant> underInvestigation(){
      return investigatorRepository.underInvestigation(findByPrincipal().getId());
   }
}
