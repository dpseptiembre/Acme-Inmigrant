/*
 * Authority.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package security;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Collection;

@Embeddable
@Access(AccessType.PROPERTY)
public class Authority implements GrantedAuthority {

	// Constructors -----------------------------------------------------------

	private static final long	serialVersionUID	= 1L;


	public Authority() {
		super();
	}


	// Values -----------------------------------------------------------------

   public static final String ADMINISTRATOR = "ADMINISTRATOR";
   public static final String INMIGRANT = "INMIGRANT";
   public static final String INVESTIGATOR = "INVESTIGATOR";
   public static final String OFFICER = "OFFICER";
   public static final String SUPERVISOR = "SUPERVISOR";
	// Attributes -------------------------------------------------------------

	private String				authority;

	public static Collection<Authority> listAuthorities() {
		Collection<Authority> result;
		Authority authority;

		result = new ArrayList<Authority>();

		authority = new Authority();
      authority.setAuthority(Authority.ADMINISTRATOR);
		result.add(authority);

		authority = new Authority();
      authority.setAuthority(Authority.INMIGRANT);
		result.add(authority);

      authority = new Authority();
      authority.setAuthority(Authority.INVESTIGATOR);
      result.add(authority);

      authority = new Authority();
      authority.setAuthority(Authority.OFFICER);
      result.add(authority);

		authority = new Authority();
		authority.setAuthority(Authority.SUPERVISOR);
		result.add(authority);
		return result;
	}

   public void setAuthority(final String authority) {
      this.authority = authority;
   }

   @NotBlank
   @Pattern(regexp = "^" + Authority.ADMINISTRATOR + "|" + Authority.INMIGRANT + "|" + Authority.INVESTIGATOR + "|" + Authority.OFFICER + "|" + Authority.SUPERVISOR + "$")
   @Override
   public String getAuthority() {
      return this.authority;
   }

	// Equality ---------------------------------------------------------------

	@Override
	public int hashCode() {
		return this.getAuthority().hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		boolean result;

		if (this == other) {
			result = true;
		} else if (other == null) {
			result = false;
		} else if (!this.getClass().isInstance(other)) {
			result = false;
		} else {
			result = (this.getAuthority().equals(((Authority) other).getAuthority()));
		}

		return result;
	}

}
