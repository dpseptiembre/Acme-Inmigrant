/*
 * UserAccountService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package security;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Transactional
public class UserAccountService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private UserAccountRepository	userAccountRepository;


	// Supporting services ----------------------------------------------------

	// Constructors -----------------------------------------------------------

	public UserAccountService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------
   public UserAccount save(UserAccount userAccount) {
      Assert.notNull(userAccount);
      return userAccountRepository.save(userAccount);
   }

	public UserAccount findByActor(final Actor actor) {
		Assert.notNull(actor);

		UserAccount result;

		result = this.userAccountRepository.findByActorId(actor.getId());

		return result;
	}

   public UserAccount findByAdministrator(final Administrator administrator) {
      Assert.notNull(administrator);

      UserAccount result;

      result = this.userAccountRepository.findByAdministratorId(administrator.getId());

      return result;
   }


   public UserAccount findByInmigrant(final Inmigrant inmigrant) {
      Assert.notNull(inmigrant);

      UserAccount result;

      result = this.userAccountRepository.findByInmigrantId(inmigrant.getId());

      return result;
   }

   public UserAccount findByInvestigator(final Investigator investigator) {
      Assert.notNull(investigator);

      UserAccount result;

      result = this.userAccountRepository.findByInvestigatorId(investigator.getId());

      return result;
   }

   public UserAccount findByOfficer(final Officer officer) {
      Assert.notNull(officer);

      UserAccount result;

      result = this.userAccountRepository.findByOfficerId(officer.getId());

      return result;
   }

   public UserAccount findBySupervisor(final Supervisor supervisor) {
      Assert.notNull(supervisor);

      UserAccount result;

      result = this.userAccountRepository.findByInmigrantId(supervisor.getId());

      return result;
   }
	// Other business methods -------------------------------------------------

}
