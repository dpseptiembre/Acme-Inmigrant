DROP DATABASE IF EXISTS `Acme-Inmigrant`;
CREATE DATABASE `Acme-Inmigrant`;
DROP USER 'acme-user'@'%';
DROP USER 'acme-manager'@'%';
CREATE USER 'acme-user'@'%'
  IDENTIFIED BY PASSWORD '*4F10007AADA9EE3DBB2CC36575DFC6F4FDE27577';
CREATE USER 'acme-manager'@'%'
  IDENTIFIED BY PASSWORD '*FDB8CD304EB2317D10C95D797A4BD7492560F55F';
GRANT SELECT, INSERT, UPDATE, DELETE
ON `Acme-Inmigrant`.* TO 'acme-user'@'%';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER,
CREATE TEMPORARY TABLES, LOCK TABLES, CREATE VIEW, CREATE ROUTINE,
ALTER ROUTINE, EXECUTE, TRIGGER, SHOW VIEW
ON `Acme-Inmigrant`.* TO 'acme-manager'@'%';





# Volcado de tabla Actor_Finder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Actor_Finder`;

CREATE TABLE `Actor_Finder` (
  `Actor_id` int(11) NOT NULL,
  `finders_id` int(11) NOT NULL,
  UNIQUE KEY `UK_l5y66isjfammndhk9x5s4ejv2` (`finders_id`),
  CONSTRAINT `FK_l5y66isjfammndhk9x5s4ejv2` FOREIGN KEY (`finders_id`) REFERENCES `Finder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Volcado de tabla Administrator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Administrator`;

CREATE TABLE `Administrator` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_idt4b4u259p6vs4pyr9lax4eg` (`userAccount_id`),
  CONSTRAINT `FK_idt4b4u259p6vs4pyr9lax4eg` FOREIGN KEY (`userAccount_id`) REFERENCES `UserAccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Administrator` WRITE;
/*!40000 ALTER TABLE `Administrator` DISABLE KEYS */;

INSERT INTO `Administrator` (`id`, `version`, `address`, `email`, `name`, `phone`, `surname`, `userAccount_id`)
VALUES
	(1143,0,'Address 1','adminitrator1@mail.com','Administrator 1','+34 (555) 4354352001','admin1Surname',1131),
	(1144,0,'Address 2','adminitrator2@mail.com','Administrator 2','+34 (555) 4354352002','admin2Surname',1132);

/*!40000 ALTER TABLE `Administrator` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Application`;

CREATE TABLE `Application` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `closeDate` datetime DEFAULT NULL,
  `closed` bit(1) DEFAULT NULL,
  `elapsedTime` bigint(20) DEFAULT NULL,
  `openDate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusChange` datetime DEFAULT NULL,
  `statusComment` varchar(255) DEFAULT NULL,
  `ticker` varchar(255) DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `officer_id` int(11) DEFAULT NULL,
  `personalSection_id` int(11) DEFAULT NULL,
  `visa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5ho5khuj5b6xfls5l7h5969xo` (`applicant_id`),
  KEY `FK_k8kn42d2a9geayum9vvcqnhpg` (`officer_id`),
  KEY `FK_ljcrqvs9rm9fr4c9122g1xdwy` (`personalSection_id`),
  KEY `FK_87kxbeua8uijovhovrose2l98` (`visa_id`),
  CONSTRAINT `FK_87kxbeua8uijovhovrose2l98` FOREIGN KEY (`visa_id`) REFERENCES `Visa` (`id`),
  CONSTRAINT `FK_5ho5khuj5b6xfls5l7h5969xo` FOREIGN KEY (`applicant_id`) REFERENCES `Inmigrant` (`id`),
  CONSTRAINT `FK_k8kn42d2a9geayum9vvcqnhpg` FOREIGN KEY (`officer_id`) REFERENCES `Officer` (`id`),
  CONSTRAINT `FK_ljcrqvs9rm9fr4c9122g1xdwy` FOREIGN KEY (`personalSection_id`) REFERENCES `PersonalSection` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Application` WRITE;
/*!40000 ALTER TABLE `Application` DISABLE KEYS */;

INSERT INTO `Application` (`id`, `version`, `closeDate`, `closed`, `elapsedTime`, `openDate`, `status`, `statusChange`, `statusComment`, `ticker`, `applicant_id`, `officer_id`, `personalSection_id`, `visa_id`)
VALUES
	(1220,1,'2018-10-20 20:23:21',b'0',123,'2017-09-18 20:23:21',0,'2018-05-20 20:23:21','It\'s been accepted','180920-LRXW29',1145,NULL,1224,1206),
	(1221,1,'2018-09-20 20:23:21',b'1',92,'2017-09-20 20:23:21',1,'2018-05-20 20:23:21','It\'s been rejected','200920-SDFA29',1147,1154,1225,1206),
	(1222,1,NULL,b'0',0,'2018-01-14 20:23:21',2,NULL,NULL,'180110-PRTY14',1145,NULL,1226,1209),
	(1223,1,NULL,b'0',0,'2018-07-14 20:23:21',2,NULL,NULL,'180714-PTRE89',1145,NULL,1226,1209);

/*!40000 ALTER TABLE `Application` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Application_Application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Application_Application`;

CREATE TABLE `Application_Application` (
  `Application_id` int(11) NOT NULL,
  `linkedApplications_id` int(11) NOT NULL,
  KEY `FK_oep8nqis5vo4tqyics798p6si` (`linkedApplications_id`),
  KEY `FK_1y58f51hd3yjt4bwgjtt62ng6` (`Application_id`),
  CONSTRAINT `FK_1y58f51hd3yjt4bwgjtt62ng6` FOREIGN KEY (`Application_id`) REFERENCES `Application` (`id`),
  CONSTRAINT `FK_oep8nqis5vo4tqyics798p6si` FOREIGN KEY (`linkedApplications_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Application_Application` WRITE;
/*!40000 ALTER TABLE `Application_Application` DISABLE KEYS */;

INSERT INTO `Application_Application` (`Application_id`, `linkedApplications_id`)
VALUES
	(1220,1222),
	(1222,1220);

/*!40000 ALTER TABLE `Application_Application` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Category`;

CREATE TABLE `Category` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `hidden` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_k8dpq6k9hyb58e8ma51c57cto` (`father_id`),
  CONSTRAINT `FK_k8dpq6k9hyb58e8ma51c57cto` FOREIGN KEY (`father_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;

INSERT INTO `Category` (`id`, `version`, `description`, `hidden`, `name`, `father_id`)
VALUES
	(1211,0,'category 0',b'0','category 0',1211),
	(1212,0,'category 1',b'0','category 1',1211),
	(1213,0,'category 2',b'0','category 2',1211),
	(1214,0,'category 3',b'0','category 3',1211),
	(1215,0,'category 4',b'0','category 4',1212),
	(1216,0,'category 5',b'0','category 5',1212),
	(1217,0,'category 6',b'0','category 6',1213);

/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Category_Category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Category_Category`;

CREATE TABLE `Category_Category` (
  `Category_id` int(11) NOT NULL,
  `sons_id` int(11) NOT NULL,
  UNIQUE KEY `UK_4piitutjpwlh6gi749e57lfr4` (`sons_id`),
  KEY `FK_90tievw3x18jv9f0n4ma0ethu` (`Category_id`),
  CONSTRAINT `FK_90tievw3x18jv9f0n4ma0ethu` FOREIGN KEY (`Category_id`) REFERENCES `Category` (`id`),
  CONSTRAINT `FK_4piitutjpwlh6gi749e57lfr4` FOREIGN KEY (`sons_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Category_Category` WRITE;
/*!40000 ALTER TABLE `Category_Category` DISABLE KEYS */;

INSERT INTO `Category_Category` (`Category_id`, `sons_id`)
VALUES
	(1211,1212),
	(1211,1213),
	(1211,1214),
	(1212,1215),
	(1212,1216),
	(1213,1217);

/*!40000 ALTER TABLE `Category_Category` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Category_Visa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Category_Visa`;

CREATE TABLE `Category_Visa` (
  `Category_id` int(11) NOT NULL,
  `visas_id` int(11) NOT NULL,
  UNIQUE KEY `UK_orgckaiynkiv2q58lhybg3rqe` (`visas_id`),
  KEY `FK_tosoo1cc2x3s5x3li2r5wgogy` (`Category_id`),
  CONSTRAINT `FK_tosoo1cc2x3s5x3li2r5wgogy` FOREIGN KEY (`Category_id`) REFERENCES `Category` (`id`),
  CONSTRAINT `FK_orgckaiynkiv2q58lhybg3rqe` FOREIGN KEY (`visas_id`) REFERENCES `Visa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Category_Visa` WRITE;
/*!40000 ALTER TABLE `Category_Visa` DISABLE KEYS */;

INSERT INTO `Category_Visa` (`Category_id`, `visas_id`)
VALUES
	(1212,1206),
	(1212,1207),
	(1214,1208),
	(1215,1209),
	(1215,1210);

/*!40000 ALTER TABLE `Category_Visa` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla ContactSection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ContactSection`;

CREATE TABLE `ContactSection` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pageNumber` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_168bq3jp4a68wiaxpn0q1ayhg` (`application_id`),
  CONSTRAINT `FK_168bq3jp4a68wiaxpn0q1ayhg` FOREIGN KEY (`application_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `ContactSection` WRITE;
/*!40000 ALTER TABLE `ContactSection` DISABLE KEYS */;

INSERT INTO `ContactSection` (`id`, `version`, `email`, `pageNumber`, `phoneNumber`, `application_id`)
VALUES
	(1227,0,'email1@mail.com','1','+34 4354352003',1220),
	(1228,0,'email1@mail.com','1','+34 4354352003',1220),
	(1229,0,'email3@mail.com','4','+34 4354444003',1222);

/*!40000 ALTER TABLE `ContactSection` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Country`;

CREATE TABLE `Country` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `isoCode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `wikiLink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Country` WRITE;
/*!40000 ALTER TABLE `Country` DISABLE KEYS */;

INSERT INTO `Country` (`id`, `version`, `flag`, `isoCode`, `name`, `wikiLink`)
VALUES
	(1204,0,'https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Flag_of_Spain.svg/1280px-Flag_of_Spain.svg.png','ES','Spain','https://en.wikipedia.org/wiki/Spain'),
	(1205,0,'http://www.argirakio.gr/drapeau_en.png','GB','England','https://en.wikipedia.org/wiki/England');

/*!40000 ALTER TABLE `Country` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Country_Law
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Country_Law`;

CREATE TABLE `Country_Law` (
  `Country_id` int(11) NOT NULL,
  `legislation_id` int(11) NOT NULL,
  UNIQUE KEY `UK_9715j2y9jqepr08gnt8khtoy6` (`legislation_id`),
  KEY `FK_c4sgsbinivp4pdn3m1m6j49il` (`Country_id`),
  CONSTRAINT `FK_c4sgsbinivp4pdn3m1m6j49il` FOREIGN KEY (`Country_id`) REFERENCES `Country` (`id`),
  CONSTRAINT `FK_9715j2y9jqepr08gnt8khtoy6` FOREIGN KEY (`legislation_id`) REFERENCES `Law` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Country_Law` WRITE;
/*!40000 ALTER TABLE `Country_Law` DISABLE KEYS */;

INSERT INTO `Country_Law` (`Country_id`, `legislation_id`)
VALUES
	(1204,1244),
	(1204,1245),
	(1204,1246),
	(1204,1247),
	(1205,1248),
	(1205,1249);

/*!40000 ALTER TABLE `Country_Law` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Country_Visa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Country_Visa`;

CREATE TABLE `Country_Visa` (
  `Country_id` int(11) NOT NULL,
  `offeredVisas_id` int(11) NOT NULL,
  UNIQUE KEY `UK_rgm3jhqsp8chvvneo6mfbcgu5` (`offeredVisas_id`),
  KEY `FK_irslb0lexd192hq1j6w0ybd06` (`Country_id`),
  CONSTRAINT `FK_irslb0lexd192hq1j6w0ybd06` FOREIGN KEY (`Country_id`) REFERENCES `Country` (`id`),
  CONSTRAINT `FK_rgm3jhqsp8chvvneo6mfbcgu5` FOREIGN KEY (`offeredVisas_id`) REFERENCES `Visa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Country_Visa` WRITE;
/*!40000 ALTER TABLE `Country_Visa` DISABLE KEYS */;

INSERT INTO `Country_Visa` (`Country_id`, `offeredVisas_id`)
VALUES
	(1204,1206),
	(1204,1207),
	(1204,1208),
	(1205,1209),
	(1205,1210);

/*!40000 ALTER TABLE `Country_Visa` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla CreditCard
# ------------------------------------------------------------

DROP TABLE IF EXISTS `CreditCard`;

CREATE TABLE `CreditCard` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `CVV` varchar(255) DEFAULT NULL,
  `holderName` varchar(255) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `valid` bit(1) NOT NULL,
  `year` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1m4tt0bmhib6hwf8edgt5c43b` (`owner_id`),
  CONSTRAINT `FK_1m4tt0bmhib6hwf8edgt5c43b` FOREIGN KEY (`owner_id`) REFERENCES `Inmigrant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `CreditCard` WRITE;
/*!40000 ALTER TABLE `CreditCard` DISABLE KEYS */;

INSERT INTO `CreditCard` (`id`, `version`, `CVV`, `holderName`, `month`, `number`, `type`, `valid`, `year`, `owner_id`)
VALUES
	(1146,1,'432','Inmigrant 1 ',10,'54275498043695577',0,b'1',2018,1145),
	(1148,1,'432','Inmigrant 2 ',11,'54275498043695577',1,b'1',2018,1147),
	(1151,1,'432','Inmigrant 4 ',12,'54275498043695577',0,b'1',2018,1150);

/*!40000 ALTER TABLE `CreditCard` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla EducationSection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `EducationSection`;

CREATE TABLE `EducationSection` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `awardedDate` date DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `institution` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9e25ebfx5mbdn8g81304cnewq` (`application_id`),
  CONSTRAINT `FK_9e25ebfx5mbdn8g81304cnewq` FOREIGN KEY (`application_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `EducationSection` WRITE;
/*!40000 ALTER TABLE `EducationSection` DISABLE KEYS */;

INSERT INTO `EducationSection` (`id`, `version`, `awardedDate`, `degree`, `institution`, `level`, `application_id`)
VALUES
	(1234,0,'2016-09-23','UNIVERSITY DEGREE','institution1',6,1220),
	(1235,0,'2016-09-23','degree 1','MASTER',7,1220),
	(1236,0,'2016-09-23','degree 1','BACHERLOR',5,1220),
	(1237,0,'2016-09-23','BACHERLOR','institution1',5,1221),
	(1238,0,'2016-09-23','BACHERLOR','institution5',5,1222);

/*!40000 ALTER TABLE `EducationSection` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Evaluation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Evaluation`;

CREATE TABLE `Evaluation` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  `postDate` date DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gwyaib0sty4p7im5k9avce0af` (`owner_id`),
  KEY `FK_fil1rf66963v262gxpy0e0iao` (`receiver_id`),
  CONSTRAINT `FK_fil1rf66963v262gxpy0e0iao` FOREIGN KEY (`receiver_id`) REFERENCES `Officer` (`id`),
  CONSTRAINT `FK_gwyaib0sty4p7im5k9avce0af` FOREIGN KEY (`owner_id`) REFERENCES `Supervisor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Evaluation` WRITE;
/*!40000 ALTER TABLE `Evaluation` DISABLE KEYS */;

INSERT INTO `Evaluation` (`id`, `version`, `description`, `mark`, `postDate`, `owner_id`, `receiver_id`)
VALUES
	(1158,0,'Evaluation Description 1',2,'2017-02-21',1156,1154),
	(1159,0,'Evaluation Description 2',3,'2017-02-21',1156,1154),
	(1160,0,'Evaluation Description 1',4,'2018-05-21',1156,1155);

/*!40000 ALTER TABLE `Evaluation` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Finder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Finder`;

CREATE TABLE `Finder` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Volcado de tabla Finder_Visa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Finder_Visa`;

CREATE TABLE `Finder_Visa` (
  `Finder_id` int(11) NOT NULL,
  `results_id` int(11) NOT NULL,
  UNIQUE KEY `UK_pfin2ye23wt906x3s2cclhve5` (`results_id`),
  KEY `FK_6kf1d6l2trj3sio6pyyuuqal3` (`Finder_id`),
  CONSTRAINT `FK_6kf1d6l2trj3sio6pyyuuqal3` FOREIGN KEY (`Finder_id`) REFERENCES `Finder` (`id`),
  CONSTRAINT `FK_pfin2ye23wt906x3s2cclhve5` FOREIGN KEY (`results_id`) REFERENCES `Visa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Volcado de tabla FinderMessage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FinderMessage`;

CREATE TABLE `FinderMessage` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `creationMoment` datetime DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `FinderMessage` WRITE;
/*!40000 ALTER TABLE `FinderMessage` DISABLE KEYS */;

INSERT INTO `FinderMessage` (`id`, `version`, `creationMoment`, `endDate`, `keyword`, `startDate`, `owner_id`)
VALUES
	(1218,0,'2018-01-01 01:01:01','2018-12-01','mess','2018-01-01',1145),
	(1219,0,'2018-07-30 18:54:01','2018-12-01','age','2018-01-01',1154);

/*!40000 ALTER TABLE `FinderMessage` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla FinderMessage_Mezzage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FinderMessage_Mezzage`;

CREATE TABLE `FinderMessage_Mezzage` (
  `FinderMessage_id` int(11) NOT NULL,
  `results_id` int(11) NOT NULL,
  UNIQUE KEY `UK_m9ckrf1jpf8p3kegkys7vt3vs` (`results_id`),
  KEY `FK_alxtffd1upqi28iedsdhv8e7c` (`FinderMessage_id`),
  CONSTRAINT `FK_alxtffd1upqi28iedsdhv8e7c` FOREIGN KEY (`FinderMessage_id`) REFERENCES `FinderMessage` (`id`),
  CONSTRAINT `FK_m9ckrf1jpf8p3kegkys7vt3vs` FOREIGN KEY (`results_id`) REFERENCES `Mezzage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `FinderMessage_Mezzage` WRITE;
/*!40000 ALTER TABLE `FinderMessage_Mezzage` DISABLE KEYS */;

INSERT INTO `FinderMessage_Mezzage` (`FinderMessage_id`, `results_id`)
VALUES
	(1218,1190),
	(1218,1192),
	(1219,1193),
	(1219,1194),
	(1219,1195);

/*!40000 ALTER TABLE `FinderMessage_Mezzage` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Folder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Folder`;

CREATE TABLE `Folder` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `folderType` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Folder` WRITE;
/*!40000 ALTER TABLE `Folder` DISABLE KEYS */;

INSERT INTO `Folder` (`id`, `version`, `folderType`, `name`, `owner_id`)
VALUES
	(1161,0,2,'inbox',1145),
	(1162,0,0,'public',1145),
	(1163,0,1,'private',1145),
	(1164,0,2,'inbox',1147),
	(1165,0,0,'private',1147),
	(1166,0,1,'private',1147),
	(1167,0,2,'private',1149),
	(1168,0,0,'public',1149),
	(1169,0,1,'public',1149),
	(1170,0,0,'public',1156),
	(1171,0,2,'inbox',1156),
	(1172,0,2,'inbox',1157),
	(1173,0,0,'public',1157),
	(1174,0,0,'public',1143),
	(1175,0,2,'inbox',1143),
	(1176,0,2,'inbox',1144),
	(1177,0,0,'public',1144),
	(1178,0,2,'inbox',1154),
	(1179,0,2,'inbox',1154),
	(1180,0,0,'public',1154),
	(1181,0,2,'inbox',1155),
	(1182,0,2,'inbox',1155),
	(1183,0,0,'public',1155),
	(1184,0,2,'inbox',1152),
	(1185,0,2,'inbox',1152),
	(1186,0,0,'public',1152),
	(1187,0,2,'inbox',1153),
	(1188,0,2,'inbox',1153),
	(1189,0,0,'public',1153);

/*!40000 ALTER TABLE `Folder` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla hibernate_sequences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hibernate_sequences`;

CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hibernate_sequences` WRITE;
/*!40000 ALTER TABLE `hibernate_sequences` DISABLE KEYS */;

INSERT INTO `hibernate_sequences` (`sequence_name`, `sequence_next_hi_value`)
VALUES
	('DomainEntity',1);

/*!40000 ALTER TABLE `hibernate_sequences` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Inmigrant
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Inmigrant`;

CREATE TABLE `Inmigrant` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  `creditCard_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_i85x6v2498ensgdy52j8s93ov` (`creditCard_id`),
  KEY `FK_rlbi1s0mt7g9xyy42ukodbgpa` (`userAccount_id`),
  CONSTRAINT `FK_rlbi1s0mt7g9xyy42ukodbgpa` FOREIGN KEY (`userAccount_id`) REFERENCES `UserAccount` (`id`),
  CONSTRAINT `FK_i85x6v2498ensgdy52j8s93ov` FOREIGN KEY (`creditCard_id`) REFERENCES `CreditCard` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Inmigrant` WRITE;
/*!40000 ALTER TABLE `Inmigrant` DISABLE KEYS */;

INSERT INTO `Inmigrant` (`id`, `version`, `address`, `email`, `name`, `phone`, `surname`, `userAccount_id`, `creditCard_id`)
VALUES
	(1145,0,'Address 2','Inmigrant1@mail.com','Inmigrant 1','+34 4354352003','Inmigrant1Surname',1133,1146),
	(1147,0,'Address 2','Inmigrant1@mail.com','Inmigrant 2','+34 (555) 4354352004','Inmigrant1Surname',1134,1148),
	(1149,0,'Address 2','Inmigrant1@mail.com','Inmigrant 3','+34 (555) 4354352005','Inmigrant1Surname',1135,NULL),
	(1150,0,'Address 2','Inmigrant1@mail.com','Inmigrant 4','+34 (555) 4354352006','Inmigrant1Surname',1136,1151);

/*!40000 ALTER TABLE `Inmigrant` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Inmigrant_Question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Inmigrant_Question`;

CREATE TABLE `Inmigrant_Question` (
  `Inmigrant_id` int(11) NOT NULL,
  `questionsForMe_id` int(11) NOT NULL,
  UNIQUE KEY `UK_4kwn9mmxbm0dyptk4ke97p01o` (`questionsForMe_id`),
  KEY `FK_5n65gjicf0qerrmat072yxdq4` (`Inmigrant_id`),
  CONSTRAINT `FK_5n65gjicf0qerrmat072yxdq4` FOREIGN KEY (`Inmigrant_id`) REFERENCES `Inmigrant` (`id`),
  CONSTRAINT `FK_4kwn9mmxbm0dyptk4ke97p01o` FOREIGN KEY (`questionsForMe_id`) REFERENCES `Question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Inmigrant_Question` WRITE;
/*!40000 ALTER TABLE `Inmigrant_Question` DISABLE KEYS */;

INSERT INTO `Inmigrant_Question` (`Inmigrant_id`, `questionsForMe_id`)
VALUES
	(1145,1200),
	(1145,1203),
	(1147,1201),
	(1149,1202);

/*!40000 ALTER TABLE `Inmigrant_Question` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Inmigrant_Report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Inmigrant_Report`;

CREATE TABLE `Inmigrant_Report` (
  `Inmigrant_id` int(11) NOT NULL,
  `reports_id` int(11) NOT NULL,
  UNIQUE KEY `UK_h3d61adluo9dy6g502yyu4o98` (`reports_id`),
  KEY `FK_kyl82d7afamw1hs5e2sq122sb` (`Inmigrant_id`),
  CONSTRAINT `FK_kyl82d7afamw1hs5e2sq122sb` FOREIGN KEY (`Inmigrant_id`) REFERENCES `Inmigrant` (`id`),
  CONSTRAINT `FK_h3d61adluo9dy6g502yyu4o98` FOREIGN KEY (`reports_id`) REFERENCES `Report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Inmigrant_Report` WRITE;
/*!40000 ALTER TABLE `Inmigrant_Report` DISABLE KEYS */;

INSERT INTO `Inmigrant_Report` (`Inmigrant_id`, `reports_id`)
VALUES
	(1145,1197),
	(1145,1199),
	(1147,1198);

/*!40000 ALTER TABLE `Inmigrant_Report` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Investigator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Investigator`;

CREATE TABLE `Investigator` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_di2p09ij3fudbrrhk1tx2ntm9` (`userAccount_id`),
  CONSTRAINT `FK_di2p09ij3fudbrrhk1tx2ntm9` FOREIGN KEY (`userAccount_id`) REFERENCES `UserAccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Investigator` WRITE;
/*!40000 ALTER TABLE `Investigator` DISABLE KEYS */;

INSERT INTO `Investigator` (`id`, `version`, `address`, `email`, `name`, `phone`, `surname`, `userAccount_id`)
VALUES
	(1152,0,'Address 2','Investigator1@mail.com','Investigator 1','+34 (555) 4354352007','investigator2Surname',1137),
	(1153,0,'Address 2','Investigator1@mail.com','Investigator 2','+34 (555) 4354352008','investigator2Surname',1138);

/*!40000 ALTER TABLE `Investigator` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Investigator_Inmigrant
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Investigator_Inmigrant`;

CREATE TABLE `Investigator_Inmigrant` (
  `Investigator_id` int(11) NOT NULL,
  `underInvestigation_id` int(11) NOT NULL,
  UNIQUE KEY `UK_mtrg8bx5k9ufmc4n14ttimltu` (`underInvestigation_id`),
  KEY `FK_8gwc6vlpe02k37s5fd4l82dxu` (`Investigator_id`),
  CONSTRAINT `FK_8gwc6vlpe02k37s5fd4l82dxu` FOREIGN KEY (`Investigator_id`) REFERENCES `Investigator` (`id`),
  CONSTRAINT `FK_mtrg8bx5k9ufmc4n14ttimltu` FOREIGN KEY (`underInvestigation_id`) REFERENCES `Inmigrant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Investigator_Inmigrant` WRITE;
/*!40000 ALTER TABLE `Investigator_Inmigrant` DISABLE KEYS */;

INSERT INTO `Investigator_Inmigrant` (`Investigator_id`, `underInvestigation_id`)
VALUES
	(1152,1149),
	(1153,1150);

/*!40000 ALTER TABLE `Investigator_Inmigrant` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Law
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Law`;

CREATE TABLE `Law` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `abrogation` date DEFAULT NULL,
  `enatctment` date DEFAULT NULL,
  `hidden` bit(1) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_is49rdvlftjviqiryjy6tbb5a` (`country_id`),
  CONSTRAINT `FK_is49rdvlftjviqiryjy6tbb5a` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Law` WRITE;
/*!40000 ALTER TABLE `Law` DISABLE KEYS */;

INSERT INTO `Law` (`id`, `version`, `abrogation`, `enatctment`, `hidden`, `text`, `title`, `country_id`)
VALUES
	(1244,0,'2019-02-01','2018-02-01',b'0','Text 1','Law 1',1204),
	(1245,0,'2019-02-01','2018-02-01',b'0','Text 2','Law 2',1204),
	(1246,0,'2019-02-01','2018-02-01',b'0','Text 3','Law 3',1204),
	(1247,0,'2019-02-01','2018-02-01',b'0','Text 4','Law 4',1204),
	(1248,0,'2019-02-01','2018-02-01',b'0','Text 5','Law 5',1205),
	(1249,0,'2019-02-01','2018-02-01',b'0','Text 6','Law 6',1205);

/*!40000 ALTER TABLE `Law` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Law_Law
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Law_Law`;

CREATE TABLE `Law_Law` (
  `Law_id` int(11) NOT NULL,
  `relatedLaws_id` int(11) NOT NULL,
  UNIQUE KEY `UK_82oy92l3u7mnigbdt2rm4s6rn` (`relatedLaws_id`),
  KEY `FK_2lt6lusiv8sd3p80i6rorwdgh` (`Law_id`),
  CONSTRAINT `FK_2lt6lusiv8sd3p80i6rorwdgh` FOREIGN KEY (`Law_id`) REFERENCES `Law` (`id`),
  CONSTRAINT `FK_82oy92l3u7mnigbdt2rm4s6rn` FOREIGN KEY (`relatedLaws_id`) REFERENCES `Law` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Volcado de tabla Law_Requirement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Law_Requirement`;

CREATE TABLE `Law_Requirement` (
  `Law_id` int(11) NOT NULL,
  `requirements_id` int(11) NOT NULL,
  UNIQUE KEY `UK_44er5u5e0hfaudthrr9bvvu5u` (`requirements_id`),
  KEY `FK_83wxb5niy1nd3gt48shuj2fme` (`Law_id`),
  CONSTRAINT `FK_83wxb5niy1nd3gt48shuj2fme` FOREIGN KEY (`Law_id`) REFERENCES `Law` (`id`),
  CONSTRAINT `FK_44er5u5e0hfaudthrr9bvvu5u` FOREIGN KEY (`requirements_id`) REFERENCES `Requirement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Law_Requirement` WRITE;
/*!40000 ALTER TABLE `Law_Requirement` DISABLE KEYS */;

INSERT INTO `Law_Requirement` (`Law_id`, `requirements_id`)
VALUES
	(1244,1250),
	(1245,1251),
	(1246,1252);

/*!40000 ALTER TABLE `Law_Requirement` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Mezzage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Mezzage`;

CREATE TABLE `Mezzage` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `body` varchar(255) DEFAULT NULL,
  `receiverEmail` varchar(255) DEFAULT NULL,
  `sendDate` datetime DEFAULT NULL,
  `senderEmail` varchar(255) DEFAULT NULL,
  `spam` bit(1) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gughpct86iqnkatriwn09umui` (`folder_id`),
  CONSTRAINT `FK_gughpct86iqnkatriwn09umui` FOREIGN KEY (`folder_id`) REFERENCES `Folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Mezzage` WRITE;
/*!40000 ALTER TABLE `Mezzage` DISABLE KEYS */;

INSERT INTO `Mezzage` (`id`, `version`, `body`, `receiverEmail`, `sendDate`, `senderEmail`, `spam`, `subject`, `folder_id`, `receiver_id`, `sender_id`)
VALUES
	(1190,0,'body','adminitrator1@mail.com','2018-01-02 00:00:00','adminitrator2@mail.com',b'0','test',1161,1143,1144),
	(1191,0,'sex','adminitrator1@mail.com','2018-01-02 00:00:00','Officer1@mail.com',b'1','test',1161,1143,1154),
	(1192,0,'body','inmigrant1@mail.com','2018-01-02 00:00:00','investigator1@mail.com',b'0','test',1163,1145,1152),
	(1193,0,'body','inmigrant1@mail.com','2018-01-02 00:00:00','inmigrant1@mail.com',b'0','test',1162,1145,1145),
	(1194,0,'body','investigator1@mail.com','2018-01-02 00:00:00','investigator1@mail.com',b'0','test',1165,1152,1152),
	(1195,0,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ','supervisor1@mail.com','2018-01-02 00:00:00','supervisor1@mail.com',b'0','test',1168,1156,1156),
	(1196,0,'Lorem ipsum dolor sit nigga, consectetur adipiscing elit, sed do eiusmod tempor incididunt ','supervisor1@mail.com','2018-01-02 00:00:00','supervisor1@mail.com',b'0','test',1174,1143,1156);

/*!40000 ALTER TABLE `Mezzage` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Mezzage_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Mezzage_attachments`;

CREATE TABLE `Mezzage_attachments` (
  `Mezzage_id` int(11) NOT NULL,
  `attachments` varchar(255) DEFAULT NULL,
  KEY `FK_fvc63hmswbm9f32junkx1au20` (`Mezzage_id`),
  CONSTRAINT `FK_fvc63hmswbm9f32junkx1au20` FOREIGN KEY (`Mezzage_id`) REFERENCES `Mezzage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Mezzage_attachments` WRITE;
/*!40000 ALTER TABLE `Mezzage_attachments` DISABLE KEYS */;

INSERT INTO `Mezzage_attachments` (`Mezzage_id`, `attachments`)
VALUES
	(1190,'https://asdasd.com'),
	(1190,'https://asdasd.com'),
	(1190,'https://asdasd.com');

/*!40000 ALTER TABLE `Mezzage_attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Officer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Officer`;

CREATE TABLE `Officer` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  `lastEvaluationDate` datetime DEFAULT NULL,
  `underSupervision` bit(1) NOT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l1t1o3angigxk0oe9oa6tfom0` (`supervisor_id`),
  KEY `FK_j2oip9hkjxaqmvwiei4ocfght` (`userAccount_id`),
  CONSTRAINT `FK_j2oip9hkjxaqmvwiei4ocfght` FOREIGN KEY (`userAccount_id`) REFERENCES `UserAccount` (`id`),
  CONSTRAINT `FK_l1t1o3angigxk0oe9oa6tfom0` FOREIGN KEY (`supervisor_id`) REFERENCES `Supervisor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Officer` WRITE;
/*!40000 ALTER TABLE `Officer` DISABLE KEYS */;

INSERT INTO `Officer` (`id`, `version`, `address`, `email`, `name`, `phone`, `surname`, `userAccount_id`, `lastEvaluationDate`, `underSupervision`, `supervisor_id`)
VALUES
	(1154,0,'Address 2','Officer1@mail.com','Officer 1','+34 4354352009','Officer1Surname',1139,'2017-02-21 00:00:00',b'1',NULL),
	(1155,1,'Address 2','Officer2@mail.com','Officer 2','+34 (555) 4354352010','Officer2Surname',1140,'2018-05-21 00:00:00',b'1',1156);

/*!40000 ALTER TABLE `Officer` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Officer_Application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Officer_Application`;

CREATE TABLE `Officer_Application` (
  `Officer_id` int(11) NOT NULL,
  `applications_id` int(11) NOT NULL,
  UNIQUE KEY `UK_103o56g9fjkk7rttsmq2k498w` (`applications_id`),
  KEY `FK_jyy7ig0gur3ux2okjdv4hkt6i` (`Officer_id`),
  CONSTRAINT `FK_jyy7ig0gur3ux2okjdv4hkt6i` FOREIGN KEY (`Officer_id`) REFERENCES `Officer` (`id`),
  CONSTRAINT `FK_103o56g9fjkk7rttsmq2k498w` FOREIGN KEY (`applications_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Officer_Application` WRITE;
/*!40000 ALTER TABLE `Officer_Application` DISABLE KEYS */;

INSERT INTO `Officer_Application` (`Officer_id`, `applications_id`)
VALUES
	(1154,1221);

/*!40000 ALTER TABLE `Officer_Application` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Officer_Evaluation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Officer_Evaluation`;

CREATE TABLE `Officer_Evaluation` (
  `Officer_id` int(11) NOT NULL,
  `evaluations_id` int(11) NOT NULL,
  UNIQUE KEY `UK_a84yxpniggvynxppc4xdrnbuc` (`evaluations_id`),
  KEY `FK_dlcdjf98t946blyhpb8r7bidb` (`Officer_id`),
  CONSTRAINT `FK_dlcdjf98t946blyhpb8r7bidb` FOREIGN KEY (`Officer_id`) REFERENCES `Officer` (`id`),
  CONSTRAINT `FK_a84yxpniggvynxppc4xdrnbuc` FOREIGN KEY (`evaluations_id`) REFERENCES `Evaluation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Officer_Evaluation` WRITE;
/*!40000 ALTER TABLE `Officer_Evaluation` DISABLE KEYS */;

INSERT INTO `Officer_Evaluation` (`Officer_id`, `evaluations_id`)
VALUES
	(1154,1158),
	(1154,1159),
	(1155,1160);

/*!40000 ALTER TABLE `Officer_Evaluation` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Officer_Question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Officer_Question`;

CREATE TABLE `Officer_Question` (
  `Officer_id` int(11) NOT NULL,
  `questions_id` int(11) NOT NULL,
  UNIQUE KEY `UK_kgw0xqrnio2j2g6fh8ewssaxa` (`questions_id`),
  KEY `FK_lsb0uy8j64cqtggmsgjr0n2l5` (`Officer_id`),
  CONSTRAINT `FK_lsb0uy8j64cqtggmsgjr0n2l5` FOREIGN KEY (`Officer_id`) REFERENCES `Officer` (`id`),
  CONSTRAINT `FK_kgw0xqrnio2j2g6fh8ewssaxa` FOREIGN KEY (`questions_id`) REFERENCES `Question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Officer_Question` WRITE;
/*!40000 ALTER TABLE `Officer_Question` DISABLE KEYS */;

INSERT INTO `Officer_Question` (`Officer_id`, `questions_id`)
VALUES
	(1154,1200),
	(1154,1201),
	(1154,1202),
	(1155,1203);

/*!40000 ALTER TABLE `Officer_Question` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla PersonalSection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PersonalSection`;

CREATE TABLE `PersonalSection` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `birthDate` date DEFAULT NULL,
  `birthPlace` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8le2crvvarbnug14o396qnn5g` (`application_id`),
  CONSTRAINT `FK_8le2crvvarbnug14o396qnn5g` FOREIGN KEY (`application_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `PersonalSection` WRITE;
/*!40000 ALTER TABLE `PersonalSection` DISABLE KEYS */;

INSERT INTO `PersonalSection` (`id`, `version`, `birthDate`, `birthPlace`, `picture`, `application_id`)
VALUES
	(1224,0,'1993-07-12','somewhere 1','https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Flag_of_Spain.svg/1280px-Flag_of_Spain.svg.png',1220),
	(1225,0,'1973-07-12','somewhere 2','https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Flag_of_Spain.svg/1280px-Flag_of_Spain.svg.png',1221),
	(1226,0,'1943-08-12','somewhere I belong','https://upload.wikimedia.org/wikipedia/en/thumb/9/9a/Flag_of_Spain.svg/1280px-Flag_of_Spain.svg.png',1222);

/*!40000 ALTER TABLE `PersonalSection` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla PersonalSection_names
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PersonalSection_names`;

CREATE TABLE `PersonalSection_names` (
  `PersonalSection_id` int(11) NOT NULL,
  `names` varchar(255) DEFAULT NULL,
  KEY `FK_9qx3p5d0fpr44c5qqf9mo94es` (`PersonalSection_id`),
  CONSTRAINT `FK_9qx3p5d0fpr44c5qqf9mo94es` FOREIGN KEY (`PersonalSection_id`) REFERENCES `PersonalSection` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `PersonalSection_names` WRITE;
/*!40000 ALTER TABLE `PersonalSection_names` DISABLE KEYS */;

INSERT INTO `PersonalSection_names` (`PersonalSection_id`, `names`)
VALUES
	(1224,'Name 1'),
	(1224,'Name 2'),
	(1225,'Name 1'),
	(1226,'Name 1');

/*!40000 ALTER TABLE `PersonalSection_names` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Question`;

CREATE TABLE `Question` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `statement` varchar(255) DEFAULT NULL,
  `writeDate` datetime DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `maker_id` int(11) DEFAULT NULL,
  `relatedApplication_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_60uo468wjtmcklneu7i8sah9g` (`applicant_id`),
  KEY `FK_4ou8rpabmowrou7ltopsyy2ff` (`maker_id`),
  KEY `FK_7t2sb87ovo6fn9dyurydi9jb1` (`relatedApplication_id`),
  CONSTRAINT `FK_7t2sb87ovo6fn9dyurydi9jb1` FOREIGN KEY (`relatedApplication_id`) REFERENCES `Application` (`id`),
  CONSTRAINT `FK_4ou8rpabmowrou7ltopsyy2ff` FOREIGN KEY (`maker_id`) REFERENCES `Officer` (`id`),
  CONSTRAINT `FK_60uo468wjtmcklneu7i8sah9g` FOREIGN KEY (`applicant_id`) REFERENCES `Inmigrant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Question` WRITE;
/*!40000 ALTER TABLE `Question` DISABLE KEYS */;

INSERT INTO `Question` (`id`, `version`, `answer`, `statement`, `writeDate`, `applicant_id`, `maker_id`, `relatedApplication_id`)
VALUES
	(1200,1,'answer1','question1','2018-01-02 21:21:50',1145,1154,1221),
	(1201,1,'answer2','question2','2018-01-02 21:21:50',1147,1154,1220),
	(1202,1,'answer3','question3','2018-01-02 21:21:50',1149,1154,1221),
	(1203,1,'answer4','question4','2018-01-02 21:21:50',1145,1155,1222);

/*!40000 ALTER TABLE `Question` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Report`;

CREATE TABLE `Report` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `attendant_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_3vee44y9f1hdp1u2lt5cpl8kt` (`attendant_id`),
  KEY `FK_t8hpcmawch9npdwc658ym34rk` (`owner_id`),
  CONSTRAINT `FK_t8hpcmawch9npdwc658ym34rk` FOREIGN KEY (`owner_id`) REFERENCES `Investigator` (`id`),
  CONSTRAINT `FK_3vee44y9f1hdp1u2lt5cpl8kt` FOREIGN KEY (`attendant_id`) REFERENCES `Inmigrant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Report` WRITE;
/*!40000 ALTER TABLE `Report` DISABLE KEYS */;

INSERT INTO `Report` (`id`, `version`, `text`, `attendant_id`, `owner_id`)
VALUES
	(1197,0,'report1',1145,1152),
	(1198,0,'report2',1147,1152),
	(1199,0,'report3',1145,1153);

/*!40000 ALTER TABLE `Report` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Report_pictures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Report_pictures`;

CREATE TABLE `Report_pictures` (
  `Report_id` int(11) NOT NULL,
  `pictures` varchar(255) DEFAULT NULL,
  KEY `FK_3xv6agwub00h0cxppes02otva` (`Report_id`),
  CONSTRAINT `FK_3xv6agwub00h0cxppes02otva` FOREIGN KEY (`Report_id`) REFERENCES `Report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Report_pictures` WRITE;
/*!40000 ALTER TABLE `Report_pictures` DISABLE KEYS */;

INSERT INTO `Report_pictures` (`Report_id`, `pictures`)
VALUES
	(1197,'https://dasd.com'),
	(1198,'https://dasd.com'),
	(1199,'https://dasd.com');

/*!40000 ALTER TABLE `Report_pictures` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Requirement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Requirement`;

CREATE TABLE `Requirement` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `abrogated` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `hidden` bit(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `law_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1qwnx6c4ygc13a58uo6wp7x9i` (`law_id`),
  CONSTRAINT `FK_1qwnx6c4ygc13a58uo6wp7x9i` FOREIGN KEY (`law_id`) REFERENCES `Law` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Requirement` WRITE;
/*!40000 ALTER TABLE `Requirement` DISABLE KEYS */;

INSERT INTO `Requirement` (`id`, `version`, `abrogated`, `description`, `hidden`, `title`, `law_id`)
VALUES
	(1250,0,b'0','description 1',b'0','requirement 1',1244),
	(1251,0,b'0','description 2',b'0','requirement 2',1245),
	(1252,0,b'0','description 1',b'0','requirement 1',1246);

/*!40000 ALTER TABLE `Requirement` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla SocialSection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `SocialSection`;

CREATE TABLE `SocialSection` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `socialNetwork` varchar(255) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_a046wuekj8lqx898cb2584qio` (`application_id`),
  CONSTRAINT `FK_a046wuekj8lqx898cb2584qio` FOREIGN KEY (`application_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `SocialSection` WRITE;
/*!40000 ALTER TABLE `SocialSection` DISABLE KEYS */;

INSERT INTO `SocialSection` (`id`, `version`, `link`, `nickname`, `socialNetwork`, `application_id`)
VALUES
	(1230,0,'http://facebook.com','nick 1','facebook',1220),
	(1231,0,'http://twitter.com','nick 2','facebook',1220),
	(1232,0,'http://facebook.com','nick 3','facebook',1221),
	(1233,0,'http://facebook.com','nick 4','facebook',1222);

/*!40000 ALTER TABLE `SocialSection` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Supervisor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Supervisor`;

CREATE TABLE `Supervisor` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userAccount_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_p0m2as59gdgn27sxsud5cxift` (`userAccount_id`),
  CONSTRAINT `FK_p0m2as59gdgn27sxsud5cxift` FOREIGN KEY (`userAccount_id`) REFERENCES `UserAccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Supervisor` WRITE;
/*!40000 ALTER TABLE `Supervisor` DISABLE KEYS */;

INSERT INTO `Supervisor` (`id`, `version`, `address`, `email`, `name`, `phone`, `surname`, `userAccount_id`)
VALUES
	(1156,0,'Address 2','Supervisor1@mail.com','Supervisor 1','+34 4354352011','Supervisor1Surname',1141),
	(1157,0,'Address 2','Supervisor1@mail.com','Supervisor 2','+34 (555) 4354352012','Supervisor1Surname',1142);

/*!40000 ALTER TABLE `Supervisor` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla UserAccount
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserAccount`;

CREATE TABLE `UserAccount` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_csivo9yqa08nrbkog71ycilh5` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `UserAccount` WRITE;
/*!40000 ALTER TABLE `UserAccount` DISABLE KEYS */;

INSERT INTO `UserAccount` (`id`, `version`, `password`, `username`)
VALUES
	(1131,0,'d5cee333abe432891a0de57d0ee38713','administrator1'),
	(1132,0,'82954495ff7e2a735ed2192c35b2cd00','administrator2'),
	(1133,0,'24ed2221ac57cc7a3c9fb44f3c775009','inmigrant1'),
	(1134,0,'32f568df8673555c328efc5f1137996a','inmigrant2'),
	(1135,0,'d27f94f9328cd9b343d70370e132983b','inmigrant3'),
	(1136,0,'d36d64f7521e5ce8222ffd935d7125d9','inmigrant4'),
	(1137,0,'038ffd9c1cfccd215fabf99238cf84e2','investigator1'),
	(1138,0,'bde3c61bc57bc2dc922f0ba43dbaa009','investigator2'),
	(1139,0,'7c3d0453108aefa46c07e91c8bc031d3','officer1'),
	(1140,0,'5cea39c4f5c604f237baebc9e440ae5f','officer2'),
	(1141,0,'d947df684dd40733bef0cd787f8ae4e1','supervisor1'),
	(1142,0,'2a03aa2f503b2db79e38d483f2f612cc','supervisor2');

/*!40000 ALTER TABLE `UserAccount` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla UserAccount_authorities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserAccount_authorities`;

CREATE TABLE `UserAccount_authorities` (
  `UserAccount_id` int(11) NOT NULL,
  `authority` varchar(255) DEFAULT NULL,
  KEY `FK_b63ua47r0u1m7ccc9lte2ui4r` (`UserAccount_id`),
  CONSTRAINT `FK_b63ua47r0u1m7ccc9lte2ui4r` FOREIGN KEY (`UserAccount_id`) REFERENCES `UserAccount` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `UserAccount_authorities` WRITE;
/*!40000 ALTER TABLE `UserAccount_authorities` DISABLE KEYS */;

INSERT INTO `UserAccount_authorities` (`UserAccount_id`, `authority`)
VALUES
	(1131,'ADMINISTRATOR'),
	(1132,'ADMINISTRATOR'),
	(1133,'INMIGRANT'),
	(1134,'INMIGRANT'),
	(1135,'INMIGRANT'),
	(1136,'INMIGRANT'),
	(1137,'INVESTIGATOR'),
	(1138,'INVESTIGATOR'),
	(1139,'OFFICER'),
	(1140,'OFFICER'),
	(1141,'SUPERVISOR'),
	(1142,'SUPERVISOR');

/*!40000 ALTER TABLE `UserAccount_authorities` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Uttils
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Uttils`;

CREATE TABLE `Uttils` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `finderCarche` int(11) NOT NULL,
  `numberOfFinderResults` int(11) NOT NULL,
  `systemName` varchar(255) DEFAULT NULL,
  `welcomeMezzage` varchar(255) DEFAULT NULL,
  `welcomeMezzageES` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Uttils` WRITE;
/*!40000 ALTER TABLE `Uttils` DISABLE KEYS */;

INSERT INTO `Uttils` (`id`, `version`, `banner`, `finderCarche`, `numberOfFinderResults`, `systemName`, `welcomeMezzage`, `welcomeMezzageES`)
VALUES
	(1243,0,'https://daviddelatorre.me/wp-content/uploads/2018/07/logo.png',1,10,'Acme-Inmigrant','Welcome to everyone','Bienvenidos');

/*!40000 ALTER TABLE `Uttils` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Uttils_spamWords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Uttils_spamWords`;

CREATE TABLE `Uttils_spamWords` (
  `Uttils_id` int(11) NOT NULL,
  `spamWords` varchar(255) DEFAULT NULL,
  KEY `FK_icralc51mxmhvq6w7l1i16eyw` (`Uttils_id`),
  CONSTRAINT `FK_icralc51mxmhvq6w7l1i16eyw` FOREIGN KEY (`Uttils_id`) REFERENCES `Uttils` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Uttils_spamWords` WRITE;
/*!40000 ALTER TABLE `Uttils_spamWords` DISABLE KEYS */;

INSERT INTO `Uttils_spamWords` (`Uttils_id`, `spamWords`)
VALUES
	(1243,'Sex'),
	(1243,'Viagra'),
	(1243,'Cialis'),
	(1243,'Nigga');

/*!40000 ALTER TABLE `Uttils_spamWords` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Visa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Visa`;

CREATE TABLE `Visa` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `abrogated` bit(1) NOT NULL,
  `clazz` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dv6pkgoytrlh3ali5vxllr0tb` (`category_id`),
  KEY `FK_ofsf31wkhm8xa37cy1k00lpjm` (`country_id`),
  CONSTRAINT `FK_ofsf31wkhm8xa37cy1k00lpjm` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`),
  CONSTRAINT `FK_dv6pkgoytrlh3ali5vxllr0tb` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Visa` WRITE;
/*!40000 ALTER TABLE `Visa` DISABLE KEYS */;

INSERT INTO `Visa` (`id`, `version`, `abrogated`, `clazz`, `currency`, `description`, `price`, `category_id`, `country_id`)
VALUES
	(1206,1,b'0','Clase 1','£','Descripción visa 1',10,1212,1204),
	(1207,1,b'0','Clase 2','$','Descripción visa 2',10,1212,1204),
	(1208,1,b'0','Clase 3','$','Descripción visa 3',10,1214,1204),
	(1209,1,b'0','Clase 3','?','Descripción visa 4',10,1215,1205),
	(1210,1,b'1','Clase 1','£','Descripción visa 5',10,1215,1205);

/*!40000 ALTER TABLE `Visa` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Visa_Application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Visa_Application`;

CREATE TABLE `Visa_Application` (
  `Visa_id` int(11) NOT NULL,
  `applications_id` int(11) NOT NULL,
  UNIQUE KEY `UK_t22sy4544p7oq2w3myq0sd3nt` (`applications_id`),
  KEY `FK_bef2trnb9qa6is57fcn0n2ik8` (`Visa_id`),
  CONSTRAINT `FK_bef2trnb9qa6is57fcn0n2ik8` FOREIGN KEY (`Visa_id`) REFERENCES `Visa` (`id`),
  CONSTRAINT `FK_t22sy4544p7oq2w3myq0sd3nt` FOREIGN KEY (`applications_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Visa_Application` WRITE;
/*!40000 ALTER TABLE `Visa_Application` DISABLE KEYS */;

INSERT INTO `Visa_Application` (`Visa_id`, `applications_id`)
VALUES
	(1206,1220),
	(1206,1221);

/*!40000 ALTER TABLE `Visa_Application` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla Visa_Requirement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Visa_Requirement`;

CREATE TABLE `Visa_Requirement` (
  `Visa_id` int(11) NOT NULL,
  `requirements_id` int(11) NOT NULL,
  KEY `FK_9wt2phjomdfykhni56dbx7d17` (`requirements_id`),
  KEY `FK_97yj83sd5wmogjswnial9cpnw` (`Visa_id`),
  CONSTRAINT `FK_97yj83sd5wmogjswnial9cpnw` FOREIGN KEY (`Visa_id`) REFERENCES `Visa` (`id`),
  CONSTRAINT `FK_9wt2phjomdfykhni56dbx7d17` FOREIGN KEY (`requirements_id`) REFERENCES `Requirement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Visa_Requirement` WRITE;
/*!40000 ALTER TABLE `Visa_Requirement` DISABLE KEYS */;

INSERT INTO `Visa_Requirement` (`Visa_id`, `requirements_id`)
VALUES
	(1206,1250),
	(1206,1251),
	(1207,1251),
	(1207,1252),
	(1208,1252),
	(1209,1250),
	(1210,1250);

/*!40000 ALTER TABLE `Visa_Requirement` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla WorkSection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `WorkSection`;

CREATE TABLE `WorkSection` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_e88ql9puakxa23goda4ubmt50` (`application_id`),
  CONSTRAINT `FK_e88ql9puakxa23goda4ubmt50` FOREIGN KEY (`application_id`) REFERENCES `Application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `WorkSection` WRITE;
/*!40000 ALTER TABLE `WorkSection` DISABLE KEYS */;

INSERT INTO `WorkSection` (`id`, `version`, `company`, `endDate`, `position`, `startDate`, `application_id`)
VALUES
	(1239,0,'company 1','2018-03-21','director','2017-03-21',1220),
	(1240,0,'company 3',NULL,'CEO','2018-03-22',1220),
	(1241,0,'company 2','2018-03-21','developer','2017-03-21',1221),
	(1242,0,'company 4444','2018-03-21','developer','2017-03-21',1222);

/*!40000 ALTER TABLE `WorkSection` ENABLE KEYS */;
UNLOCK TABLES;








COMMIT;
